#pragma once
#include "IEnemy.hpp"
#include "TextureManager.hpp"

class AIStates;
class Player;
class Projectile;
class Bullet;


class EnemyPlane : public IEnemy
{
public:
	
	EnemyPlane(Player* p_player, TextureManager* p_textureManager, float p_posX, float p_posY);
	~EnemyPlane();
	void Update();
	sf::Sprite* GetSprite();
	sf::RectangleShape GetHitBox();
	EENEMY GetType();
	
	bool IsAlerted();
	void SetAlerted(bool p_alerted);
	sf::CircleShape GetSightRadius();
	void SetSightRadius(float p_radius);
	sf::RectangleShape GetShootRange();
	void SetShootRange(sf::Vector2f p_range);
	int GetHP();
	void SetHP(int p_amount);

	sf::Time GetTimer();

	void SetPosition(float p_posX, float p_posY);
	void AddX(float p_x);
	void AddY(float p_y);
	float GetX();
	float GetY();
	float GetCenterX();
	float GetCenterY();
	void SetX(float p_x);
	void SetY(float p_y);
	float GetSpeed();
	void MoveTowards(sf::Vector2f p_pos);
	void SightCheck(sf::Sprite* p_sprite);
	
	void ShootProjectile();
	bool ProjectileCollision();

	void RotateRight();
	void RotateLeft();

	void Draw(sf::RenderWindow& p_window);
	void PauseSounds();
	void SetState(AIStates* p_state);
	
private:
	
	TextureManager* m_textureManager;

	bool m_state_check;
	bool m_alerted;
	bool m_hasFired;
	float m_speed;
	float m_turningSpeed;
	int m_HP;
	int m_bulletCount;
	int m_shotCount;
	int m_damageFrames;
	
	AIStates* m_state;
	Player* m_player;
	Bullet* m_bullets[10];

	sf::Sprite* m_sprite;
	sf::Sprite m_searchLight;
	sf::Sprite* m_rotorSprite;

	sf::Texture* m_texture;
	sf::Texture* m_damagedTex;
	sf::Texture* m_deathTex;
	sf::Texture* m_searchLightTex;
	sf::Texture* m_rotorTex;

	sf::CircleShape m_sightRadius;
	sf::RectangleShape m_shootRange;
	sf::RectangleShape m_hitbox;
	
	sf::Vector2f m_pos;
	sf::Vector2f m_center;
	sf::Vector2f m_waypoint;
	sf::Vector2i m_animation;
	sf::Vector2i m_rotorAnimation;

	sf::Clock m_clock;
	sf::Clock m_animationTimer;
	sf::Clock m_animationDelay;
	sf::Time m_invincibilityFrames;
	sf::Time m_shootDelay;
	sf::Time m_rotorAnimationDelay;
	sf::Time m_timer;
	sf::Time m_deathTimer;

	sf::SoundBuffer m_fire;
	sf::SoundBuffer m_death;
	sf::Sound m_deathSound;
	sf::Sound m_fireSound;

};