#include "stdafx.hpp"
#include "Clouds.hpp"
#include "SpriteManager.hpp"

Clouds::Clouds(float p_posX, float p_posY, TextureManager* p_textureManager, Player* p_player)
{
	m_textureManager = p_textureManager;

	m_sprite = new sf::Sprite;
	int rotation = rand() % 360;
	int texture = clamp(rand() % 7, 1, 7);

	if (texture == 1)
		m_texture = m_textureManager->LoadTexture("../assets/Clouds/Clouds1.png");
	else if (texture == 2)
		m_texture = m_textureManager->LoadTexture("../assets/Clouds/Clouds2.png");
	else if (texture == 3)
		m_texture = m_textureManager->LoadTexture("../assets/Clouds/Clouds3.png");
	else if (texture == 4)
		m_texture = m_textureManager->LoadTexture("../assets/Clouds/Clouds4.png");
	else if (texture == 5)
		m_texture = m_textureManager->LoadTexture("../assets/Clouds/Clouds5.png");
	else if (texture == 6)
		m_texture = m_textureManager->LoadTexture("../assets/Clouds/Clouds6.png");
	else if (texture == 7)
		m_texture = m_textureManager->LoadTexture("../assets/Clouds/Clouds7.png");

	m_sprite->setTexture(*m_texture);
	m_sprite->setOrigin(m_texture->getSize().x / 2, m_texture->getSize().y / 2);
	m_sprite->setRotation(rotation);
	
	m_hitbox.setRotation(rotation);
	m_hitbox.setOrigin(m_sprite->getOrigin().x / 2.5, m_sprite->getOrigin().y / 2.5);
	m_hitbox.setFillColor(sf::Color::Blue);
	m_hitbox.setSize(sf::Vector2f(m_texture->getSize().x / 2.5, m_texture->getSize().y / 2.5));

	m_speed = 0.05f;
	m_player = p_player;

	m_pos.x = p_posX;
	m_pos.y = p_posY;
}

Clouds::~Clouds()
{
	delete m_sprite;
}

bool Clouds::Update()
{
	m_pos.x += m_speed;
	m_sprite->setPosition(m_pos);
	m_hitbox.setPosition(m_pos);

	if (m_hitbox.getGlobalBounds().intersects(m_player->GetHitbox().getGlobalBounds()))
	{
		return true;
	}
	return false;

	//f -= 0.1f;

	//if (f > 0)
	//{
	//	m_sprite->setColor(sf::Color(255, 255, 255, f));
	//}
	//else
	//{
	//	f = 255;
	//}

	//if (m_pos.x > mapWidth);
	//{
	//	m_pos.x = -(m_sprite->getTextureRect().width);
	//	
	//}
}

void Clouds::SetPosition(float p_posX, float p_posY)
{
	m_pos.x = p_posX;
	m_pos.y = p_posX;
}
void Clouds::AddX(float p_x) 
{
	m_pos.x += p_x;
}
void Clouds::AddY(float p_y)
{
	m_pos.y += p_y;
}
float Clouds::GetX()
{
	return m_pos.x;
}
float Clouds::GetY()
{
	return m_pos.y;
}
float Clouds::GetCenterX()
{
	return m_center.x;
}
float Clouds::GetCenterY()
{
	return m_center.y;
}
//float Clouds::GetHideBoxX()
//{
//	return m_hide.x;
//}
//float Clouds::GetHideBoxY()
//{
//	return m_hide.y;
//}
//float Clouds::GetHideBoxHeight()
//{
//	m_hideHeight = m_sprite->getLocalBounds().height - 300;
//	return m_hideHeight;
//}
//float Clouds::GetHideBoxWidth()
//{
//	m_hideWidth = m_sprite->getLocalBounds().width - 300;
//	return m_hideWidth;
//}
float Clouds::GetSpeed()
{
	return m_speed;
}
void Clouds::SetSpeed(float p_speed)
{
	m_speed = p_speed;
}

sf::Sprite* Clouds::GetSprite()
{
	return m_sprite;
}


void Clouds::Draw(sf::RenderWindow& p_window)
{
	p_window.draw(*m_sprite);
	/*p_window.draw(m_hitbox);*/
}

