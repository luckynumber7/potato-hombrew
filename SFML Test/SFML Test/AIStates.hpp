#pragma once

class Player;
class EnemyPlane;
class IEnemy;

enum ENUM_AISTATE
{
	PATROL,
	CHASE,
	SEARCH
};

class AIStates
{
public:
	~AIStates() {};
	virtual bool HandleState() = 0;
	virtual ENUM_AISTATE GetState() = 0;
private:
};

class Patrol : public AIStates
{
public:
	Patrol(IEnemy* p_enemy, Player* p_player);
	~Patrol();
	bool HandleState();
	ENUM_AISTATE GetState();
private:
	Patrol() {};
	IEnemy* m_owner;
	Player* m_target;
	sf::Vector2f m_randPos;
};

class Chase : public AIStates
{
public:
	Chase(IEnemy* p_enemy, Player* p_player);
	~Chase();
	bool HandleState();
	ENUM_AISTATE GetState();
private:
	Chase() {};
	IEnemy* m_owner;
	Player* m_target;
};

class Search : public AIStates
{
public:
	Search(IEnemy* p_enemy, Player* p_player, sf::Vector2f p_waypoint);
	~Search();
	bool HandleState();
	ENUM_AISTATE GetState();
private:
	Search() {};
	IEnemy* m_owner;
	Player* m_target;
	sf::Vector2f m_waypoint;
	sf::Time m_timer;
	sf::Clock m_clock;
};