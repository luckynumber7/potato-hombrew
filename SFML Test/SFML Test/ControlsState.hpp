#pragma once
#pragma once
#ifndef CONTROLSSTATE_HPP
#define CONTROLSSTATE_HPP

#include "State.hpp"
#include "stdafx.hpp"
#include "TextureManager.hpp"

#include <SFML/System.hpp>
#include <SFML/Graphics/Color.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>

#include <SFML\Audio.hpp>
#include <SFML/Audio/Music.hpp>

class StateMachine;

namespace sf
{
	class RenderWindow;
}

class ControlsState : public State
{
public:
	ControlsState(StateMachine& machine, sf::RenderWindow& window, TextureManager* p_textureManager, bool replace = false, bool restart = false);

	void pause();
	void resume();
	void update();
	void draw();
private:
	sf::Texture* m_bgTex;
	sf::Texture* m_Cloud1Tex;
	sf::Texture* m_fgTex;
	sf::Texture* m_CloudTex;

	sf::Sprite m_bg;
	sf::Sprite m_Cloud1;
	sf::Sprite m_fg;
	sf::Sprite m_Cloud2;

	sf::Font font;
	sf::Font font2;
	sf::Text text;
	sf::Text text1;
	sf::Text text2;
	sf::Text text3;
	sf::Text text4;
	sf::Text text5;
	sf::Text text6;
	sf::Text text7;
	sf::Text text8;
	sf::Text text9;
	sf::Text text10;
	sf::Text text11;
	sf::Text text12;
	sf::Text text13;
	sf::Text text14;
	sf::Text text15;
	sf::Text text16;
	sf::Text text17;
	sf::Text text18;
	sf::Text text19;

	sf::Texture m_controlsTex;
	sf::Sprite m_controls;

	sf::Music music;

	float musicFade = 200;
	float musicF = 0.15f;
};

#endif // CONTROLSSTATE_HPP
