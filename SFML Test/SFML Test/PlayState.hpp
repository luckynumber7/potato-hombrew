#include "State.hpp"

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML\Main.hpp>
#include <SFML\Window.hpp>
#include <SFML\System.hpp>
#include <SFML\Audio.hpp>
#include <SFML\Graphics.hpp>

#include "StateMachine.hpp"
#include "GameOverState.hpp"

#include "SpriteManager.hpp"
#include "TextureManager.hpp"

#include "Player.hpp"
#include "Clouds.hpp"
#include "EnemyPlane.hpp"
#include "EnemyTurret.hpp"
#include "EnemyZeppelin.hpp"
#include "AIStates.hpp"
#include "IEnemy.hpp"
#include "Particle.hpp"

namespace sf
{
	class RenderWindow;
}

class PlayState : public State
{
public:
	PlayState(StateMachine& machine, sf::RenderWindow& window, TextureManager* p_textureManager, bool replace = true, bool restart = false);
	~PlayState();

	void pause();
	void resume();

	void update();
	void draw();

	void respawnEnemies();
	sf::Vector2i randPos();

	sf::View View1;

private:
	SpriteManager* m_sprite_manager;
	Particle m_dollarParticles[10];

	//Sound/Music
	sf::Music music;
	sf::Sound m_kachingSound;
	sf::Sound m_plopSound;
	sf::Sound m_potatoGetSound;
	sf::Sound m_potatoDropSound;
	sf::Sound m_potatoSuckSound;
	sf::Sound m_fanfareSound;

	sf::SoundBuffer m_kachingBuffer;
	sf::SoundBuffer m_plopBuffer;
	sf::SoundBuffer m_potatoGetBuffer;
	sf::SoundBuffer m_potatoDropBuffer;
	sf::SoundBuffer m_potatoSuckBuffer;
	sf::SoundBuffer m_fanfareBuffer;

	//Textures
	sf::Texture* m_bgTex;
	sf::Texture* m_homeFarmTex;
	sf::Texture* m_GUIbackTex;
	sf::Texture* m_GUIcompassTex;
	sf::Texture* m_GUIpotatoesTex;
	sf::Texture* m_GUIhealthTex;
	sf::Texture* m_GUIglassTex;
	sf::Texture* m_GUIpowerups;
	sf::Texture* m_GUIscoreTex;
	sf::Texture* m_distilleryTex;
<<<<<<< HEAD
=======
	sf::Texture* m_potatoGetTex;
	//sf::Texture* m_returnDialogTex;
	//sf::Texture* m_deliverDialogTex;
	//sf::Texture* m_potatoDialogTex;
>>>>>>> Pre-final_polish_branch_-_Erik_-_2

	sf::Texture* m_deathDialogTex;
	sf::Texture* m_compassTex;
	sf::Texture* m_healthLightTex;
	sf::Texture* m_potatoLightTex;
	sf::Texture* m_cloudCornerTex;
	sf::Texture* m_dollarTex;
	sf::Texture* m_newspaperTex;
	sf::Texture* m_newspaperSpinTex;
	sf::Texture posButtTex;
	sf::Texture negButtTex;
	sf::Texture sellButtTex;
	sf::Texture buttTipsTex;


	//Sprites
	sf::Sprite m_bg;
	sf::Sprite m_GUIback;
	sf::Sprite m_GUIcompass;
	sf::Sprite m_GUIpotatoes;
	sf::Sprite* m_potatoLight;
	sf::Sprite* m_homeFarm;
	sf::Sprite m_GUIhealth;
	sf::Sprite* m_healthLight;
	sf::Sprite m_GUIglass;
	sf::Sprite m_GUIbakedPotato;
	sf::Sprite m_GUIbooze;
	sf::Sprite m_GUIpotatoCloud;
	sf::Sprite m_GUIscore;
	sf::Sprite m_distillery;
	sf::Sprite m_distillery1;
	sf::Sprite m_distillery2;
	sf::Sprite m_distillery3;
	sf::Sprite* m_distilleryOn;
	sf::Sprite m_returnDialog;
	sf::Sprite m_deliveryDialog;
	sf::Sprite m_potatoDialog;
	sf::Sprite* m_newspaper;
	sf::Sprite* m_potatoGet;
	
	sf::Sprite m_deathDialog;
	sf::Sprite m_compass;
	sf::Sprite m_cloudCorners[4];
	sf::Sprite m_posPotatoButt;
	sf::Sprite m_negPotatoButt;
	sf::Sprite m_sellPotatoButt;
	sf::Sprite m_buttTips;

	sf::Clock farmAnim;
	sf::Clock m_respawnTimer;
	sf::Vector2i m_newspaperAnimation;
	sf::Vector2i m_potatoGetAnimation;

	Player* m_player;

	std::vector<Clouds*> m_clouds;
	std::vector<IEnemy*> m_enemies;

	sf::Vector2i m_homeFarmAni;

	sf::Sprite* m_scoreHUD;
	sf::Vector2i m_scoreHUDani;
	sf::Clock scoreHUDclock;

	sf::Texture m_arrowTex;
	sf::Sprite* m_arrow;
	sf::Vector2i m_arrowAni;
	sf::Clock arrowAnim;

	sf::Texture* m_potatoDropTex;
	sf::Sprite* m_potatoDropSpr;
	sf::Vector2i m_potatoDropFrames;
	sf::Clock m_potatoDropClock;

	sf::Texture m_potatoTex;
	sf::Sprite* m_potato;
	sf::Vector2i m_potatoAni;
	sf::Clock potatoAnim;

	sf::Texture m_potatoStillTex;
	sf::Sprite m_potatoStill;

	sf::Clock m_clock;
	sf::Time m_timer = sf::seconds(3);

	sf::Clock m_clock2;
	sf::Time m_timer2 = sf::seconds(1);

	sf::Font font;
	sf::Font font1;
	sf::Font hndWrtng;

	sf::Text text;
	sf::Text cargoPotato;
	sf::Text sellPotato;

	sf::Text Cargo;
	sf::Text Potatoes;

	sf::Text potatSell;
	sf::Text payoutText;

	sf::Text text5;
	sf::Text scoreText;

	sf::Text enemyReinforcements;
	sf::IntRect screenrect;

	std::string potat;
	std::string cash;
	std::string sellpotat;

	sf::Vector2i m_potatoLightAni;
	sf::Vector2i m_healthLightAni;
	sf::Clock m_healthClock;

	sf::Clock m_potatoClock;

	sf::Vector2i m_distillAni;

	sf::Clock distillAnim;

	sf::Clock deliverClock;
	sf::Clock returnClock;

	sf::Clock dialogDelay;

	sf::Clock tutorialDelay;

	sf::Texture m_returnDialogTex;
	sf::Texture m_deliverDialogTex;

	sf::Texture m_tutor1Tex;
	sf::Texture m_tutor2Tex;
	sf::Texture m_tutor3Tex;

	sf::Sprite m_tutor1;
	sf::Sprite m_tutor2;
	sf::Sprite m_tutor3;

	sf::Texture m_potatoDialogTex;

	sf::Event event;

	std::ofstream outCash;

	float m_potatoes = 100;
	float m_potatoDown = 0;
	float m_potatoMax;

	float m_cash;

	float m_coolDown = 600;
	float m_Timer = 0;
	float m_expTimer = 0;
	float m_GUIcooldownDraw = 0.0916f;

	float m_cloudCooldown = 0.0f;

	float m_pauseTime = 0;

	bool m_gamePaused = false;
	bool m_potatoDrop = false;

	bool m_tutorial = false;
	float m_tutorialBox = 4;

	bool m_delivery = false;
	bool m_respawn = false;

	bool GUIdamage = false;
	bool potatoUse = false;

	bool m_hidden = false;

<<<<<<< HEAD
	bool m_potatoesDropping = false;
=======
	bool m_flyingDollars = false;
	bool m_scoreHUDAnimationCheck = false;
	bool m_potatoGetCheck = false;
>>>>>>> Pre-final_polish_branch_-_Erik_-_2

	float m_Taters;

	float payout;
	std::string payoutString;

	int hoverButton = 0;

	int deathCounter = 0;

	float m_soldPotatoes = 0;

	bool m_potatoAnimate = false;

};
