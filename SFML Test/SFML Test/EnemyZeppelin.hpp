#pragma once
#include "IEnemy.hpp"
#include "TextureManager.hpp"

class AIStates;
class Player;
class Projectile;
class Bullet;


class EnemyZeppelin : public IEnemy
{
public:

	EnemyZeppelin(Player* p_player, TextureManager* p_textureManager, float p_posX, float p_posY);
	~EnemyZeppelin();
	void Initialize(sf::Sprite* p_sprite);
	void Update();
	sf::Sprite* GetSprite();
	sf::RectangleShape GetHitBox();
	EENEMY GetType();

	bool IsAlerted();
	void SetAlerted(bool p_alerted);
	sf::CircleShape GetSightRadius();
	void SetSightRadius(float p_radius);
	int GetHP();
	void SetHP(int p_amount);

	sf::Time GetTimer();

	void SetPosition(float p_posX, float p_posY);
	void AddX(float p_x);
	void AddY(float p_y);
	float GetX();
	float GetY();
	float GetCenterX();
	float GetCenterY();
	void SetX(float p_x);
	void SetY(float p_y);
	float GetSpeed();
	void MoveTowards(sf::Vector2f p_pos);
	void SightCheck(sf::Sprite* p_sprite);

	bool ProjectileCollision();

	void Draw(sf::RenderWindow& p_window);
	void PauseSounds();
	void SetState(AIStates* p_state);

private:

	TextureManager* m_textureManager;

	bool m_alerted;
	bool m_stateCheck;
	float m_speed;
	float m_turningSpeed;
	int m_HP;
	int m_damageFrames;

	AIStates* m_state;
	Player* m_player;
	sf::Sprite* m_sprite;
	sf::Sprite m_searchLight;

	sf::Texture* m_texture;
	sf::Texture* m_searchLightTex;
	sf::Texture* m_deathTex;

	sf::CircleShape m_sightRadius;
	sf::RectangleShape m_hitbox;

	sf::Vector2f m_pos;
	sf::Vector2f m_center;
	sf::Vector2i m_animation;
	//sf::Vector2f m_waypoint;

	sf::SoundBuffer m_alarm;
	sf::SoundBuffer m_death;
	sf::Sound m_deathSound;
	sf::Sound m_alarmSound;

	sf::Clock m_clock;
	sf::Clock m_animationTimer;
	sf::Clock m_animationDelay;
	sf::Time m_invincibilityFrames;
	sf::Time m_deathTimer;
	sf::Time m_timer;
	sf::Time m_alarmDelay;


};