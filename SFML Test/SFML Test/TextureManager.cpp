#include "TextureManager.hpp"

TextureManager::TextureManager()
{
}

TextureManager::~TextureManager()
{
	auto it = m_textures.begin();
	while (it != m_textures.end())
	{
		delete (*it).second;
		it++;
	}
	m_textures.clear();
}

void TextureManager::PreLoadTextures()
{
	//INTROSTATE
	//Idle Buttons
	CreateTexture("../assets/MenuButtIdle.png");

	//Active Buttons
	CreateTexture("../assets/StartHover.png");
	CreateTexture("../assets/ControlsHover.png");
	CreateTexture("../assets/ScoreHover.png");
	CreateTexture("../assets/CreditsHover.png");
	CreateTexture("../assets/QuitHover.png");

	//Background, logo etc;
	CreateTexture("../assets/Background.png");
	CreateTexture("../assets/Title2.png");
	CreateTexture("../assets/Cloud1.png");
	CreateTexture("../assets/Foreground.png");
	CreateTexture("../assets/Cloud.png");

	//SCORESTATE
	//Dialog
	CreateTexture("../assets/highscoreDialog.png");

	//MENUSTATE


	//PLAYSTATE
	//Player
	CreateTexture("../assets/PlayerPlane Spritesheet.png");
	CreateTexture("../assets/PlayerPlane Death Animation Spritesheet.png");
	CreateTexture("../assets/PotatoBullet.png");
	CreateTexture("../assets/Baked Potato sprite.png");
	CreateTexture("../assets/Baked potato explosion sprite sheet.png");
	CreateTexture("../assets/Potato cloud sprite sheet.png");
	CreateTexture("../assets/Bullet Impact Spritesheet.png");
	CreateTexture("../assets/PlayerPlane Spritesheet.png");
	CreateTexture("../assets/Speed booze sprite sheet.png");

	//Planes, common
	CreateTexture("../assets/Propeller animation sprite sheet.png");

	//Enemy, Common
	CreateTexture("../assets/Middle Bullet.png");
	CreateTexture("../assets/Search Light Sprite sheet.png");

	//Enemy Plane
	CreateTexture("../assets/Fighter Plane Normal.png");
	CreateTexture("../assets/Fighter Plane Damaged Sprite Sheet.png");
	CreateTexture("../assets/Fighter Plane Death Scene Sprite Sheet.png");
	CreateTexture("../assets/Plane Search Light.png");

	//Enemy Turret
	CreateTexture("../assets/Anti Aircraft Artillery Sprite Sheet.png");

	//Enemy Zeppelin
	CreateTexture("../assets/zeppelin sprite sheet.png");
	CreateTexture("../assets/Zeppelin death animation sprite sheet.png");

	//Clouds
	CreateTexture("../assets/Clouds/Clouds1.png");
	CreateTexture("../assets/Clouds/Clouds2.png");
	CreateTexture("../assets/Clouds/Clouds3.png");
	CreateTexture("../assets/Clouds/Clouds4.png");
	CreateTexture("../assets/Clouds/Clouds5.png");
	CreateTexture("../assets/Clouds/Clouds6.png");
	CreateTexture("../assets/Clouds/Clouds7.png");
	CreateTexture("../assets/Clouds/cloud corners.png");

	//Home base and destillery
	CreateTexture("../assets/Farm spritesheet 2.png");
	CreateTexture("../assets/Distillery Sprite Sheet.png");
	
	//Dropping potatoes animations
	CreateTexture("../assets/potatoDropSpritesheet.png");

	//GUI
	CreateTexture("../assets/HealthLight.png");
	CreateTexture("../assets/PotatoLight.png");
	CreateTexture("../assets/Compass V2.png");
	CreateTexture("../assets/GUIback3.png");
	CreateTexture("../assets/health.png");
	CreateTexture("../assets/potato.png");
	CreateTexture("../assets/Power Up buttons upplysta.png");
	CreateTexture("../assets/GUIback3.png");
	CreateTexture("../assets/compass.png");
	CreateTexture("../assets/health.png");
	CreateTexture("../assets/potato.png");

	//GUI - Dialogue

	//Background
	CreateTexture("../assets/fullmap.png");
	CreateTexture("../assets/PlayerPlane Spritesheet.png");
	CreateTexture("../assets/PlayerPlane Spritesheet.png");
	CreateTexture("../assets/PlayerPlane Spritesheet.png");
	CreateTexture("../assets/PlayerPlane Spritesheet.png");
	CreateTexture("../assets/PlayerPlane Spritesheet.png");

	//Misc
	CreateTexture("../assets/$ sprite sheet.png");
	CreateTexture("../assets/Newspaper spin and headlines sprite sheet.png");
	CreateTexture("../assets/newspapers.png");
	CreateTexture("../assets/Get Potatoes Sprite Sheet.png");
}

void TextureManager::CreateTexture(std::string p_filepath)
{
	auto it = m_textures.find(p_filepath);
	if (it == m_textures.end())
	{
		sf::Texture* texture = new sf::Texture;
		if (!texture->loadFromFile(p_filepath))
		{
			printf("ERROR LOADING IMAGE");
		}
		texture->setSmooth(true);
		m_textures.insert(std::pair<std::string, sf::Texture*>(p_filepath, texture));
		it = m_textures.find(p_filepath);
	}
}

void TextureManager::DestroyTexture(std::string p_filepath)
{

}

sf::Texture* TextureManager::LoadTexture(std::string p_filepath)
{
	auto it = m_textures.find(p_filepath);
	sf::Texture* texture = it->second;
	return texture;
}
