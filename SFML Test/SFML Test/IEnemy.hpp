#pragma once
class AIStates;

enum EENEMY
{
	ENEMY_PLANE,
	ENEMY_TURRET,
	ENEMY_ZEPPELIN
};
class IEnemy
{
public:
	virtual ~IEnemy() {};
	virtual void Update() = 0;

	virtual sf::Sprite* GetSprite() = 0;
	virtual sf::RectangleShape GetHitBox() = 0;
	virtual EENEMY GetType() = 0;
	
	virtual bool IsAlerted() = 0;
	virtual void SetAlerted(bool p_alerted) = 0;

	virtual sf::CircleShape GetSightRadius() = 0;
	virtual void SetSightRadius(float p_radius) = 0;
	virtual int GetHP() = 0;
	virtual void SetHP(int p_amount) = 0; 

	virtual sf::Time GetTimer() = 0;

	virtual void SetPosition(float p_posX, float p_posY) = 0;
	virtual void AddX(float p_x) = 0;
	virtual void AddY(float p_y) = 0;
	virtual float GetX() = 0;
	virtual float GetY() = 0;
	virtual float GetCenterX() = 0;
	virtual float GetCenterY() = 0;
	virtual void MoveTowards(sf::Vector2f p_pos) = 0;
	virtual void SightCheck(sf::Sprite* p_sprite) = 0;

	virtual bool ProjectileCollision() = 0;

	virtual void Draw(sf::RenderWindow& p_window) = 0;
	virtual void PauseSounds() = 0;
	virtual void SetState(AIStates* p_state) = 0;

private:

};