#pragma once
#ifndef GAMEENGINE_HPP
#define GAMEENGINE_HPP

#include <memory>
#include <stack>
#include <string>

#include <SFML\Audio.hpp>
#include "TextureManager.hpp"
class State;

namespace sf
{
	class RenderWindow;
}

class StateMachine
{
public:
	StateMachine();

	void run(std::unique_ptr<State> state);

	void nextState();
	void lastState();

	void update();
	void draw();

	bool running() { return m_running; }
	void quit() { m_running = false; }

	void clear();

	template <typename T>
	static std::unique_ptr<T> build(StateMachine& machine, sf::RenderWindow& window, TextureManager* p_textureManager, bool replace = true, bool restart = false);

private:
	// The stack of states
	std::stack<std::unique_ptr<State>> m_states;

	bool m_resume;
	bool m_running;
};

template <typename T>
std::unique_ptr<T> StateMachine::build(StateMachine& machine, sf::RenderWindow& window, TextureManager* p_textureManager, bool replace, bool restart)
{
	return std::unique_ptr<T>(new T(machine, window, p_textureManager, replace, restart));
}

#endif // GAMEENGINE_HPP