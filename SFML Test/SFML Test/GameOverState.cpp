#include <memory>
#include <fstream>
#include <string>
#include <sstream>
#include <iostream>

#include "IntroState.hpp"
#include "GameOverState.hpp"
#include "StateMachine.hpp"
#include "stdafx.hpp"


using namespace std;

GameOverState::GameOverState(StateMachine& machine, sf::RenderWindow& window, TextureManager* p_textureManager, float p_cash, bool replace, bool restart)
	: State{ machine, window, p_textureManager, replace, restart}
{
	if (!font.loadFromFile("../assets/Fonts/Caladea-Bold.ttf"))
	{
		cout << "Error: Could not load font" << endl;
	}
	if (!font2.loadFromFile("../assets/Fonts/Caladea-Regular.ttf"))
	{
		cout << "Error: Could not load font" << endl;
	}

	if (!music.openFromFile("../assets/music/AmazingGrace.wav"))
	{

		std::cout << "Could not load music file" << std::endl;
	}

	music.setVolume(50);        //Reduce the volume
	music.setLoop(true);		//Set Loop

	music.play();

	View1 = sf::View(sf::Vector2f(960, 540), sf::Vector2f(1920, 1080));
	View1.setCenter(960, 540);

	window.setView(View1);

	text.setFont(font);
	text.setString("Game Over!");
	text.setColor(sf::Color::White);
	text.setCharacterSize(70);
	text.setPosition(290, 120);

	text1.setFont(font2);
	text1.setString("You scored:");
	text1.setColor(sf::Color::White);
	text1.setCharacterSize(60);
	text1.setPosition(320, 200);

	text2.setFont(font2);
	text2.setColor(sf::Color::White);
	text2.setCharacterSize(60);
	text2.setPosition(400, 280);

	namesText.setFont(font2);
	namesText.setColor(sf::Color::White);
	namesText.setCharacterSize(50);
	namesText.setPosition(150, 480);

	scoresText.setFont(font2);
	scoresText.setColor(sf::Color::White);
	scoresText.setCharacterSize(50);
	scoresText.setPosition(575, 480);



	cout << m_fScore << endl;
	cout << scoresInt4 << endl;




	m_dialogTex.loadFromFile("../assets/gameoverDialog.png");
	m_dialog.setTexture(m_dialogTex, true);
	m_dialog.setPosition(100, 100);

	m_bgTex.loadFromFile("../assets/GameOverSepia.png");
	m_bg.setTexture(m_bgTex, true);
}

void GameOverState::pause()
{

}

void GameOverState::resume()
{

}

void GameOverState::update()
{
	inname.open("../assets/Scoring/Names.txt");
	inscore.open("../assets/Scoring/Scores.txt");
	incash.open("../assets/cash.txt");

	getline(incash, cash);

	m_fScore = atoi(cash.c_str());

	incash.close();

	for (int i = 0; i <= 4; i++) // To get you all the lines.
	{
		getline(inname, Names[i]);// Saves the line in Names[0-4].

		actNString.append(Names[i]);
		actNString.append("\n");

		namesText.setString(actNString);
	}
	inname.close();

	for (int k = 0; k <= 4; k++)
	{
		getline(inscore, Scores[k]);// Saves the line in Scores[0-4].

		actSString.append(Scores[k]);
		actSString.append("\n");

		scoresText.setString(actSString);
	}
	inscore.close();

	actNString.erase(0, actNString.length());
	actSString.erase(0, actSString.length());

	scoresInt = atoi(Scores[0].c_str());
	scoresInt1 = atoi(Scores[1].c_str());
	scoresInt2 = atoi(Scores[2].c_str());
	scoresInt3 = atoi(Scores[3].c_str());
	scoresInt4 = atoi(Scores[4].c_str());

	if (m_fScore > scoresInt4)
	{
		enterText.setFont(font2);
		enterText.setString("Enter name:");
		enterText.setColor(sf::Color::White);
		enterText.setCharacterSize(50);
		enterText.setPosition(145, 360);

		playerText.setFont(font2);
		playerText.setColor(sf::Color::White);
		playerText.setCharacterSize(50);
		playerText.setPosition(415, 360);
	}

	cash = to_string(m_fScore);


	if (m_fScore < 10)
	{
		text2.setPosition(460, 280);
		cash.resize(1);
	}
	else if (m_fScore < 100)
	{
		text2.setPosition(440, 280);
		cash.resize(2);
	}
	else if (m_fScore < 1000)
	{
		text2.setPosition(420, 280);
		cash.resize(3);
	}
	else if (m_fScore < 10000)
	{
		text2.setPosition(400, 280);
		cash.resize(4);
	}
	else if (m_fScore < 100000)
	{
		text2.setPosition(380, 280);
		cash.resize(5);
	}

	//	text2.setString(m_write);

	sf::Event event;

	while (m_window.pollEvent(event))
	{

		if (event.type == sf::Event::TextEntered)
		{
			if (m_fScore >= scoresInt4);
			{
				// Handle ASCII characters only
				if (event.text.unicode < 128)
				{
					char ecter = static_cast<char>(event.text.unicode);
					/*		written = to_string(ecter);
					m_write.append(written);
					text2.setString(m_write); */

					if (ecter == (char)8)
					{
						if (m_write.size() != 0)
							m_write.pop_back();

						playerText.setString(m_write);
					}
					else if (ecter == (char)32)
					{
						m_write.append(" ");
						playerText.setString(m_write);
					}
					else if (ecter == (char)13)
					{
					}
					else
					{
						stringstream ss;

						ss << ecter;
						ss >> written;

						if(m_write.length() < 10)
							m_write.append(written);

						playerText.setString(m_write);
					}

				}
			}
		}

		switch (event.type)
		{

		case sf::Event::KeyPressed:
			switch (event.key.code)
			{
			case sf::Keyboard::Return:

				m_saved = m_write;
				m_write.erase(0, m_write.length());
				playerText.setString(m_write);
				cout << m_saved << endl;
				cout << m_write << endl;

				if (m_fScore > scoresInt && writeable)
				{
					if (m_fScore < 10)
						cash.resize(1);
					else if (m_fScore < 100)
						cash.resize(2);
					else if (m_fScore < 1000)
						cash.resize(3);
					else if (m_fScore < 10000)
						cash.resize(4);
					else if (m_fScore < 100000)
						cash.resize(5);

					Scores[4] = Scores[3];
					Scores[3] = Scores[2];
					Scores[2] = Scores[1];
					Scores[1] = Scores[0];
					Scores[0] = cash;

					Names[4] = Names[3];
					Names[3] = Names[2];
					Names[2] = Names[1];
					Names[1] = Names[0];
					Names[0] = m_saved;

					ofstream outnames("../assets/Scoring/Names.txt");
					for (int j = 0; j <= 4; j++) // To get you all the lines.
					{
						outnames << Names[j] << endl;
					}
					outnames.close();

					ofstream outscore("../assets/Scoring/Scores.txt");
					for (int k = 0; k <= 4; k++) // To get you all the lines.
					{
						outscore << Scores[k] << endl;
					}
					outscore.close();

					writeable = false;
				}
				else if (m_fScore > scoresInt1 && writeable)
				{
					if (m_fScore < 10)
						cash.resize(1);
					else if (m_fScore < 100)
						cash.resize(2);
					else if (m_fScore < 1000)
						cash.resize(3);
					else if (m_fScore < 10000)
						cash.resize(4);
					else if (m_fScore < 100000)
						cash.resize(5);

					Scores[4] = Scores[3];
					Scores[3] = Scores[2];
					Scores[2] = Scores[1];
					Scores[1] = cash;

					Names[4] = Names[3];
					Names[3] = Names[2];
					Names[2] = Names[1];
					Names[1] = m_saved;

					ofstream outnames("../assets/Scoring/Names.txt");
					for (int j = 0; j <= 4; j++) // To get you all the lines.
					{
						outnames << Names[j] << endl;
					}
					outnames.close();

					ofstream outscore("../assets/Scoring/Scores.txt");
					for (int j = 0; j <= 4; j++) // To get you all the lines.
					{
						outscore << Scores[j] << endl;
					}
					outscore.close();

					writeable = false;
				}
				else if (m_fScore > scoresInt2 && writeable)
				{
					if (m_fScore < 10)
						cash.resize(1);
					else if (m_fScore < 100)
						cash.resize(2);
					else if (m_fScore < 1000)
						cash.resize(3);
					else if (m_fScore < 10000)
						cash.resize(4);
					else if (m_fScore < 100000)
						cash.resize(5);

					Scores[4] = Scores[3];
					Scores[3] = Scores[2];
					Scores[2] = cash;

					Names[4] = Names[3];
					Names[3] = Names[2];
					Names[2] = m_saved;

					ofstream outnames("../assets/Scoring/Names.txt");
					for (int j = 0; j <= 4; j++) // To get you all the lines.
					{
						outnames << Names[j] << endl;
					}
					outnames.close();

					ofstream outscore("../assets/Scoring/Scores.txt");
					for (int j = 0; j <= 4; j++) // To get you all the lines.
					{
						outscore << Scores[j] << endl;
					}
					outscore.close();

					writeable = false;
				}
				else if (m_fScore > scoresInt3 && writeable)
				{
					if (m_fScore < 10)
						cash.resize(1);
					else if (m_fScore < 100)
						cash.resize(2);
					else if (m_fScore < 1000)
						cash.resize(3);
					else if (m_fScore < 10000)
						cash.resize(4);
					else if (m_fScore < 100000)
						cash.resize(5);

					Scores[4] = Scores[3];
					Scores[3] = cash;

					Names[4] = Names[3];
					Names[3] = m_saved;

					ofstream outnames("../assets/Scoring/Names.txt");
					for (int j = 0; j <= 4; j++) // To get you all the lines.
					{
						outnames << Names[j] << endl;
					}
					outnames.close();

					ofstream outscore("../assets/Scoring/Scores.txt");
					for (int j = 0; j <= 4; j++) // To get you all the lines.
					{
						outscore << Scores[j] << endl;
					}
					outscore.close();

					writeable = false;
				}
				else if (m_fScore > scoresInt4 && writeable)
				{
					if (m_fScore < 10)
						cash.resize(1);
					else if (m_fScore < 100)
						cash.resize(2);
					else if (m_fScore < 1000)
						cash.resize(3);
					else if (m_fScore < 10000)
						cash.resize(4);
					else if (m_fScore < 100000)
						cash.resize(5);

					Scores[4] = cash;

					Names[4] = m_saved;

					ofstream outnames("../assets/Scoring/Names.txt");
					for (int j = 0; j <= 4; j++) // To get you all the lines.
					{
						outnames << Names[j] << endl;
					}
					outnames.close();

					ofstream outscore("../assets/Scoring/Scores.txt");
					for (int j = 0; j <= 4; j++) // To get you all the lines.
					{
						outscore << Scores[j] << endl;
					}
					outscore.close();

					writeable = false;
				}

				break;

				default:
					break;
			}
			break;

		default:
			break;
		}
	}

	text2.setString(cash);

	m_bg.setPosition(0, 0);

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
	{
		m_next = StateMachine::build<IntroState>(m_machine, m_window, m_textureManager, true, true);
	}
}

void GameOverState::draw()
{

	m_window.clear();
	m_window.setView(View1);
	m_window.draw(m_bg);
	m_window.draw(m_dialog);
	m_window.draw(text);
	m_window.draw(text1);
	m_window.draw(text2);
	m_window.draw(namesText);
	m_window.draw(scoresText);




	if (m_fScore >= scoresInt4 && writeable)
	{
		m_window.draw(playerText);
		m_window.draw(enterText);

		enterText.setPosition(145, 360);
		playerText.setPosition(415, 360);
	}
	m_window.display();
}