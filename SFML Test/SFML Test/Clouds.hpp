#pragma once

#include "stdafx.hpp"
#include "Player.hpp"
#include "TextureManager.hpp"

class Clouds
{
public:
	Clouds(float p_posX, float p_posY, TextureManager* p_textureManager, Player* p_player);
	~Clouds();
	bool Update();

	void SetPosition(float p_posX, float p_posY);
	void AddX(float p_x);
	void AddY(float p_y);
	float GetX();
	float GetY();
	float GetCenterX();
	float GetCenterY();/*
	float GetHideBoxX();
	float GetHideBoxY();
	float GetHideBoxWidth();
	float GetHideBoxHeight();*/
	float GetSpeed();
	void SetSpeed(float p_speed);
	sf::Sprite* GetSprite();

	void Draw(sf::RenderWindow& p_window);

private:
	TextureManager* m_textureManager;

	sf::Sprite* m_sprite;
	Player* m_player;
	sf::RectangleShape m_hitbox;

	sf::Texture* m_texture;

	sf::Vector2f m_pos;
	sf::Vector2f m_center;

	//sf::Vector2f m_hide;

	//float m_hideHeight;
	//float m_hideWidth;

	float m_speed;
	float f = 255;
};