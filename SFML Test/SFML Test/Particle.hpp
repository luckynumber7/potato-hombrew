#pragma once
#include "stdafx.hpp"

struct Particle
{
	sf::Sprite* m_sprite = nullptr;
	sf::Clock lifetime;
	sf::Clock animationTimer;
	sf::Vector2i animation;
	bool active = false;
	float speed;
	void init(sf::Texture* p_texture, sf::IntRect p_textureRect, int x_frames, int y_frames, float p_speed)
	{
		animation.x = x_frames;
		animation.y = y_frames;
		m_sprite = new sf::Sprite;
		m_sprite->setTexture(*p_texture);
		m_sprite->setTextureRect(p_textureRect);
		active = true;
		speed = p_speed;
	}
	bool move(sf::Vector2f p_pos, bool p_timerOnOff, float p_duration, float p_speedMod)
	{
		sf::Vector2f pos = m_sprite->getPosition();
		if (p_pos.x > pos.x)
			pos.x += speed + p_speedMod;
		else if (p_pos.x < pos.x)
			pos.x -= speed + p_speedMod;
		if (p_pos.y > pos.y)
			pos.y += speed + p_speedMod;
		else if (p_pos.y < pos.y)
			pos.y -= speed + p_speedMod;
		m_sprite->setPosition(pos);
		if (abs(m_sprite->getPosition().x - p_pos.x) < 20 && abs(m_sprite->getPosition().y - p_pos.y) < 20)
		{
			active = false;
			delete m_sprite;
			m_sprite = nullptr;
			return false;
		}
		else if (p_timerOnOff)
		{
			if (lifetime.getElapsedTime().asMilliseconds() > p_duration)
			{
				active = false;
				delete m_sprite;
				m_sprite = nullptr;
				return false;
			}
		}
		else
		{
			return true;
		}
	}
};