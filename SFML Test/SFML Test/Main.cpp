
#include <SFML\Main.hpp>
#include <SFML\Window.hpp>
#include <SFML\System.hpp>
#include <SFML\Audio.hpp>
#include <SFML\Graphics.hpp>
/*
#if !defined(NDEBUG)
#pragma comment(lib, "sfml-main-d.lib")
#pragma comment(lib, "sfml-window-d.lib")
#pragma comment(lib, "sfml-system-d.lib")
#pragma comment(lib, "sfml-audio-d.lib")
#pragma comment(lib, "sfml-graphics-d.lib")
#else
#pragma comment(lib, "sfml-main.lib")
#pragma comment(lib, "sfml-window.lib")
#pragma comment(lib, "sfml-system.lib")
#pragma comment(lib, "sfml-audio.lib")
#pragma comment(lib, "sfml-graphics.lib")
#endif


int main(int argc, char** argv) {

	sf::RenderWindow window(sf::VideoMode(640, 480), "SFML is the best");
	sf::CircleShape shape;
	shape.setRadius(10.f);
	shape.setPosition(100.f, 100.f);
	shape.setFillColor(sf::Color::Cyan);
/*
	if (window.isOpen()) return -1;

	float accumulator = 1.0f / 60.0f;
	float targettime = 0.0f;
	float frametime = 0.0f;

	sf::Clock clock;


	while (window.isOpen()) {
		/*
		sf::Time deltaTime = clock.restart();
		frametime = deltaTime.asSeconds();
		if (frametime > 0.1f)
			frametime = 0.1f;
		accumulator += frametime;
		while (accumulator > targettime)
		{
			accumulator -= targettime;
			sf::Event event;
			while (window.pollEvent(event)) 
			{
				if (event.type == sf::Event::Closed)
					window.close();
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
				{
					shape.move(10, 0);
				}
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
				{
					shape.move(0, -10);
				}
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
				{
					shape.move(-10, 0);
				}
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
				{
					shape.move(0, 10);
				}
			}
	//	}
		
		window.clear();
		window.draw(shape);
		window.display();
	}
	return 0;
}*/

#include "Application.hpp"

int main()
{
	Application app;
	app.run();
}