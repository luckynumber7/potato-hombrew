#include <memory>
#include <iostream>
#include <string>

#include "PlayState.hpp"
#include "MenuState.hpp"

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML\Main.hpp>
#include <SFML\Window.hpp>
#include <SFML\System.hpp>
#include <SFML\Audio.hpp>
#include <SFML\Graphics.hpp>
#include <math.h>
#include <ctime>
#include <stdlib.h>

PlayState::PlayState(StateMachine& machine, sf::RenderWindow& window, TextureManager* p_textureManager, bool replace, bool restart)
	: State{ machine, window, p_textureManager, replace, restart }
{
	srand((unsigned int)time(NULL));

	View1 = sf::View(sf::Vector2f(960, 540), sf::Vector2f(1920, 1080));
	View1.setCenter(960, 540);

	window.setView(View1);

	screenrect = sf::IntRect(3200, 0, 2000, 1325);
	
	m_sprite_manager = new SpriteManager();

	if (!music.openFromFile("../assets/music/BackToTheWoods.wav"))
	{

		//std::cout << "Could not load music file" << std::endl;
	}

	music.setVolume(10);        //Reduce the volume
	music.setLoop(true);		//Set Loop
	music.play();
	
	m_kachingBuffer.loadFromFile("../assets/sounds/Ka-ching Sound.wav");
	m_kachingSound.setBuffer(m_kachingBuffer);

	m_plopBuffer.loadFromFile("../assets/sounds/Confirm Sound.wav");
	m_plopSound.setBuffer(m_plopBuffer);
	m_plopSound.setVolume(70);

	m_potatoGetBuffer.loadFromFile("../assets/sounds/Get Potatoes Sound.wav");
	m_potatoGetSound.setBuffer(m_potatoGetBuffer);
	m_potatoGetSound.setVolume(200);

	m_potatoDropBuffer.loadFromFile("../assets/sounds/Potato Drop Sound.wav");
	m_potatoDropSound.setBuffer(m_potatoDropBuffer);

	m_potatoSuckBuffer.loadFromFile("../assets/sounds/Potato Suck Back Sound.wav");
	m_potatoSuckSound.setBuffer(m_potatoSuckBuffer);

	m_fanfareBuffer.loadFromFile("../assets/sounds/Newspaper Fanfare Sound.wav");
	m_fanfareSound.setBuffer(m_fanfareBuffer);
	m_fanfareSound.setVolume(60);

	if (!font.loadFromFile("../assets/Fonts/Caladea-Bold.ttf"))
	{
		std::cout << "ERROR! Font Caladea-Bold could not be loaded." << std::endl;
	}

	if (!font1.loadFromFile("../assets/Fonts/Caladea-Regular.ttf"))
	{
		std::cout << "ERROR! Font Caladea-Regular could not be loaded" << std::endl;
	}

	if (!hndWrtng.loadFromFile("../assets/Fonts/Segoe Print-Regular.ttf"))
	{
		std::cout << "ERROR! Font Caladea-Regular could not be loaded" << std::endl;
	}

	m_bgTex = m_textureManager->LoadTexture("../assets/fullmap.png");
	m_bg.setTexture(*m_bgTex, true);
	m_bg.setPosition(0, 0);

	m_newspaperTex = m_textureManager->LoadTexture("../assets/newspapers.png");
	m_newspaperSpinTex = m_textureManager->LoadTexture("../assets/Newspaper spin and headlines sprite sheet.png");
	m_newspaper = new sf::Sprite;
	m_newspaper->setTexture(*m_newspaperSpinTex);
	m_newspaper->setTextureRect(sf::IntRect(0, 0, 458, 458));
	m_newspaper->setOrigin(458 / 2, 458 / 2);
	m_newspaperAnimation.x = 0;
	m_newspaperAnimation.y = 0;

	m_potatoGetTex = m_textureManager->LoadTexture("../assets/Get Potatoes Sprite Sheet.png");
	m_potatoGet = new sf::Sprite;
	m_potatoGet->setTexture(*m_potatoGetTex);
	m_potatoGet->setTextureRect(sf::IntRect(0, 0, 161, 142));
	m_potatoGet->setOrigin(161 / 2, 142 / 2);
	m_potatoGetAnimation.x = 0;
	m_potatoGetAnimation.y = 0;

	m_player = new Player(4150, 462, 100, m_textureManager);

	m_potatoStillTex.loadFromFile("../assets/PotatoDropSprites.png", sf::IntRect(0, 0, 400, 400));
	m_potatoStill.setTexture(m_potatoStillTex, true);
	m_potatoStill.setOrigin(200, 200);

	negButtTex.loadFromFile("../assets/potatoButts.png", sf::IntRect(159, 0, 53, 53));
	posButtTex.loadFromFile("../assets/potatoButts.png", sf::IntRect(53, 0, 53, 53));
	sellButtTex.loadFromFile("../assets/ArrowButts.png", sf::IntRect(0, 0, 97, 128));
	buttTipsTex.loadFromFile("../assets/ButtTips.png");

	m_posPotatoButt.setTexture(posButtTex, true);
	m_negPotatoButt.setTexture(negButtTex, true);
	m_sellPotatoButt.setTexture(sellButtTex, true);
	m_buttTips.setTexture(buttTipsTex, true);

	m_potato = m_sprite_manager->CreateSprite("../assets/PotatoDropSprites.png", 0, 0, 400, 400);
	m_potato->setOrigin(200,200);

	m_homeFarmTex = m_textureManager->LoadTexture("../assets/Farm spritesheet 2.png");

	m_homeFarm = new sf::Sprite;
	m_homeFarm->setTexture(*m_homeFarmTex);
	m_homeFarm->setTextureRect(sf::IntRect(0, 0, 179, 178));

	m_potatoDropTex = m_textureManager->LoadTexture("../assets/potatoDropSpritesheet.png");

	m_potatoDropSpr = new sf::Sprite;
	m_potatoDropSpr->setTexture(*m_potatoDropTex);
	m_potatoDropSpr->setTextureRect(sf::IntRect(0, 0, 100, 100));

	m_cloudCornerTex = m_textureManager->LoadTexture("../assets/Clouds/cloud corners.png");
	for (int i = 0; i < 4; i++)
	{
		m_cloudCorners[i].setTexture(*m_cloudCornerTex);
		m_cloudCorners[i].setPosition(View1.getCenter());
	}

	for (int i = 0; i < 18; i++)
	{
		int randposX = clamp(rand() % mapWidth, 0, mapWidth);
		int randposY = clamp(rand() % mapHeight, 0 , mapHeight - 200);
		Clouds* cloud = new Clouds(randposX, randposY, m_textureManager, m_player);
		m_clouds.push_back(cloud);
	}

	for (int i = 0; i < 8; i++)
	{
		sf::Vector2i pos = randPos();

		EnemyPlane* enemy = new EnemyPlane(m_player, m_textureManager, pos.x, pos.y);
		m_enemies.push_back(enemy);
	}
	for (int i = 0; i < 2; i++)
	{
		sf::Vector2i pos = randPos();

		EnemyTurret* enemy = new EnemyTurret(m_player, m_textureManager, pos.x, pos.y);
		m_enemies.push_back(enemy);
	}
	for (int i = 0; i < 1; i++)
	{
		sf::Vector2i pos = randPos();
		
		EnemyZeppelin* enemy = new EnemyZeppelin(m_player, m_textureManager, pos.x, pos.y);
		m_enemies.push_back(enemy);
	}


	m_tutor1Tex.loadFromFile("../assets/tutorial 1.png");
	m_tutor2Tex.loadFromFile("../assets/tutorial 2.png");
	m_tutor3Tex.loadFromFile("../assets/tutorial 3.png");

	m_tutor1.setTexture(m_tutor1Tex, true);
	m_tutor2.setTexture(m_tutor2Tex, true);
	m_tutor3.setTexture(m_tutor3Tex, true);

	m_tutor1.setOrigin(m_tutor1.getGlobalBounds().width / 2, m_tutor1.getGlobalBounds().height / 2);
	m_tutor2.setOrigin(m_tutor2.getGlobalBounds().width / 2, m_tutor2.getGlobalBounds().height / 2);
	m_tutor3.setOrigin(m_tutor3.getGlobalBounds().width / 2, m_tutor3.getGlobalBounds().height / 2);

	m_arrow = m_sprite_manager->CreateSprite("../assets/ArrowSprites.png", 0, 0, 212, 200);

	m_dollarTex = m_textureManager->LoadTexture("../assets/$ sprite sheet.png");
	m_distilleryTex = m_textureManager->LoadTexture("../assets/Distillery Sprite Sheet.png");
	
	m_distillery.setTexture(*m_distilleryTex, true);
	m_distillery.setTextureRect(sf::IntRect(2490, 0, 249, 242));
	m_distillery1.setTexture(*m_distilleryTex, true);
	m_distillery1.setTextureRect(sf::IntRect(2490, 0, 249, 242));
	m_distillery2.setTexture(*m_distilleryTex, true);
	m_distillery2.setTextureRect(sf::IntRect(2490, 0, 249, 242));
	m_distillery3.setTexture(*m_distilleryTex, true);
	m_distillery3.setTextureRect(sf::IntRect(2490, 0, 249, 242));
	
	m_distillery.setOrigin(121, 119);
	m_distillery1.setOrigin(121, 119);
	m_distillery2.setOrigin(121, 119);
	m_distillery3.setOrigin(121, 119);

	m_distillery.setPosition(214, 1020);
	m_distillery1.setPosition(6340, 2495);
	m_distillery2.setPosition(7456, 947);
	m_distillery3.setPosition(1913, 2060);

	m_distilleryOn = new sf::Sprite;
	m_distilleryOn->setTexture(*m_distilleryTex);
	m_distilleryOn->setTextureRect(sf::IntRect( 0, 0, 249, 242));
	m_distilleryOn->setOrigin(122, 119);

	m_scoreHUD = m_sprite_manager->CreateSprite("../assets/scoreHud.png", 0, 0, 271, 112);
	m_scoreHUD->setOrigin(135, 0);

	m_healthLightTex = m_textureManager->LoadTexture("../assets/HealthLight.png");

	m_healthLight = new sf::Sprite;
	m_healthLight->setTexture(*m_healthLightTex);
	m_healthLight->setTextureRect(sf::IntRect(0, 0, 158, 185));
	m_healthLight->setOrigin(79, 175);

	m_potatoLightTex = m_textureManager->LoadTexture("../assets/PotatoLight.png");
	
	m_potatoLight = new sf::Sprite;
	m_potatoLight->setTexture(*m_potatoLightTex);
	m_potatoLight->setTextureRect(sf::IntRect(0, 0, 158, 185));
	m_potatoLight->setOrigin(45.5, 98);

	float distPos = rand() % 3; //121
	if (distPos == 0) //124
	{
		m_distilleryOn->setPosition(214, 1020);
	}
	else if (distPos == 1)
	{
		m_distilleryOn->setPosition(6340, 2495);
	}
	else if (distPos == 2)
	{
		m_distilleryOn->setPosition(7456, 947);
	}
	else if (distPos == 3)
	{
		m_distilleryOn->setPosition(1913, 2060);
	}

	m_compassTex = m_textureManager->LoadTexture("../assets/Compass V2.png");
	m_compass.setTexture(*m_compassTex, true);

	m_GUIbackTex = m_textureManager->LoadTexture("../assets/GUIback3.png");
	m_GUIhealthTex = m_textureManager->LoadTexture("../assets/health.png");
	m_GUIpotatoesTex = m_textureManager->LoadTexture("../assets/potato.png");


	m_GUIpowerups = m_textureManager->LoadTexture("../assets/Power Up buttons upplysta.png");
	m_GUIbackTex = m_textureManager->LoadTexture("../assets/GUIback3.png");
//	m_GUIcompassTex = m_textureManager->LoadTexture("../assets/compass.png");
	m_GUIhealthTex = m_textureManager->LoadTexture("../assets/health.png");
	m_GUIpotatoesTex = m_textureManager->LoadTexture("../assets/potato.png");
//	m_GUIglassTex = m_textureManager->LoadTexture("../assets/GUIglass.png");

	
//	m_GUIscoreTex = m_textureManager->LoadTexture("../assets/scoreGUI.png");

	m_GUIback.setTexture(*m_GUIbackTex, true);
	m_GUIhealth.setTexture(*m_GUIhealthTex, true);
	m_GUIpotatoes.setTexture(*m_GUIpotatoesTex, true);
	//m_GUIglass.setTexture(m_GUIglassTex, true);
	//m_GUIglass.setTexture(*m_GUIglassTex, true);

	m_GUIbakedPotato.setTexture(*m_GUIpowerups, true);
	m_GUIbakedPotato.setTextureRect(sf::IntRect(42, 0, 52, 54));
	m_GUIbooze.setTexture(*m_GUIpowerups, true);
	m_GUIbooze.setTextureRect(sf::IntRect(0, 0, 41, 64));
	m_GUIpotatoCloud.setTexture(*m_GUIpowerups, true);
	m_GUIpotatoCloud.setTextureRect(sf::IntRect(0, 66, 75, 58));

//	m_GUIscore.setTexture(*m_GUIscoreTex, true);

	m_GUIback.setOrigin(0, 241);
	m_GUIback.setPosition(0, 1080);

	m_compass.setOrigin(96, 126);

	m_GUIhealth.setOrigin(6, 156);

	m_GUIpotatoes.setOrigin(8, 62);
	m_GUIhealth.setRotation(85);

//	m_GUIglass.setOrigin(0, 241);
//	m_GUIglass.setPosition(0, 1080);

	m_GUIbakedPotato.setPosition(309, 1080 - 109);

	m_homeFarm->setPosition(4150, 462);

	m_potatoDialogTex.loadFromFile("../assets/scoreDialog2.png");
	m_potatoDialog.setTexture(m_potatoDialogTex, true);
	m_potatoDialog.setOrigin( 0, 0);

	Cargo.setFont(font1);
	Cargo.setString("Cargo:");
	Cargo.setColor(sf::Color::Black);
	Cargo.setCharacterSize(40);

	Potatoes.setFont(font1);
	Potatoes.setString("Potatoes:");
	Potatoes.setColor(sf::Color::Black);
	Potatoes.setCharacterSize(35);

	enemyReinforcements.setPosition(-1000, -1000);
	enemyReinforcements.setColor(sf::Color::Red);
	enemyReinforcements.setCharacterSize(75);
	enemyReinforcements.setFont(font);
	enemyReinforcements.setString("ENEMY REINFORCEMENTS INCOMING!");
	
}

PlayState::~PlayState()
{
	for (int i = 0; i < 10; i++)
	{
		if (m_dollarParticles[i].active == true)
		{
			delete m_dollarParticles[i].m_sprite;
		}
	}

	delete m_healthLight;
	delete m_potatoLight;
	delete m_distilleryOn;
	delete m_homeFarm;

	delete m_player;
	delete m_potatoGet;
	delete m_newspaper;

	for (int i = 0; i < m_enemies.size(); i++)
	{
		//m_enemies.at(i)->Shutdown();
		delete m_enemies.at(i);
	}
	m_enemies.clear();
	for (int i = 0; i < m_clouds.size(); i++)
	{
		delete m_clouds.at(i);
	}
	m_clouds.clear();

	delete m_sprite_manager;
	m_sprite_manager = nullptr;
}

void PlayState::pause()
{
	m_player->PauseSounds();
	for (int i = 0; i < m_enemies.size(); i++)
	{
		m_enemies.at(i)->PauseSounds();
	}
	music.pause();
}

void PlayState::resume()
{
	music.play();
}

void PlayState::update()
{

	m_scoreHUD->setPosition(View1.getCenter().x, View1.getCenter().y - screenHeight/2);
	
	m_Taters = m_player->GetPotatoes();
	//Return and Delivery dialog delay
	//if (dialogDelay.getElapsedTime().asSeconds() > 8)
	//{
	//	m_dialogs = true;
	//}
	//

	if (m_player->getDamageFrames() > 0)
		GUIdamage = true;
	else if (m_player->getDamageFrames() <= 0)
		GUIdamage = false;

	if (GUIdamage)
	{
		//Health blink
		if (m_healthLightAni.x < 2 && m_healthClock.getElapsedTime().asMilliseconds() > 0.150f)
		{
			m_healthClock.restart();
			m_healthLightAni.x++;
			Animation(m_healthLight, m_healthLightAni, 0, 474, 0, 158, 158, 185);
		}
		//
	}
	else
	{
		m_healthLightAni.x = 0;
	}

	if (potatoUse)//Se definition at end of update
	{
		//Potato blink
		if (m_potatoLightAni.x < 2 && m_potatoClock.getElapsedTime().asMilliseconds() > 150.0f)
		{
			m_potatoClock.restart();
			m_potatoLightAni.x++;
			Animation(m_potatoLight, m_potatoLightAni, 0, 276, 0, 114, 92, 114);
		}
		else if (m_potatoLightAni.x == 2)
		{
			m_potatoClock.restart();
			m_potatoLightAni.x = 0;
			potatoUse = false;
		}
		//
	}

	//Homefarm animation
	if (m_homeFarmAni.x < 11 && farmAnim.getElapsedTime().asMilliseconds() > 90.0f)
	{
		farmAnim.restart();
		m_homeFarmAni.x++;
		Animation(m_homeFarm, m_homeFarmAni, 0, 1760, 0, 178, 160, 178);
	}
	else if (farmAnim.getElapsedTime().asMilliseconds() > 90.0f)
	{
		farmAnim.restart();
		m_homeFarmAni.x = 0;

	}
	//

	//Dollar particles animation/update
	if (m_flyingDollars == true)
	{
		int dollarcheck = 0;
		for (int i = 0; i < 10; i++)
		{
			if (m_dollarParticles[i].active == true)
			{
				if (m_dollarParticles[i].move(m_scoreHUD->getPosition(), false, 2000, (m_player->GetSpeed() - 0.7)))
				{
					//kan s�tta n�t h�r
					if (m_dollarParticles[i].animationTimer.getElapsedTime().asMilliseconds() > 200)
					{
						clamp(m_dollarParticles[i].animation.x--, 0, 19);
						Animation(m_dollarParticles[i].m_sprite, m_dollarParticles[i].animation, 0, 1027, 0, 100, 56, 100);
						m_dollarParticles[i].animationTimer.restart();
					}
				}
				else
				{
					m_scoreHUDAnimationCheck = true;
				}
				dollarcheck++;
			}
		}
		if (dollarcheck <= 0)
		{
			m_flyingDollars = false;
		}
	}
	//

	//Potato get animation
	if (m_potatoGetCheck)
	{
		if (m_potatoGetSound.getStatus() != m_potatoGetSound.Playing && m_potatoGetAnimation.x == 0)
		{
			m_potatoGetSound.play();
		}
		m_potatoGet->setPosition(m_player->GetX(), m_player->GetY());
		if (m_potatoGetAnimation.x > 8)
		{
			m_potatoGetAnimation.x = 0;
			m_potatoGetCheck = false;
		}
		else if (m_potatoGetAnimation.x == 0 && m_respawnTimer.getElapsedTime().asSeconds() > 0.4)
		{
			Animation(m_potatoGet, m_potatoGetAnimation, 0, 1449, 0, 142, 161, 142);
			m_respawnTimer.restart();
			m_potatoGetAnimation.x++;
		}
		else if (m_respawnTimer.getElapsedTime().asSeconds() > 0.1)
		{
			Animation(m_potatoGet, m_potatoGetAnimation, 0, 1449, 0, 142, 161, 142);
			m_respawnTimer.restart();
			m_potatoGetAnimation.x++;
		}
	}
	//

	if (m_scoreHUDAnimationCheck)
	{
		if (m_scoreHUDani.x < 1 && scoreHUDclock.getElapsedTime().asMilliseconds() > 220.0f)
		{
			scoreHUDclock.restart();
			m_scoreHUDani.x++;
			Animation(m_scoreHUD, m_scoreHUDani, 0, 542, 0, 112, 271, 112);
			m_plopSound.play();
		}
		else if (scoreHUDclock.getElapsedTime().asMilliseconds() > 220.0f)
		{
			scoreHUDclock.restart();
			m_scoreHUDani.x = 0;
			Animation(m_scoreHUD, m_scoreHUDani, 0, 542, 0, 112, 271, 112);
			m_scoreHUDAnimationCheck = false;
		}
	}


	m_potato->setPosition(View1.getCenter().x - 10, View1.getCenter().y - 120);
	m_potatoStill.setPosition(View1.getCenter().x - 10, View1.getCenter().y - 120);

	m_negPotatoButt.setPosition(m_potatoStill.getPosition().x - 141, m_potatoStill.getPosition().y + 139);
	m_posPotatoButt.setPosition(m_potatoStill.getPosition().x - 60, m_potatoStill.getPosition().y + 139);
	m_sellPotatoButt.setPosition(m_potatoStill.getPosition().x + 56, m_potatoStill.getPosition().y - 30);
	m_buttTips.setPosition(m_potatoStill.getPosition().x - 142, m_potatoStill.getPosition().y + 91);

	//Destillery Animation//
	if (m_distillAni.x < 9 && distillAnim.getElapsedTime().asMilliseconds() > 280.0f)
	{
		distillAnim.restart();
		m_distillAni.x++;
		Animation(m_distilleryOn, m_distillAni, 0, 2690, 0, 242, 249, 242);
	}
	else if(distillAnim.getElapsedTime().asMilliseconds() > 280.0f)
	{
		distillAnim.restart();
		m_distillAni.x = 0;
	}
	//

	//Homefarm Potatofiller//
	//Fills player cargo on collision
	if (m_player->GetSprite()->getGlobalBounds().intersects(m_homeFarm->getGlobalBounds()) && (m_delivery || m_player->GetPotatoes() == 0))
	{
		//enemyReinforcements.setPosition(m_homeFarm->getPosition().x - 600, m_homeFarm->getPosition().y);
		m_player->SetPotatoes(100);
		m_delivery = false;
		m_respawn = true;
<<<<<<< HEAD
=======
		m_fanfareSound.play();
		m_newspaper->setPosition(View1.getCenter());
		m_gamePaused = true;
		//returnClock.restart();
		// Distiller placer//
		//
>>>>>>> Pre-final_polish_branch_-_Erik_-_2
		float distPos = rand() % 3;
		if (distPos == 0) 
		{
			m_distilleryOn->setPosition(214, 1020);
		}
		else if (distPos == 1)
		{
			m_distilleryOn->setPosition(6340, 2495);
		}
		else if (distPos == 2)
		{
			m_distilleryOn->setPosition(7456, 947);
		}
		else if (distPos == 3)
		{
			m_distilleryOn->setPosition(1913, 2060);
		}
	}


	cash = std::to_string(m_cash);

	scoreText.setFont(font1);
	scoreText.setCharacterSize(50);
	scoreText.setColor(sf::Color::White);

	if (m_cash < 10)
	{
		scoreText.setPosition(View1.getCenter().x + 80, View1.getCenter().y - 535);
		cash.resize(1);
	}
	else if (m_cash < 100)
	{
		scoreText.setPosition(View1.getCenter().x + 50, View1.getCenter().y - 535);
		cash.resize(2);
	}
	else if (m_cash < 1000)
	{
		scoreText.setPosition(View1.getCenter().x + 20, View1.getCenter().y - 535);
		cash.resize(3);
	}
	else if (m_cash < 10000)
	{
		scoreText.setPosition(View1.getCenter().x - 10, View1.getCenter().y - 535);
		cash.resize(4);
	}
	else if (m_cash < 100000)
	{
		scoreText.setPosition(View1.getCenter().x - 40, View1.getCenter().y - 535);
		cash.resize(5);
	}
	else if (m_cash < 1000000)
	{
		scoreText.setPosition(View1.getCenter().x - 70, View1.getCenter().y - 535);
		cash.resize(6);
	}
	scoreText.setString(cash);
	m_GUIscore.setPosition(View1.getCenter().x + 722, View1.getCenter().y - 540);

	m_potatoDialog.setPosition(View1.getCenter().x - m_potatoDialog.getGlobalBounds().width/2, (View1.getCenter().y - m_potatoDialog.getGlobalBounds().height/2) - 100);

	if ((m_distilleryOn->getGlobalBounds().intersects(m_player->GetSprite()->getGlobalBounds())) && m_potatoDown < 0)
	{
		m_potatoDown = 1000;
		m_pauseTime = 100;
		m_potatoMax = m_player->GetPotatoes();
		m_gamePaused = true;
		m_delivery = true;
	}


	if (!m_gamePaused && !m_tutorial && !m_respawn)
	{
		if (m_potatoesDropping)
		{
			if (m_potatoDropFrames.x < 19 && m_potatoDropClock.getElapsedTime().asMilliseconds() > 120.0f)
			{
				m_potatoDropClock.restart();
				m_potatoDropFrames.x++;
				Animation(m_potatoDropSpr, m_potatoDropFrames, 0, 2000, 0, 100, 100, 100);
			}
			else if (m_potatoDropClock.getElapsedTime().asMilliseconds() > 120.0f)
			{
				m_potatoesDropping = false;
				m_potatoDropSpr->setPosition(-100, -100);
				m_potatoDropFrames.x = 0;
			}
		} 

		if (m_cash < 10)
		{
			cash.resize(1);
			text5.setString(cash);
			text5.setPosition(View1.getCenter().x - 30, View1.getCenter().y - 100);
		}
		else if (m_cash < 100)
		{
			cash.resize(2);
			text5.setString(cash);
			text5.setPosition(View1.getCenter().x - 45, View1.getCenter().y - 100);
		}
		else if (m_cash < 1000)
		{
			cash.resize(3);
			text5.setString(cash);
			text5.setPosition(View1.getCenter().x - 60, View1.getCenter().y - 100);
		}
		else if (m_cash < 10000)
		{
			cash.resize(4);
			text5.setString(cash);
			text5.setPosition(View1.getCenter().x - 75, View1.getCenter().y - 100);
		}
		else if (m_cash < 100000)
		{
			cash.resize(5);
			text5.setString(cash);
			text5.setPosition(View1.getCenter().x - 90, View1.getCenter().y - 100);
		}
		else if (m_cash < 1000000)
		{
			cash.resize(6);
			text5.setString(cash);
			text5.setPosition(View1.getCenter().x - 105, View1.getCenter().y - 100);
		}
		
		if (m_player->GetX() > 960 && m_player->GetX() < mapWidth - 960 &&
			m_player->GetY() > 540 && m_player->GetY() < mapHeight - 540)
		{
			View1.setCenter(m_player->GetX(), m_player->GetY());
		}
		else if (m_player->GetX() < 960 && m_player->GetY() < 540)
		{
			View1.setCenter(960, 540);
		}
		else if (m_player->GetX() > mapWidth - 960 && m_player->GetY() > mapHeight - 540)
		{
			View1.setCenter(mapWidth - 960, mapHeight - 540);
		}
		else if (m_player->GetX() > mapWidth - 960 && m_player->GetY() < 540)
		{
			View1.setCenter(mapWidth - 960, 540);
		}
		else if (m_player->GetX() < 960 && m_player->GetY() > mapHeight - 540)
		{
			View1.setCenter(960, mapHeight - 540);
		}
		else if (m_player->GetX() < 960)
		{
			View1.setCenter(960, m_player->GetY());
		}
		else if (m_player->GetY() < 540)
		{
			View1.setCenter(m_player->GetX(), 540);
		}
		else if (m_player->GetX() > mapWidth - 960)
		{
			View1.setCenter(mapWidth - 960, m_player->GetY());
		}
		else if (m_player->GetY() > mapHeight - 540)
		{
			View1.setCenter(m_player->GetX(), mapHeight - 540);
		}

		//Debug  cheats
		
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::P))
		{
			m_gamePaused = true;
			m_potatoMax = m_player->GetPotatoes();
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::K))
		{
			m_player->SetHP(-100);
		} 
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::F))
		{
			m_cash += 100;
		}
		
		//


		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
		{
			m_next = StateMachine::build<MenuState>(m_machine, m_window, m_textureManager, false, false);
		}

		m_potatoDown--;

		int cloudcheck = 0;
		for (int i = 0; i < m_clouds.size(); i++)
		{
			if (m_clouds.at(i)->GetX() > mapWidth)
			{
				int randposY = clamp(rand() % mapHeight, 0, mapHeight);
				m_clouds.at(i)->SetPosition(-500, randposY);
			}

			if (m_clouds.at(i)->Update())
			{
				cloudcheck += 1;
				m_player->setHidden(true);
			}
			else if(cloudcheck <= 0)
			{
				m_player->setHidden(false);
			}
		}
		if (m_player->IsHidden() == true)
		{
			for (int i = 0; i < 4; i++)
			{
				//m_cloudCorners[i].setOrigin(View1.getCenter());
				m_cloudCorners[i].setRotation(i * 90);
				if (m_cloudCorners[i].getRotation() == 0)
				{
					m_cloudCorners[i].setPosition(View1.getCenter().x + View1.getSize().x / 4, View1.getCenter().y);
				}
				else if (90 * i == 90)
				{
					m_cloudCorners[i].setPosition(View1.getCenter().x - View1.getSize().x / 4, View1.getCenter().y);
				}
				else if(m_cloudCorners[i].getRotation() == 180)
				{
					m_cloudCorners[i].setPosition(View1.getCenter().x - View1.getSize().x / 4, View1.getCenter().y -20);
				}
				else if (m_cloudCorners[i].getRotation() == 270)
				{
					m_cloudCorners[i].setPosition(View1.getCenter().x + View1.getSize().x / 4, View1.getCenter().y - 20);
				}
			}
		}

		m_player->Update();
		for (int i = 0; i < m_enemies.size(); i++)
		{
			if (m_enemies.at(i)->GetHP() > 0)
			{
				if (m_player->GetHP() > 0)
				{
					m_player->PowerupCollision(m_enemies.at(i));
					m_player->ProjectileCollision(m_enemies.at(i));
				}
				if (m_player->IsSpotted() && m_enemies.at(i)->GetType() == ENEMY_PLANE) //&& m_enemies.at(i)->GetType() == ENEMY_PLANE
				{
					m_enemies.at(i)->SetAlerted(true);
				}
				else if (m_player->IsSpotted() == false && m_player->GetTimer() < sf::microseconds(100)) //&& m_enemies.at(i)->GetType() == ENEMY_PLANE
				{
					m_enemies.at(i)->SetAlerted(false);
				}
				if (m_enemies.at(i)->ProjectileCollision())
				{
					m_GUIhealth.rotate(-1.8);
				}
			}
			m_enemies.at(i)->Update();
		}

		while (m_window.pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::Closed:
				m_machine.quit();
				break;

			default:
				break;
			}
		}
		
		if (m_delivery || m_player->GetPotatoes() == 0)
		{
			float ans, ans2, ans3;
			ans = atan2(m_homeFarm->getPosition().y - m_player->GetY(),
				m_homeFarm->getPosition().x - m_player->GetX());
			float pai = 180 / PI;
			ans *= pai;

			if (ans < 0)
			{
				ans = 360 - (-ans);
			}
			m_compass.setRotation(90 + ans);
		}
		else
		{
			float ans, ans2, ans3;
			ans = atan2(m_distilleryOn->getPosition().y - m_player->GetY(),
				m_distilleryOn->getPosition().x - m_player->GetX());
			float pai = 180 / PI;
			ans *= pai;

			if (ans < 0)
			{
				ans = 360 - (-ans);
			}
			m_compass.setRotation(90 + ans);
		}
		if (m_player->GetPotatoes() != m_Taters)//when amount of potatoes changes make potatoUse true
		{
			potatoUse = true;
		}
		
		
		text.setPosition(View1.getCenter().x + 200, View1.getCenter().y - 150);
		m_GUIback.setPosition((View1.getCenter().x - 960), (View1.getCenter().y + 540));
		m_compass.setPosition(m_player->GetX(), m_player->GetY());
		m_GUIhealth.setPosition((m_GUIback.getPosition().x + 919),
			m_GUIback.getPosition().y - 10);
			m_healthLight->setPosition(m_GUIhealth.getPosition().x, m_GUIhealth.getPosition().y);
			m_healthLight->setRotation(m_GUIhealth.getRotation());
		m_GUIpotatoes.setPosition((m_GUIback.getPosition().x + 1160) + (m_player->GetPotatoes() * 7.0f),
			m_GUIback.getPosition().y - 31);
		m_potatoLight->setPosition(m_GUIpotatoes.getPosition().x, m_GUIpotatoes.getPosition().y);
		m_potatoLight->setRotation(m_GUIpotatoes.getRotation());
//		m_GUIglass.setPosition((View1.getCenter().x - 960), (View1.getCenter().y + 540));


		m_GUIbakedPotato.setPosition((m_GUIback.getPosition().x + 200),
			m_GUIback.getPosition().y - 109);
		m_GUIbooze.setPosition((m_GUIback.getPosition().x + 311),
			m_GUIback.getPosition().y - 105);
		m_GUIpotatoCloud.setPosition((m_GUIback.getPosition().x + 406),
			m_GUIback.getPosition().y - 108);
	}
	//Game Paused
	else if (m_gamePaused && !m_respawn)
	{
		m_pauseTime--;
		m_potatoDrop = true;
		m_potatoesDropping = true;
		m_potatoDropSpr->setPosition(m_player->GetX(), m_player->GetY());

		m_player->PauseSounds();
		for (int i = 0; i < m_enemies.size(); i++)
		{
			m_enemies.at(i)->PauseSounds();
		}


		//Potato suck animation//
		if (m_potatoAni.x == 0 && potatoAnim.getElapsedTime().asMilliseconds() > 120.0f)
		{
			if (m_potatoAni.y == 2)
			{
				m_potatoSuckSound.play();
			}
			
		}
		if (m_potatoAni.x < 14 && potatoAnim.getElapsedTime().asMilliseconds() > 120.0f &&	m_potatoAnimate)
		{
			if (m_potatoAni.x == 7)
			{
				if (m_potatoAni.y == 1)
				{
					m_potatoDropSound.play();
				}
				else if (m_potatoAni.y == 2)
				{
					m_potatoSuckSound.play();
				}
			}
			potatoAnim.restart();
			Animation(m_potato, m_potatoAni, 0, 5600, 0, 1200, 400, 400);
			m_potatoAni.x++;
		}
		else if (potatoAnim.getElapsedTime().asMilliseconds() > 120.0f &&	m_potatoAnimate)
		{
			if (m_potatoAni.y == 1)
			{
				m_potatoDropSound.play();
			}
			potatoAnim.restart();
			m_potatoAni.x = 0;
			m_potatoAni.y = 0;
			m_potatoAnimate = false;
		}
		//

		potat = std::to_string(m_player->GetPotatoes());
		cargoPotato.setFont(font1);
		cargoPotato.setColor(sf::Color::Black);
		cargoPotato.setCharacterSize(40);
		if (m_player->GetPotatoes() == 100)
			cargoPotato.setPosition(m_potatoDialog.getPosition().x + 185,
				m_potatoDialog.getPosition().y + 55);
		else if (m_player->GetPotatoes() < 10)
			cargoPotato.setPosition(m_potatoDialog.getPosition().x + 225,
				m_potatoDialog.getPosition().y + 55);
		else
			cargoPotato.setPosition(m_potatoDialog.getPosition().x + 205,
				m_potatoDialog.getPosition().y + 55);

		Cargo.setPosition(m_potatoDialog.getPosition().x + 80, m_potatoDialog.getPosition().y + 55);
		Potatoes.setPosition(m_potatoDialog.getPosition().x + 90, m_potatoDialog.getPosition().y + 275);

		sellpotat = std::to_string(m_soldPotatoes);
		sellPotato.setFont(font1);
		sellPotato.setColor(sf::Color::Black);
		sellPotato.setCharacterSize(40);
		potatSell.setFont(hndWrtng);
		potatSell.setColor(sf::Color::White);
		potatSell.setCharacterSize(30);
		if (m_soldPotatoes == 100)
		{
			sellPotato.setPosition(m_potatoDialog.getPosition().x + 130,
				m_potatoDialog.getPosition().y + 310);
			potatSell.setPosition(m_potatoDialog.getPosition().x + 355,
				m_potatoDialog.getPosition().y + 75);
		}
		else if (m_soldPotatoes < 10)
		{
			sellPotato.setPosition(m_potatoDialog.getPosition().x + 160,
				m_potatoDialog.getPosition().y + 310);
			potatSell.setPosition(m_potatoDialog.getPosition().x + 395,
				m_potatoDialog.getPosition().y + 75);
		}
		else if (m_soldPotatoes < 100)
		{
			sellPotato.setPosition(m_potatoDialog.getPosition().x + 140,
				m_potatoDialog.getPosition().y + 310);
			potatSell.setPosition(m_potatoDialog.getPosition().x + 375,
				m_potatoDialog.getPosition().y + 75);
		}

		payout = m_soldPotatoes * 3;
		payoutString = std::to_string(payout);
		payoutText.setFont(hndWrtng);
		payoutText.setColor(sf::Color::White);
		payoutText.setCharacterSize(30);



		if (m_player->GetPotatoes() == 100)
			potat.resize(3);
		else if (m_player->GetPotatoes() > 9)
			potat.resize(2);
		else
			potat.resize(1);

		if (m_soldPotatoes == 100)
			sellpotat.resize(3);
		else if (m_soldPotatoes > 9)
			sellpotat.resize(2);
		else
			sellpotat.resize(1);

		if (payout < 10)
		{
			payoutString.resize(1);
			payoutText.setString(payoutString);
			payoutText.setPosition(m_potatoDialog.getPosition().x + 395, m_potatoDialog.getPosition().y + 165);
		}
		else if (payout < 100)
		{
			payoutString.resize(2);
			payoutText.setString(payoutString);
			payoutText.setPosition(m_potatoDialog.getPosition().x + 375, m_potatoDialog.getPosition().y + 165);
		}
		else if (payout < 1000)
		{
			payoutString.resize(3);
			payoutText.setString(payoutString);
			payoutText.setPosition(m_potatoDialog.getPosition().x + 355, m_potatoDialog.getPosition().y + 165);
		}

		if (m_cash < 10)
		{
			cash.resize(1);
		}
		else if (m_cash < 100)
		{
			cash.resize(2);
		}
		else if (m_cash < 1000)
		{
			cash.resize(3);
		}
		else if (m_cash < 10000)
		{
			cash.resize(4);
		}
		else if (m_cash < 100000)
		{
			cash.resize(5);
		}
		else if (m_cash < 1000000)
		{
			cash.resize(6);
		}

		cargoPotato.setString(potat);
		sellPotato.setString(sellpotat);
		potatSell.setString(sellpotat);
		scoreText.setString(cash);
		
		float dollars = 0;
		int dollarcheck = 0;
		while (m_window.pollEvent(event))
		{
			switch (event.type)
			{

				case sf::Event::Closed:
					m_machine.quit();
					break;
				
				case sf::Event::KeyPressed:
				{
					switch (event.key.code)
					{
						case sf::Keyboard::A:
							if (m_player->GetPotatoes() < m_potatoMax)
							{
								m_player->SetPotatoes(m_player->GetPotatoes() + 1);
								m_soldPotatoes -= 1;
								m_cash -= 3;
								m_potatoAni.y = 2;
								m_potatoAnimate = true;
							}
							break;

						case sf::Keyboard::D:
							if (m_player->GetPotatoes() >= 1)
							{
								m_player->SetPotatoes(m_player->GetPotatoes() - 1);
								m_soldPotatoes += 1;
								m_cash += 3;
								m_potatoAni.y = 1;
								m_potatoAnimate = true;
								//
							}
							break;
						case sf::Keyboard::Space:
<<<<<<< HEAD
								m_gamePaused = false;
								m_potatoDrop = false;
								m_potatoDropClock.restart();
								m_soldPotatoes = 0;
=======
							m_player->setShootDelay(1);
							dollars = clamp((m_soldPotatoes + 1) / 10, 0, 10);
							dollarcheck = 0;
							for (int i = 0; i < dollars; i++)
							{
								
								if (m_dollarParticles[i].m_sprite == nullptr)
								{
									int Ispeed = clamp(rand() % 9, 6, 9);
									sf::Vector2f cashpos;
									cashpos.x = rand() % 40;
									cashpos.y = rand() % 40;
									float speed = static_cast<float>(Ispeed) / 10;
									
									m_dollarParticles[i].init(m_dollarTex, sf::IntRect(1027, 0, 56, 100), 19, 0, speed); // 1027 minsta
									m_dollarParticles[i].m_sprite->setPosition(m_distilleryOn->getPosition().x - cashpos.x, m_distilleryOn->getPosition().y - cashpos.y);//m_player->GetX(), m_player->GetY());
								}
								dollarcheck++;
							}
							if (dollarcheck > 0)
							{
								m_kachingSound.play();
								m_flyingDollars = true;
								//Ka-ching sound
							}
							m_gamePaused = false;
							m_potatoDrop = false;
							m_soldPotatoes = 0;
>>>>>>> Pre-final_polish_branch_-_Erik_-_2
							break;
						default:
							break;

					}

					default:
						break; 
				}
			}
			
		}
	}
	else if (m_gamePaused && m_respawn)
	{
		m_player->PauseSounds();
		for (int i = 0; i < m_enemies.size(); i++)
		{
			m_enemies.at(i)->PauseSounds();
		}
		if (m_newspaperAnimation.x == 0 && m_fanfareSound.getStatus() != m_fanfareSound.Playing)
		{
		}
		if (m_respawnTimer.getElapsedTime().asSeconds() > 0.08 && m_newspaperAnimation.x < 6)
		{
			Animation(m_newspaper, m_newspaperAnimation, 0, 3205, 0, 458, 458, 458);
			clamp(m_newspaperAnimation.x++, 0, 6);
			m_respawnTimer.restart();
		}
		else if (m_respawnTimer.getElapsedTime().asSeconds() > 0.08 && m_newspaperAnimation.x == 6)
		{
			int random = (rand() % 4) + 1;
			m_newspaper->setTexture(*m_newspaperTex);
			m_newspaper->setTextureRect(sf::IntRect(459 * random, 0, 458, 458));
			m_newspaperAnimation.x = 10;
		}
		else if (m_respawnTimer.getElapsedTime().asSeconds() > 3)
		{
			m_newspaperAnimation.x = 0;
			respawnEnemies();
			m_respawn = false;
			m_gamePaused = false;
			m_potatoGetCheck = true;
			m_newspaper->setTexture(*m_newspaperSpinTex);
			m_newspaper->setTextureRect(sf::IntRect(0, 0, 3205, 458));
		}
	}
	//End of pause

	if (m_player->IsDead())
	{
		if (deathCounter > 600)
		{
			outCash.open("../assets/cash.txt");

			cash = std::to_string(m_cash);


			if (m_cash < 10)
			{
				cash.resize(1);
			}
			else if (m_cash < 100)
			{
				cash.resize(2);
			}
			else if (m_cash < 1000)
			{
				cash.resize(3);
			}
			else if (m_cash < 10000)
			{
				cash.resize(4);
			}
			else if (m_cash < 100000)
			{
				cash.resize(5);
			}
			else if (m_cash < 1000000)
			{
				cash.resize(6);
			}
			else if (m_cash < 1000000)
			{
				cash.resize(7);
			}

			outCash << cash << std::endl;

			outCash.close();


			m_next = StateMachine::build<GameOverState>(m_machine, m_window, m_textureManager, true, false);
		}
		else
			deathCounter++;
	}

	if (tutorialDelay.getElapsedTime().asMilliseconds() > 3000 && !m_tutorial && m_tutorialBox > 0)
	{
		m_player->setShootDelay(1);
		m_tutorial = true;
		m_tutorialBox--;
		m_tutor1.setPosition(View1.getCenter().x, View1.getCenter().y);
		m_tutor2.setPosition(View1.getCenter().x + 500, View1.getCenter().y + 200);
		m_tutor3.setPosition(View1.getCenter().x + 500, View1.getCenter().y - 300);
	}


	if (m_tutorial && m_tutorialBox > 0)
	{
		if (m_arrowAni.x < 2 && arrowAnim.getElapsedTime().asMilliseconds() > 120.0f)
		{
			arrowAnim.restart();
			m_arrowAni.x++;
			Animation(m_arrow, m_arrowAni, 0, 425, 0, 200, 212, 200);
		}
		else if (arrowAnim.getElapsedTime().asMilliseconds() > 120.0f)
		{
			arrowAnim.restart();
			m_arrowAni.x = 0;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space) && tutorialDelay.getElapsedTime().asMilliseconds() > 500)
		{
			tutorialDelay.restart();
			m_tutorialBox--;
		}
		if (m_tutorialBox <= 0)
		{
			m_tutorial = false;
		}
	}
}

void PlayState::draw()
{
	// Clear the previous drawing
	m_window.clear();
	m_window.setView(View1);
	m_window.draw(m_bg);
	m_window.draw(m_distillery);
	m_window.draw(m_distillery1);
	m_window.draw(m_distillery2);
	m_window.draw(m_distillery3);
	m_window.draw(*m_homeFarm);

	m_window.draw(*m_potatoDropSpr);

	if (!m_delivery)
	{
		m_window.draw(*m_distilleryOn);
	}
	if (m_player->GetHP() <= 0)
	{
		m_player->Draw(m_window);
	}
	for (int i = 0; i < m_enemies.size(); i++)
	{
		if (m_enemies.at(i)->GetType() == ENEMY_TURRET)
		{
			m_enemies.at(i)->Draw(m_window);
		}
		/*else
		{
			m_enemies.at(i)->Draw(m_window);
		}*/
	}
	if (m_player->GetHP() > 0)
	{
		m_player->Draw(m_window);
	}
	if (m_potatoGetCheck)
	{
		m_window.draw(*m_potatoGet);
	}
	for (int i = 0; i < m_enemies.size(); i++)
	{
		if (m_enemies.at(i)->GetType() != ENEMY_TURRET)
		{
			m_enemies.at(i)->Draw(m_window);
		}
	}

	for (int i = 0; i < m_clouds.size(); i++)
	{
		m_clouds.at(i)->Draw(m_window);
	}
	//if (deliverClock.getElapsedTime().asSeconds() > 10 && m_dialogs)
	//{
	//	m_window.draw(m_deliveryDialog);
	//}

	//if (returnClock.getElapsedTime().asSeconds() > 10 && m_dialogs)
	//{
	//	m_window.draw(m_returnDialog);
	//}
	if (m_player->GetHP() > 0)
	{
		m_window.draw(m_compass);

	}
	m_window.draw(enemyReinforcements);
	m_window.draw(m_GUIback);
	if (GUIdamage)
	{
		m_window.draw(*m_healthLight);
	}
	m_window.draw(m_GUIhealth);
	if (potatoUse)
	{
		m_window.draw(*m_potatoLight);
	}
	m_window.draw(m_GUIpotatoes);
	if (m_player->PowerupCheck(SPEED_BOOZE) == false)
	{
		m_window.draw(m_GUIbooze);
	}
	if (m_player->PowerupCheck(BAKED_POTATO) == false)
	{
		m_window.draw(m_GUIbakedPotato);
	}
	if (m_player->PowerupCheck(POTATO_CLOUD) == false)
	{
		m_window.draw(m_GUIpotatoCloud);
	}

	if (m_player->IsHidden())
	{
		for (int i = 0; i < 4; i++)
		{
			m_window.draw(m_cloudCorners[i]);
		}
	}

	if (m_potatoDrop)
	{
		m_window.draw(m_potatoDialog);


		if(m_potatoAnimate)
			m_window.draw(*m_potato);
		else
			m_window.draw(m_potatoStill);
		
		m_window.draw(cargoPotato);
		m_window.draw(sellPotato);
		m_window.draw(m_negPotatoButt);
		m_window.draw(m_posPotatoButt);
		m_window.draw(m_sellPotatoButt);
		m_window.draw(payoutText);
		m_window.draw(potatSell);
		m_window.draw(Cargo);
		m_window.draw(Potatoes);
		m_window.draw(m_buttTips);
	}

	if (m_tutorialBox == 3 && m_tutorial)
	{
		m_window.draw(m_tutor1);
	}
	if (m_tutorialBox == 2 && m_tutorial)
	{
		m_window.draw(m_tutor2);
		m_window.draw(*m_arrow);
		m_arrow->setPosition(View1.getCenter().x + 945, View1.getCenter().y + 290);
		m_arrow->setScale(-1, 1);
	}
	if (m_tutorialBox == 1 && m_tutorial)
	{
		m_window.draw(m_tutor3);
		m_window.draw(*m_arrow);
		m_arrow->setPosition(View1.getCenter().x + 100, View1.getCenter().y - 200);
		m_arrow->setScale(1, 1);
	}

	m_window.draw(*m_scoreHUD);

	m_window.draw(m_GUIscore);
	m_window.draw(scoreText);

	if (m_flyingDollars == true)
	{
		for (int i = 0; i < 10; i++)
		{
			if (m_dollarParticles[i].active == true)
			{
				m_window.draw(*m_dollarParticles[i].m_sprite);
			}
		}
	}

	if (m_respawn)
	{
		m_window.draw(*m_newspaper);
	}

	m_window.display();
}

void PlayState::respawnEnemies()
{
	//enemyReinforcements.setOrigin(m_player->GetCenterX(), m_player->GetCenterY());

	//std::cout << text.getGlobalBounds().width << " " << text.getGlobalBounds().height << std::endl;

	int currentsize = m_enemies.size();
	int planes = 0, turrets = 0, zeppelins = 0, reinforcements = 0, random = 0;
	auto it = m_enemies.begin();
	while (it != m_enemies.end())
	{
		if ((*it)->GetHP() <= 0)
		{
			delete (*it);
			it = m_enemies.erase(it);
		}
		else
		{
			if ((*it)->GetType() == ENEMY_ZEPPELIN)
			{
				planes++;
			}
			if ((*it)->GetType() == ENEMY_ZEPPELIN)
			{
				turrets++;
			}
			if ((*it)->GetType() == ENEMY_ZEPPELIN)
			{
				zeppelins++;
			}
			it++;
		}
	}
	
	int casualties = currentsize - m_enemies.size();
	if (casualties != 0)
	{
		reinforcements = clamp(casualties / 6, 1, 10) + 1 + zeppelins;
	}
	else
	{
		reinforcements = 1 + zeppelins;
	}
	//if (casualties >= currentsize)
	//{
	//	//Potato pilot ace badge!
	//}
	if (currentsize <= 50 && casualties + reinforcements < 50)
	{
		for (int i = 0; i < casualties + reinforcements; i++)
		{
			random = rand() % 100;
			if (random < 10 - zeppelins * 2)
			{
				sf::Vector2i pos = randPos();

				EnemyZeppelin* enemy = new EnemyZeppelin(m_player, m_textureManager, pos.x, pos.y);
				m_enemies.push_back(enemy);
				//Spawna zeppelinare
			}
			else if (random < 30 - turrets * 3)
			{
				sf::Vector2i pos = randPos();

				EnemyTurret* enemy = new EnemyTurret(m_player, m_textureManager, pos.x, pos.y);
				m_enemies.push_back(enemy);
				//spawna turret
			}
			else
			{
				sf::Vector2i pos = randPos();

				EnemyPlane* enemy = new EnemyPlane(m_player, m_textureManager, pos.x, pos.y);
				m_enemies.push_back(enemy);
				//spawn plane
			}
		}
	}
	else
	{
		for (int i = 0; i < casualties; i++)
		{
			random = rand() % 100;
			if (random < 10 - zeppelins * 2)
			{
				sf::Vector2i pos = randPos();

				EnemyZeppelin* enemy = new EnemyZeppelin(m_player, m_textureManager, pos.x, pos.y);
				m_enemies.push_back(enemy);
				//Spawna zeppelinare
			}
			else if (random < 30 - turrets * 3)
			{
				sf::Vector2i pos = randPos();

				EnemyTurret* enemy = new EnemyTurret(m_player, m_textureManager, pos.x, pos.y);
				m_enemies.push_back(enemy);
				//spawna turret
			}
			else
			{
				sf::Vector2i pos = randPos();

				EnemyPlane* enemy = new EnemyPlane(m_player, m_textureManager, pos.x, pos.y);
				m_enemies.push_back(enemy);
				//spawn plane
			}
		}
	}
}

sf::Vector2i PlayState::randPos()
{
	sf::Vector2i pos;
	pos.x = clamp(rand() % mapWidth, 50, mapWidth - 50);
	pos.y = clamp(rand() % mapHeight, 50, mapHeight - 200);
	while (screenrect.contains(pos))
	{
		pos.x = clamp(rand() % mapWidth, 50, mapWidth - 50);
		pos.y = clamp(rand() % mapHeight, 50, mapHeight - 200);

	}
	if(screenrect.contains(pos) == false)
	{
		return pos;
	}
}
