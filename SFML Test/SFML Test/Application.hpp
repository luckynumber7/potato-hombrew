#ifndef APPLICATION_HPP
#define APPLICATION_HPP

#include "IntroState.hpp"
#include "PlayState.hpp"
#include "StateMachine.hpp"
#include "GameOverState.hpp"
#include "SpriteManager.hpp"
#include "TextureManager.hpp"

#include <SFML/Graphics/RenderWindow.hpp>

#include <SFML/Audio.hpp>

class Application
{
public:
	Application();
	~Application();
	void run();

private:
	StateMachine m_machine;
	sf::RenderWindow m_window;

	//Managers here
	TextureManager* m_textureManager;

};

#endif // APPLICATION_HPP