#pragma once

#include "State.hpp"
#include "stdafx.hpp"
#include "TextureManager.hpp"

#include <SFML/System.hpp>
#include <SFML/Graphics/Color.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>

#include <SFML\Audio.hpp>
#include <SFML/Audio/Music.hpp>

#include <iostream>
#include <fstream>
#include <string>

class StateMachine;

namespace sf
{
	class RenderWindow;
}

class GameOverState : public State
{
public:

	GameOverState(StateMachine& game, sf::RenderWindow& window, TextureManager* p_textureManager, float p_cash, bool replace = true, bool restart = true);

	void pause();
	void resume();
	void update();
	void draw();

	sf::View View1;

private:
	std::ifstream inname;
	std::ifstream inscore;
	std::ifstream incash;

	std::ofstream outnames;
	std::ofstream outscore;

	sf::Font font;
	sf::Font font2;
	sf::Text text;
	sf::Text text1;
	sf::Text text2;
	sf::Text namesText;
	sf::Text scoresText;

	sf::Text enterText;
	sf::Text playerText;

	std::string Names[5];
	std::string Scores[5];

	std::string actNString;
	std::string actSString;

	std::string m_write;
	std::string m_saved;
	std::string written;

	sf::Texture m_bgTex;
	sf::Sprite m_bg;

	sf::Texture m_dialogTex;
	sf::Sprite m_dialog;

	sf::Music music;

	std::string cash;

	int m_fScore;

	int scoresInt;
	int scoresInt1;
	int scoresInt2;
	int scoresInt3;
	int scoresInt4;

	bool writeable = true;
};
