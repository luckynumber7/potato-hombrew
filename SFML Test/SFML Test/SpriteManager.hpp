#pragma once
#include "stdafx.hpp"

class SpriteManager
{
public:
	SpriteManager();
	~SpriteManager();
	/**
	* Creates a sprite from file, at x/y cooridnates of image, at the determined w/h. Pushes both into seperate arrays
	*/
	sf::Sprite* CreateSprite(const std::string& p_filename, int p_x, int p_y, int p_w, int p_h);
	void DestroySprite(sf::Sprite* p_sprite);

private:
	std::vector<sf::Sprite*> m_sprites;
	std::map<std::string, sf::Texture> m_textureMap;

};
