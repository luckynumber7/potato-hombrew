#ifndef STATE_HPP
#define STATE_HPP

#include <memory>
#include "TextureManager.hpp"

class StateMachine;
struct System;

namespace sf
{
	class RenderWindow;
}

class State
{
public:
	State(StateMachine& machine, sf::RenderWindow& window, TextureManager* p_textureManager, bool replace = true, bool restart = false);
	virtual ~State() = default;

	State(const State&) = delete;
	State& operator= (const State&) = delete;

	virtual void pause() = 0;
	virtual void resume() = 0;

	virtual void update() = 0;
	virtual void draw() = 0;

	std::unique_ptr<State> next();

	bool isReplacing();
	bool isRestarting();

protected:
	StateMachine& m_machine;
	sf::RenderWindow& m_window;
	TextureManager* m_textureManager;

	bool m_replacing;
	bool m_restarting;

	std::unique_ptr<State> m_next;
};

#endif // STATE_HPP