#include <memory>
#include <iostream>

#include "IntroState.hpp"
#include "PlayState.hpp"
#include "CreditsState.hpp"
#include "StateMachine.hpp"

#include "stdafx.hpp"
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML/Audio.hpp>



CreditsState::CreditsState(StateMachine& machine, sf::RenderWindow& window, TextureManager* p_texureManager, bool replace, bool restart)
	: State{ machine, window, p_texureManager, replace, restart }
{
	if (!music.openFromFile("../assets/music/NotDrunk.wav"))
	{

		std::cout << "Could not load music file" << std::endl;
	}

	music.setVolume(30);        //Reduce the volume
	music.setLoop(true);		//Set loop

	music.play();

	if (!font.loadFromFile("../assets/Fonts/Caladea-Bold.ttf"))
	{
		std::cout << "Error: Could not load font" << std::endl;
	}
	if (!font2.loadFromFile("../assets/Fonts/Caladea-Regular.ttf"))
	{
		std::cout << "Error: Could not load font" << std::endl;
	}

	text.setFont(font);
	text.setString("Lead Design:");
	text.setColor(sf::Color::White);
	text.setCharacterSize(40);
	text.setPosition(960, 1080);

	text1.setFont(font2);
	text1.setString("Gustav Larsson");
	text1.setColor(sf::Color::White);
	text1.setCharacterSize(50);
	text1.setPosition(960, 1120);

	text2.setFont(font);
	text2.setString("Lead Programming:");
	text2.setColor(sf::Color::White);
	text2.setCharacterSize(40);
	text2.setPosition(960, 1280);

	text3.setFont(font2);
	text3.setString("Bengt Hagnelius");
	text3.setColor(sf::Color::White);
	text3.setCharacterSize(50);
	text3.setPosition(960, 1320);

	text4.setFont(font);
	text4.setString("Additional Programmers:");
	text4.setColor(sf::Color::White);
	text4.setCharacterSize(40);
	text4.setPosition(960, 1480);

	text5.setFont(font2);
	text5.setString("Erik Nord");
	text5.setColor(sf::Color::White);
	text5.setCharacterSize(50);
	text5.setPosition(960, 1520);

	text6.setFont(font);
	text6.setString("Lead Art:");
	text6.setColor(sf::Color::White);
	text6.setCharacterSize(40);
	text6.setPosition(960, 1680);

	text7.setFont(font2);
	text7.setString("Erik Levin");
	text7.setColor(sf::Color::White);
	text7.setCharacterSize(50);
	text7.setPosition(960, 1720);

	text8.setFont(font);
	text8.setString("Additional Art:");
	text8.setColor(sf::Color::White);
	text8.setCharacterSize(40);
	text8.setPosition(960, 1880);

	text9.setFont(font2);
	text9.setString("Gustav Larsson");
	text9.setColor(sf::Color::White);
	text9.setCharacterSize(50);
	text9.setPosition(960, 1920);

	text10.setFont(font2);
	text10.setString("Martin M�nsson");
	text10.setColor(sf::Color::White);
	text10.setCharacterSize(50);
	text10.setPosition(960, 1970);

	text11.setFont(font2);
	text11.setString("Sakarias St�hl");
	text11.setColor(sf::Color::White);
	text11.setCharacterSize(50);
	text11.setPosition(960, 2020);

	text12.setFont(font);
	text12.setString("Sound and sound mixing:");
	text12.setColor(sf::Color::White);
	text12.setCharacterSize(40);
	text12.setPosition(960, 2180);

	text13.setFont(font2);
	text13.setString("Martin M�nsson");
	text13.setColor(sf::Color::White);
	text13.setCharacterSize(50);
	text13.setPosition(960, 2220);

	text14.setFont(font);
	text14.setString("Additional sounds effects:");
	text14.setColor(sf::Color::White);
	text14.setCharacterSize(40);
	text14.setPosition(960, 2380);

	text15.setFont(font2);
	text15.setString("Freesound.org");
	text15.setColor(sf::Color::White);
	text15.setCharacterSize(50);
	text15.setPosition(960, 2420);

	text16.setFont(font);
	text16.setString("Music:");
	text16.setColor(sf::Color::White);
	text16.setCharacterSize(40);
	text16.setPosition(960, 2560);

	text17.setFont(font2);
	text17.setString("FreeMusicArchive.com");
	text17.setColor(sf::Color::White);
	text17.setCharacterSize(50);
	text17.setPosition(960, 2610);

	text18.setFont(font2);
	text18.setString("Jason Shaw");
	text18.setColor(sf::Color::White);
	text18.setCharacterSize(50);
	text18.setPosition(960, 2660);

	text19.setFont(font2);
	text19.setString("The Joy Drops");
	text19.setColor(sf::Color::White);
	text19.setCharacterSize(50);
	text19.setPosition(960, 2710);

	text20.setFont(font);
	text20.setString("Press Escape to return to menu");
	text20.setColor(sf::Color::White);
	text20.setCharacterSize(20);
	text20.setPosition(80, 980);

}

void CreditsState::pause()
{

}

void CreditsState::resume()
{

}

void CreditsState::update()
{
	text.move(0, -0.4);
	text1.move(0, -0.4);
	text2.move(0, -0.4);
	text3.move(0, -0.4);
	text4.move(0, -0.4);
	text5.move(0, -0.4);
	text6.move(0, -0.4);
	text7.move(0, -0.4);
	text8.move(0, -0.4);
	text9.move(0, -0.4);
	text10.move(0, -0.4);
	text11.move(0, -0.4);
	text12.move(0, -0.4);
	text13.move(0, -0.4);
	text14.move(0, -0.4);
	text15.move(0, -0.4);
	text16.move(0, -0.4);
	text17.move(0, -0.4);
	text18.move(0, -0.4);
	text19.move(0, -0.4);

	std::cout << text.getPosition().y << std::endl;

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape) || text.getPosition().y < -2200)
	{
		music.stop();

		m_next = StateMachine::build<IntroState>(m_machine, m_window, m_textureManager, true, false);
	}
	else if (text.getPosition().y < -2000)
	{
		musicFade -= 0.3f;
		music.setVolume(musicFade * musicF);
	}
}

void CreditsState::draw()
{

	m_window.clear();
	m_window.draw(text);
	m_window.draw(text1);
	m_window.draw(text2);
	m_window.draw(text3);
	m_window.draw(text4);
	m_window.draw(text5);
	m_window.draw(text6);
	m_window.draw(text7);
	m_window.draw(text8);
	m_window.draw(text9);
	m_window.draw(text10);
	m_window.draw(text11);
	m_window.draw(text12);
	m_window.draw(text13);
	m_window.draw(text14);
	m_window.draw(text15);
	m_window.draw(text16);
	m_window.draw(text17);
	m_window.draw(text18);
	m_window.draw(text19);
	m_window.draw(text20);
	m_window.display();
}