#include "stdafx.hpp"
#include "EnemyPlane.hpp"
#include "AIStates.hpp"
#include "Player.hpp"
#include "Projectile.hpp"
#include <iostream>

EnemyPlane::EnemyPlane(Player* p_player, TextureManager* p_textureManager, float p_posX, float p_posY)
{
	m_textureManager = p_textureManager;

	m_player = p_player;
	
	m_pos.x = p_posX;
	m_pos.y = p_posY;
	m_animation = sf::Vector2i(0, 0);
	m_rotorAnimation = sf::Vector2i(0, 0);

	m_sightRadius.setRadius(400);
	m_sightRadius.setOrigin(m_sightRadius.getRadius(), m_sightRadius.getRadius());
	m_sightRadius.setFillColor(sf::Color(100, 250, 50, 20));

	m_shootRange.setSize(sf::Vector2f(50, 600));
	m_shootRange.setOrigin(sf::Vector2f((m_shootRange.getSize().x / 2),
		m_shootRange.getSize().y + 40));
	m_shootRange.setFillColor(sf::Color(250, 50, 50, 20));

	m_HP = 2;
	bool m_alerted = false;
	bool m_hasFired = false;
	m_speed = 0.50;
	m_turningSpeed = 0.10;
	if (m_alerted == true)
	{
		m_state = new Chase(this, m_player);
	}
	else
		m_state = new Patrol(this, m_player);
	m_timer = sf::seconds(6);
	m_shootDelay = sf::seconds(0);
	m_deathTimer = sf::seconds(1);

	m_texture = m_textureManager->LoadTexture("../assets/Fighter Plane Normal.png");
	m_sprite = new sf::Sprite;
	m_sprite->setTexture(*m_texture);
	m_sprite->setTextureRect(sf::IntRect(0, 0, 204, 142));
	
	m_damagedTex = m_textureManager->LoadTexture("../assets/Fighter Plane Damaged Sprite Sheet.png");
	m_deathTex = m_textureManager->LoadTexture("../assets/Fighter Plane Death Scene Sprite Sheet.png");
	m_searchLightTex = m_textureManager->LoadTexture("../assets/Plane Search Light.png");
	m_rotorTex = m_textureManager->LoadTexture("../assets/Propeller animation sprite sheet.png");

	m_searchLight.setTexture(*m_searchLightTex);
	m_searchLight.setTextureRect(sf::IntRect(0, 0, 164, 174));
	m_searchLight.setColor(sf::Color(255, 255, 255, 70));
	
	m_rotorSprite = new sf::Sprite;
	m_rotorSprite->setTexture(*m_rotorTex);
	m_rotorSprite->setTextureRect(sf::IntRect(0, 0, 66, 8));

	m_center.x = 204 / 2;
	m_center.y = 142 / 2;
	m_sprite->setOrigin(m_center);
	m_searchLight.setOrigin(204 / 2.5, 140);
	m_rotorSprite->setOrigin(m_center.x / 2 - 20, m_center.y - 20);
	m_sprite->setScale(0.70, 0.70);

	m_hitbox.setSize(sf::Vector2f(100, 70));
	m_hitbox.setOrigin(sf::Vector2f(m_center.x - (m_sprite->getTextureRect().width - m_hitbox.getSize().x) / 2, m_center.y / 1.5));
	m_hitbox.setFillColor(sf::Color::Blue);

	m_bulletCount = 0;
	for (int i = 0; i < 10; i++)
	{
		m_bullets[i] = nullptr;
	}
	if (!m_fire.loadFromFile("../assets/sounds/enemyMG.wav"))
		std::cout << "ERROR! Could not load sound!" << std::endl;
	if (!m_death.loadFromFile("../assets/sounds/Plane Crashing.wav"))
		std::cout << "ERROR! Could not load sound!" << std::endl;
	m_fireSound.setBuffer(m_fire);
	m_deathSound.setBuffer(m_death);
	m_fireSound.setVolume(30);
	m_deathSound.setVolume(10);
}

EnemyPlane::~EnemyPlane()
{
	for (int i = 0; i < 10; i++)
	{
		if (m_bullets[i] != nullptr)
		{
			delete m_bullets[i];
			m_bullets[i] = nullptr;
		}
	}
	delete m_state;
	m_state = nullptr;

	delete m_rotorSprite;
	delete m_sprite;

}

void EnemyPlane::Update()
{
	if (m_HP > 0)
	{
		if (m_player->IsHidden() || m_player->GetHP() <= 0)
		{
			m_alerted = false;
		}
		m_pos.x += m_speed * sin(m_sprite->getRotation()*3.14159265 / 180.0) * 1.0;
		m_pos.y += -m_speed * cos(m_sprite->getRotation()*3.14159265 / 180.0) * 1.0;

		m_sprite->setPosition(m_pos);
		m_hitbox.setPosition(m_pos);
		m_hitbox.setRotation(m_sprite->getRotation());

		m_rotorSprite->setPosition(m_pos);
		m_rotorSprite->setRotation(m_sprite->getRotation());
		m_searchLight.setPosition(m_pos);
		m_searchLight.setRotation(m_sprite->getRotation());
		m_sightRadius.setPosition(m_pos.x, m_pos.y);
		m_shootRange.setPosition(m_pos.x, m_pos.y);
		m_shootRange.setRotation(m_sprite->getRotation());
		SightCheck(m_player->GetSprite());

		if (m_shootRange.getGlobalBounds().intersects(m_player->GetHitbox().getGlobalBounds()) && m_alerted == true && m_player->GetHP() > 0)
		{
			ShootProjectile();
		}
		if (m_damageFrames <= 0)
		{
			m_sprite->setColor(sf::Color::White);
		}
		m_damageFrames--;

		if (m_state->GetState() == CHASE)
		{
			m_waypoint.x = m_player->GetX();
			m_waypoint.y = m_player->GetY();
		}
		m_state_check = m_state->HandleState();
		if (m_state_check == false)
		{
			if (m_state->GetState() == PATROL)
			{
				delete m_state;
				m_state = new Chase(this, m_player);
				m_searchLight.setColor(sf::Color(255, 0, 0, 70));
			}
			else if (m_state->GetState() == CHASE)
			{
				delete m_state;
				m_state = new Search(this, m_player, m_waypoint);
				m_searchLight.setColor(sf::Color(255, 255, 255, 70));
			}
			else if (m_state->GetState() == SEARCH)
			{
				if (m_alerted == true)
				{
					delete m_state;
					m_state = new Chase(this, m_player);
					m_searchLight.setColor(sf::Color(255, 0, 0, 70));
				}
				else
				{
					delete m_state;
					m_state = new Patrol(this, m_player);
					m_searchLight.setColor(sf::Color(255, 255, 255, 70));
				}

			}
		}


		if (m_shotCount > 9)
		{
			m_shootDelay = sf::seconds(1);
			m_shotCount = 0;
		}
		int count = 0;
		for (int i = 0; i < 10; i++)
		{
			if (m_bullets[i] != nullptr)
			{
				m_bullets[i]->Trajectory();
				if (m_bullets[i]->GetLifetime() < sf::Time::Zero)
				{
					delete m_bullets[i];
					m_bullets[i] = nullptr;
					m_bulletCount--;
				}
			}
			else
			{
				count++;
				if (count >= 10)
				{
					m_bulletCount = 0;
				}
			}
			
		}

		if (m_rotorAnimationDelay < sf::Time::Zero)
		{
			m_rotorAnimation.x++;
			if (m_rotorAnimation.x > 7)
			{
				m_rotorAnimation.x = 0;
			}
			Animation(m_rotorSprite, m_rotorAnimation, 0, 531, 0, 8, 66, 8);
			m_rotorAnimationDelay = sf::milliseconds(2000);
		}
		else
		{
			m_rotorAnimationDelay -= sf::milliseconds(50);
		}

		if (m_HP == 1)
		{
			if (m_animationTimer.getElapsedTime().asMilliseconds() > 60)
			{
				m_animation.x++;
				if (m_animation.x >= 10)
				{
					m_animation.x = 0;
				}
				Animation(m_sprite, m_animation, 0, 1683, 0, 528, 153, 176);
				m_animationTimer.restart();
			}
			
		}
	}

	else if (m_animation.x < 19)
	{
		if (m_animationTimer.getElapsedTime().asMilliseconds() > 60)
		{
			m_sprite->setColor(sf::Color::White);
			m_damageFrames = 75;
			clamp(m_animation.x++, 1, 19);
			Animation(m_sprite, m_animation, 0, 4460, 0, 418, 235, 418);
			m_animationTimer.restart();
		}
		if (m_damageFrames <= 0)
		{
			m_sprite->setColor(sf::Color::Red);
		}
		m_damageFrames--;
		if (m_animation.x < 11)
		{
			m_pos.x += 0.8 * sin(m_sprite->getRotation()*3.14159265 / 180.0) * 1.0;
			m_pos.y += -0.8 * cos(m_sprite->getRotation()*3.14159265 / 180.0) * 1.0;
			//m_sprite->rotate(0.3);
			m_sprite->setPosition(m_pos);
		}
	}
	m_invincibilityFrames -= sf::microseconds(100);
	m_shootDelay -= m_clock.restart();

}

sf::Sprite* EnemyPlane::GetSprite()
{
	return m_sprite;
}

sf::RectangleShape EnemyPlane::GetHitBox()
{
	return m_hitbox;
}

EENEMY EnemyPlane::GetType()
{
	return ENEMY_PLANE;
}


bool EnemyPlane::IsAlerted()
{
	return m_alerted;
}

void EnemyPlane::SetAlerted(bool p_alerted)
{
	m_alerted = p_alerted;
}

sf::CircleShape EnemyPlane::GetSightRadius()
{
	return m_sightRadius;
}

void EnemyPlane::SetSightRadius(float p_radius)
{
	m_sightRadius.setRadius(p_radius);
	m_sightRadius.setOrigin(p_radius, p_radius);
}

sf::RectangleShape EnemyPlane::GetShootRange()
{
	return m_shootRange;
}

void EnemyPlane::SetShootRange(sf::Vector2f p_range)
{
	m_shootRange.setSize(p_range);
	m_shootRange.setOrigin(p_range.x / 2, p_range.y);
}

int EnemyPlane::GetHP()
{
	return m_HP;
}

void EnemyPlane::SetHP(int p_amount)
{
	if (m_invincibilityFrames < sf::Time::Zero)
	{
		m_HP += p_amount;
		if (m_HP == 1)
		{
			m_sprite->setScale(0.92, 0.92);
			m_sprite->setOrigin(153 / 2, 50);
			m_animation.x = 0;
			m_animation.y = 0;
			m_sprite->setTexture(*m_damagedTex);
			m_sprite->setTextureRect(sf::IntRect(0, 0, 153, 176));
		}
		if (m_HP <= 0)
		{
			int randPitch = clamp(rand() % 5, 1, 5);
			float Pitch = 0.5 + static_cast<float>(randPitch) / 2;
			m_deathSound.setPitch(Pitch);
			m_deathSound.play();
			m_sprite->setScale(0.92, 0.92);
			m_sprite->setOrigin(114, 32);
			m_animation.x = 0;
			m_animation.y = 0;
			m_sprite->setTexture(*m_deathTex);
			m_sprite->setTextureRect(sf::IntRect(0, 0, 235, 418));
		}
		if (m_HP + p_amount < m_HP)
		{
			m_sprite->setColor(sf::Color::Red);
			m_damageFrames = 85;
			m_invincibilityFrames = sf::microseconds(20000);
		}
	}
}

sf::Time EnemyPlane::GetTimer()
{
	return m_timer;
}

void EnemyPlane::SetPosition(float p_posX, float p_posY)
{
	m_pos.x = p_posX;
	m_pos.y = p_posY;
}

void EnemyPlane::AddX(float p_x)
{
	m_pos.x += p_x;
}

void EnemyPlane::AddY(float p_y)
{
	m_pos.y += p_y;
}

float EnemyPlane::GetX()
{
	return m_pos.x;
}

float EnemyPlane::GetY()
{
	return m_pos.y;
}

float EnemyPlane::GetCenterX()
{
	return m_center.x;
}

float EnemyPlane::GetCenterY()
{
	return m_center.y;
}

void EnemyPlane::SetX(float p_x)
{
}

void EnemyPlane::SetY(float p_y)
{
}

float EnemyPlane::GetSpeed()
{
	return m_speed;
}

void EnemyPlane::MoveTowards(sf::Vector2f p_pos)
{
	//float mX = sf::Mouse::getPosition(m_window).x; use p_pos.x
	//float mY = sf::Mouse::getPosition(m_window).y; use p_pos.y
	//float posX = m_player.getPosition().x; use p
	//float posY = m_player.getPosition().y;
	float ans = FindAngle(p_pos.x, p_pos.y, m_pos.x, m_pos.y);
	if (p_pos.x > m_pos.x)
	{
		if (p_pos.y > m_pos.y)
		{
				//Undre H�ger
			if (m_sprite->getRotation() > (90 + ans))
			{
				m_sprite->rotate(-m_turningSpeed);
				RotateLeft();
			}
			else if (m_sprite->getRotation() < (90 + ans))
			{
				m_sprite->rotate(m_turningSpeed);
				RotateRight();
			}
		}
		else if (p_pos.y < m_pos.y)
		{
				//�vre H�ger
			if (m_sprite->getRotation() >(90 - ans))
			{
				m_sprite->rotate(-m_turningSpeed);
				RotateLeft();
			}
			else if (m_sprite->getRotation() < (90 - ans))
			{
				m_sprite->rotate(m_turningSpeed);
				RotateRight();
			}
		}
	}
	else if (p_pos.x < m_pos.x)
	{
		if (p_pos.y > m_pos.y)
		{
				//Undre V�nster
			if (m_sprite->getRotation() > (270 - ans))
			{
				m_sprite->rotate(-m_turningSpeed);
				RotateLeft();
			}
			else if (m_sprite->getRotation() < (270 - ans))
			{
				m_sprite->rotate(m_turningSpeed);
				RotateRight();
			}
		}
		else if (p_pos.y < m_pos.y)
		{
				//�vre V�nster
			if (m_sprite->getRotation() >(270 + ans))
			{
				m_sprite->rotate(-m_turningSpeed);
				RotateLeft();
			}
			else if (m_sprite->getRotation() < (270 + ans))
			{
				m_sprite->rotate(m_turningSpeed);
				RotateRight();
			}
		}
	}
	else
	{
		if (m_HP >= 2)
		{
			m_animation.x = 0;
			if (m_animationDelay.getElapsedTime().asSeconds() > 2)
			{
				Animation(m_sprite, m_animation, 0, 613, 0, 142, 204, 142);
				m_animationDelay.restart();
			}
		}
		else if( m_HP == 1)
		{
			if (m_animationDelay.getElapsedTime().asSeconds() > 2)
			{
				m_animation.y = 0;
				m_animationDelay.restart();
			}
		}
	}
	
}

void EnemyPlane::SightCheck(sf::Sprite* p_sprite)
{
	if (m_sightRadius.getGlobalBounds().intersects(p_sprite->getGlobalBounds()))
	{
		float XDif = abs(m_pos.x) - abs(p_sprite->getPosition().x);
		float YDif = abs(m_pos.y) - abs(p_sprite->getPosition().y);
		float hyp = pow(abs(YDif), 2) + pow(abs(XDif), 2);
		if (sqrt(hyp) < m_sightRadius.getRadius() && !m_player->IsHidden() && m_player->GetHP() > 0)
		{
			m_alerted = true;
			m_sightRadius.setRadius(1000);
			m_sightRadius.setOrigin(m_sightRadius.getRadius(), m_sightRadius.getRadius());
		}
		else if(m_player->IsSpotted() == false || m_player->GetHP() <= 0)
		{
			m_alerted = false;
//			m_sightRadius.setRadius(400);
			m_sightRadius.setOrigin(m_sightRadius.getRadius(), m_sightRadius.getRadius());
		}
	}
}

void EnemyPlane::ShootProjectile()
{
	if (m_bulletCount < 10 && m_shootDelay < sf::Time::Zero)
	{
		
		int speed_difference = clamp(rand() % 4, 2, 4);
		//int direction_difference = clamp(rand() % 2, 1, 2);
		sf::Vector2f direction = sf::Vector2f
			(
				sin(m_sprite->getRotation() * PI / 180.0) * 1.0,
				cos(m_sprite->getRotation() * PI / 180.0) * 1.0
				);
		m_bullets[m_bulletCount] = new Bullet(m_textureManager, speed_difference, direction,
			m_pos, m_sprite);
		//m_bullets[m_bulletCount]->GetSprite().setRotation(m_sprite->getRotation());
		m_shootDelay = sf::milliseconds(100);
		m_fireSound.play();
		clamp(m_bulletCount++, 0, 9);
		clamp(m_shotCount++, 0, 9);
	}
}

bool EnemyPlane::ProjectileCollision()
{
	for (int i = 0; i < 10; i++)
	{
		if (m_bullets[i] != nullptr)
		{
			if (m_player->GetHitbox().getGlobalBounds().intersects(m_bullets[i]->GetSprite().getGlobalBounds()) 
				&& m_player->GetHP() > 0)
			{
				m_player->SetHP(-1);
				delete m_bullets[i];
				m_bullets[i] = nullptr;
				m_bulletCount--;
				return true;
			}
		}
	}
	return false;
}

void EnemyPlane::RotateRight()
{
	if (m_HP >= 2)
	{
		m_animation.x = 1;
		if (m_animationDelay.getElapsedTime().asSeconds() > 2)
		{
			Animation(m_sprite, m_animation, 0, 613, 0, 142, 204, 142);
			m_animationDelay.restart();
		}
	}
	else if (m_HP == 1)
	{
		if (m_animationDelay.getElapsedTime().asSeconds() > 2)
		{
			m_animation.y = 1;
			m_animationDelay.restart();
		}
	}
}

void EnemyPlane::RotateLeft()
{
	if (m_HP >= 2)
	{
		m_animation.x = 2;
		if (m_animationDelay.getElapsedTime().asSeconds() > 2)
		{
			Animation(m_sprite, m_animation, 0, 1683, 0, 142, 204, 142);
			m_animationDelay.restart();
		}
	}
	else if (m_HP == 1)
	{
		if (m_animationDelay.getElapsedTime().asSeconds() > 2)
		{
			m_animation.y = 2;
			m_animationDelay.restart();
		}
	}
}

void EnemyPlane::Draw(sf::RenderWindow& p_window)
{
	//DEBUG F�R SIGHTRADIUS/SHOOTRANGE
	/*p_window.draw(m_sightRadius);
	p_window.draw(m_shootRange);*/
	if (m_HP > 0)
	{
		for (int i = 0; i < 10; i++)
		{
			if (m_bullets[i] != nullptr)
			{
				p_window.draw(m_bullets[i]->GetSprite());
			}
		}
		//Debug-hitbox
		//p_window.draw(m_hitbox);

		p_window.draw(m_searchLight);
	}
	if (m_animation.x < 18 || m_HP > 0)
	{
		p_window.draw(*m_sprite);
	}
	if (m_HP > 0)
	{
		p_window.draw(*m_rotorSprite);
	}
}

void EnemyPlane::PauseSounds()
{
}

void EnemyPlane::SetState(AIStates* p_state)
{
	m_state = p_state;
}
