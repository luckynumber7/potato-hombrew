#pragma once

#include "TextureManager.hpp"

enum EPOWERUP
{
	BAKED_POTATO,
	POTATO_CLOUD,
	SPEED_BOOZE
};

class Powerup
{
public:
	virtual ~Powerup() {};
	virtual bool Update() = 0;
	virtual sf::Sprite* GetSprite() = 0;
	virtual EPOWERUP GetType() = 0;
	virtual sf::RectangleShape GetHitbox() = 0;
private:
};

class BakedPotato : public Powerup
{
public:
	BakedPotato(TextureManager* p_TextureManager, sf::Vector2f p_direction, sf::Vector2f p_start); //sf::Texture p_texturePotato, sf::Texture p_textureExplosion
	~BakedPotato();
	bool Update();
	bool IsExploding();
	sf::Sprite* GetSprite();
	EPOWERUP GetType();
	sf::RectangleShape GetHitbox();
	void draw(sf::RenderWindow& p_window);

private:
	sf::Sprite* m_sprite;
	sf::Texture* m_bakedPotato;
	sf::Texture* m_explosion;
	sf::RectangleShape m_hitbox;

	sf::Vector2f m_center;
	sf::Vector2f m_direction;
	sf::Vector2f m_scale;
	sf::Vector2i m_animation;

	sf::Clock m_clock;
	sf::Time m_timer;

	float m_color;
	bool m_isExploding;


};
//class SpeedBooze : public Powerup
//{
//public:
//	bool Update();
//	sf::Sprite* GetSprite();
//	EPOWERUP GetType();
//private:
//};

class PotatoCloud : public Powerup
{
public:
	PotatoCloud(TextureManager* p_textureManager, sf::Vector2f p_direction, sf::Vector2f p_start, float p_rotation);
	~PotatoCloud();
	bool Update();
	bool IsActive();
	sf::Sprite* GetSprite();
	EPOWERUP GetType();
	sf::RectangleShape GetHitbox();
	void draw(sf::RenderWindow& p_window);

private:
	sf::Texture* m_potatoCloud;
	sf::Sprite* m_sprite;
	sf::RectangleShape m_hitbox;

	sf::Vector2f m_center;
	sf::Vector2f m_direction;
	sf::Vector2f m_scale;
	sf::Vector2i m_animation;

	sf::Clock m_clock;
	sf::Time m_timer;
	sf::Time m_lifetime;

	bool m_active;
	float m_fade;

};