#include "stdafx.hpp"
#include "EnemyZeppelin.hpp"
#include "IEnemy.hpp"
#include "Player.hpp"
#include "AIStates.hpp"
#include "Projectile.hpp"
#include <iostream>

EnemyZeppelin::EnemyZeppelin(Player* p_player, TextureManager* p_textureManager, float p_posX, float p_posY)
{
	m_textureManager = p_textureManager;

	m_player = p_player;
	m_state = nullptr;
	m_alerted = false;

	m_pos.x = p_posX;
	m_pos.y = p_posY;
	m_animation.x = 0;
	m_animation.y = 0;

	m_HP = 5;
	m_speed = 0.10;
	m_turningSpeed = 0.30;
	m_timer = sf::seconds(6);
	m_alarmDelay = sf::seconds(2);

	m_sightRadius.setRadius(700);
	m_sightRadius.setOrigin(m_sightRadius.getRadius(), m_sightRadius.getRadius());
	m_sightRadius.setFillColor(sf::Color(100, 250, 50, 0));
	m_sightRadius.setOutlineThickness(5);
	m_sightRadius.setOutlineColor(sf::Color(250, 0, 0, 50));

	m_texture = m_textureManager->LoadTexture("../assets/zeppelin sprite sheet.png");
	m_deathTex = m_textureManager->LoadTexture("../assets/Zeppelin death animation sprite sheet.png");
	m_searchLightTex = m_textureManager->LoadTexture("../assets/Search Light Sprite sheet.png");
	
	m_sprite = new sf::Sprite;
	m_sprite->setTexture(*m_texture);
	m_sprite->setTextureRect(sf::IntRect(0, 0, 140, 319));

	m_searchLight.setTexture(*m_searchLightTex);
	m_searchLight.setTextureRect(sf::IntRect(144, 0, 144, 746));
	
	m_center.x = 140 / 2; //Mindre om spritesheet blir aktuellt
	m_center.y = 319 / 2;
	m_sprite->setOrigin(m_center);
	m_searchLight.setOrigin(m_center.x, m_searchLight.getTextureRect().height);
	m_state = new Patrol(this, m_player);

	m_hitbox.setSize(sf::Vector2f(100, 300));
	m_hitbox.setOrigin(sf::Vector2f(m_center.x - (m_sprite->getTextureRect().width - m_hitbox.getSize().x) / 2, m_center.y));
	m_hitbox.setFillColor(sf::Color::Blue);
	
	if (!m_alarm.loadFromFile("../assets/sounds/Zeppelin Alarm.wav"))
		std::cout << "ERROR! Could not load sound!" << std::endl;
	if (!m_death.loadFromFile("../assets/sounds/Beta Zeppelinare.wav"))
		std::cout << "ERROR! Could not load sound!" << std::endl;
	m_alarmSound.setBuffer(m_alarm);
	m_alarmSound.setVolume(3);
	m_deathSound.setBuffer(m_death);
	m_deathSound.setVolume(20);
}

EnemyZeppelin::~EnemyZeppelin()
{
	delete m_state;
	delete m_sprite;
}

void EnemyZeppelin::Initialize(sf::Sprite* p_sprite)
{
	
}

void EnemyZeppelin::Update()
{
	if (m_HP > 0)
	{
		if (m_player->IsHidden())
		{
			m_alerted = false;
		}
		if (m_alarmSound.getStatus() == m_alarmSound.Paused)
		{
			m_alarmSound.play();
		} 
		if (m_player->IsSpotted())
		{
			m_searchLight.setColor(sf::Color::Red);

			float ans, ans2, ans3;
			ans = atan2(m_player->GetY() - m_pos.y, m_player->GetX() - m_pos.x);
			float pai = 180 / PI;
			ans *= pai;

			if (ans < 0)
			{
				ans = 360 - (-ans);
			}
			ans2 = static_cast<int>(ans);
			m_searchLight.setRotation(90 + ans2);
		}
		m_pos.x += m_speed * sin(m_sprite->getRotation()*3.14159265 / 180.0) * 1.0;
		m_pos.y += -m_speed * cos(m_sprite->getRotation()*3.14159265 / 180.0) * 1.0;

		if (m_damageFrames <= 0)
		{
			m_sprite->setColor(sf::Color::White);
		}
		m_damageFrames--;
		if (m_animation.x <= 0 && m_animationTimer.getElapsedTime().asMicroseconds() > 6)
		{
			m_animation.x++;
			Animation(m_sprite, m_animation, 0, 281, 0, 319, 140, 319);
			m_animationTimer.restart();
		}
		else
		{
			m_animation.x = 0;
			Animation(m_sprite, m_animation, 0, 281, 0, 319, 140, 319);
			m_animationTimer.restart();
		}
		m_sprite->setPosition(m_pos);
		m_hitbox.setPosition(m_pos);
		m_hitbox.setRotation(m_sprite->getRotation());
		m_searchLight.setPosition(m_pos);
		m_searchLight.rotate(0.03);
		m_sightRadius.setPosition(m_pos.x, m_pos.y);
		SightCheck(m_player->GetSprite());
		m_stateCheck = m_state->HandleState();
		if (m_stateCheck == true)
		{
			m_speed = 0.10;
			m_alarmDelay = sf::seconds(2);
		}
		else if (m_alarmDelay < sf::Time::Zero)
		{
			if (m_alarmSound.getLoop() == false && m_player->IsSpotted() == false)
			{
				m_alarmSound.setLoop(true);
				m_alarmSound.play();
			}
			m_searchLight.setColor(sf::Color::Red);
			m_speed = 0.01;
			m_alarmDelay = sf::seconds(2);
			m_player->setSpotted(true);
			m_player->SetTimer(sf::seconds(5));
		}
		else if (m_stateCheck == false)
		{
			m_speed = 0.01;
			m_alarmDelay -= sf::microseconds(1500);

			float ans, ans2, ans3;
			ans = atan2(m_player->GetY() - m_pos.y, m_player->GetX() - m_pos.x);
			float pai = 180 / PI;
			ans *= pai;

			if (ans < 0)
			{
				ans = 360 - (-ans);
			}
			ans2 = static_cast<int>(ans);
			m_searchLight.setRotation(90 + ans2);
		}
	}
	else if (m_animation.y < 4)
	{
		if (m_animationTimer.getElapsedTime().asMilliseconds() > 75)
		{
			m_animation.x++;
			Animation(m_sprite, m_animation, 0, 1500, 0, 1400, 300, 350);
			m_animationTimer.restart();
			m_sprite->setColor(sf::Color::White);
			if (m_animation.x >= 4)
			{
				m_sprite->setColor(sf::Color::Red);
				m_animation.x = 0;
				m_animation.y++;
			}
		}
	}
	if (m_player->IsSpotted() == false)
	{
		m_alarmSound.setLoop(false);
		m_alarmSound.stop();
		m_searchLight.setColor(sf::Color::White);
	}
	m_invincibilityFrames -= sf::microseconds(100);
}

sf::Sprite* EnemyZeppelin::GetSprite()
{
	return m_sprite;
}

sf::RectangleShape EnemyZeppelin::GetHitBox()
{
	return m_hitbox;
}

EENEMY EnemyZeppelin::GetType()
{
	return ENEMY_ZEPPELIN;
}


bool EnemyZeppelin::IsAlerted()
{
	return m_alerted;
}

void EnemyZeppelin::SetAlerted(bool p_alerted)
{
	m_alerted = p_alerted;
}

sf::CircleShape EnemyZeppelin::GetSightRadius()
{
	return m_sightRadius;
}

void EnemyZeppelin::SetSightRadius(float p_radius)
{
	m_sightRadius.setRadius(p_radius);
	m_sightRadius.setOrigin(p_radius, p_radius);
}

int EnemyZeppelin::GetHP()
{
	return m_HP;
}

void EnemyZeppelin::SetHP(int p_amount)
{
	if (m_invincibilityFrames < sf::Time::Zero)
	{
		
		m_HP += p_amount;
		if (m_HP <= 0)
		{
			m_animation.x = 0;
			m_animation.y = 0;
			m_sprite->setTexture(*m_deathTex);
			m_sprite->setTextureRect(sf::IntRect(0, 0, 300, 350));
			m_sprite->setOrigin(150, 175);
			m_deathSound.play();
		}
		if (m_HP + p_amount < m_HP)
		{
			m_sprite->setColor(sf::Color::Red);
			m_damageFrames = 85;
			m_invincibilityFrames = sf::microseconds(25000);
		}
	}
}

sf::Time EnemyZeppelin::GetTimer()
{
	return m_timer;
}

void EnemyZeppelin::SetPosition(float p_posX, float p_posY)
{
	m_pos.x = p_posX;
	m_pos.y = p_posY;
}

void EnemyZeppelin::AddX(float p_x)
{
	m_pos.x += p_x;
}

void EnemyZeppelin::AddY(float p_y)
{
	m_pos.y += p_y;
}

float EnemyZeppelin::GetX()
{
	return m_pos.x;
}

float EnemyZeppelin::GetY()
{
	return m_pos.y;
}

float EnemyZeppelin::GetCenterX()
{
	return m_center.x;
}

float EnemyZeppelin::GetCenterY()
{
	return m_center.y;
}

void EnemyZeppelin::SetX(float p_x)
{
	m_pos.x = p_x;
}

void EnemyZeppelin::SetY(float p_y)
{
	m_pos.y = p_y;
}

float EnemyZeppelin::GetSpeed()
{
	return m_speed;
}

void EnemyZeppelin::MoveTowards(sf::Vector2f p_pos)
{
	//float mX = sf::Mouse::getPosition(m_window).x; use p_pos.x
	//float mY = sf::Mouse::getPosition(m_window).y; use p_pos.y
	//float posX = m_player.getPosition().x; use p
	//float posY = m_player.getPosition().y;
	float ans = FindAngle(p_pos.x, p_pos.y, m_pos.x, m_pos.y);
	if (p_pos.x > m_pos.x)
	{
		if (p_pos.y > m_pos.y)
		{
			//Undre H�ger
			if (m_sprite->getRotation() > (90 + ans))
			{
				m_sprite->rotate(-m_turningSpeed);
			}
			else if (m_sprite->getRotation() < (90 + ans))
			{
				m_sprite->rotate(m_turningSpeed);
			}
		}
		else if (p_pos.y < m_pos.y)
		{
			//�vre H�ger
			if (m_sprite->getRotation() >(90 - ans))
			{
				m_sprite->rotate(-m_turningSpeed);
			}
			else if (m_sprite->getRotation() < (90 - ans))
			{
				m_sprite->rotate(m_turningSpeed);
			}
		}
	}
	else if (p_pos.x < m_pos.x)
	{
		if (p_pos.y > m_pos.y)
		{
			//Undre V�nster
			if (m_sprite->getRotation() > (270 - ans))
			{
				m_sprite->rotate(-m_turningSpeed);
			}
			else if (m_sprite->getRotation() < (270 - ans))
			{
				m_sprite->rotate(m_turningSpeed);
			}
		}
		else if (p_pos.y < m_pos.y)
		{
			//�vre V�nster
			if (m_sprite->getRotation() >(270 + ans))
			{
				m_sprite->rotate(-m_turningSpeed);
			}
			else if (m_sprite->getRotation() < (270 + ans))
			{
				m_sprite->rotate(m_turningSpeed);
			}
		}
	}

}

void EnemyZeppelin::SightCheck(sf::Sprite* p_sprite)
{
	if (m_sightRadius.getGlobalBounds().intersects(p_sprite->getGlobalBounds()))
	{
		float XDif = abs(m_pos.x) - abs(p_sprite->getPosition().x);
		float YDif = abs(m_pos.y) - abs(p_sprite->getPosition().y);
		float hyp = pow(abs(YDif), 2) + pow(abs(XDif), 2);
		if (sqrt(hyp) < m_sightRadius.getRadius() && !m_player->IsHidden()
				)
		{
			m_alerted = true;
			m_sightRadius.setRadius(700);
			m_sightRadius.setOrigin(m_sightRadius.getRadius(), m_sightRadius.getRadius());
		}
		else
		{
			m_alerted = false;
			m_sightRadius.setRadius(700);
			m_sightRadius.setOrigin(m_sightRadius.getRadius(), m_sightRadius.getRadius());
		}
	}
}

bool EnemyZeppelin::ProjectileCollision()
{
	return false;
}

void EnemyZeppelin::SetState(AIStates* p_state)
{
	m_state = p_state;
}

void EnemyZeppelin::Draw(sf::RenderWindow& p_window)
{
	//DEBUG F�R SIGHTRADIUS
	//p_window.draw(m_sightRadius);
	if (m_HP > 0)
	{
		//p_window.draw(m_hitbox);
		p_window.draw(m_searchLight);
	}
	p_window.draw(*m_sprite);
}

void EnemyZeppelin::PauseSounds()
{
	//if (m_deathSound.getStatus() == sf::Sound::Status::Playing)
	//{
	//	m_deathSound.pause();
	//}
	if (m_alarmSound.getStatus() == m_alarmSound.Playing)
	{
		m_alarmSound.pause();
	}
	
}
