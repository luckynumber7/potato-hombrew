#pragma once

#include "TextureManager.hpp"

class IEnemy;
class Player;

enum EPROJECTILE
{
	PLAYER_POTATO,
	ENEMY_BULLET,
	ENEMY_FLACK
};

class Projectile
{
public:
	virtual ~Projectile() {};
	virtual void Trajectory() = 0;
	virtual sf::Time GetLifetime() = 0;
	virtual EPROJECTILE GetType() = 0;
	virtual sf::Sprite GetSprite() = 0;

private:


};

class Potato : public Projectile
{
public:
	Potato(TextureManager* p_textureManager, float p_speed, sf::Vector2f p_direction, sf::Vector2f p_start);
	~Potato();
	void Trajectory();
	sf::Time GetLifetime();
	EPROJECTILE GetType();
	sf::Sprite GetSprite();
private:
	sf::Texture* m_potato;
	sf::Sprite m_sprite;

	int m_speed;
	float m_color;
	sf::Vector2f m_direction;
	sf::Vector2f m_scale;
	sf::Time m_lifetime;
	sf::Clock m_clock;

};

class Bullet : public Projectile
{
public:
	Bullet(TextureManager* p_textureManager, float p_speed, sf::Vector2f p_direction, sf::Vector2f p_start, sf::Sprite* p_sprite);
	~Bullet();
	void Trajectory();
	sf::Time GetLifetime();
	EPROJECTILE GetType();
	sf::Sprite GetSprite();
private:
	int m_speed;
	float m_color;
	sf::Vector2f m_direction;
	sf::Vector2f m_scale;
	sf::Time m_lifetime;
	sf::Clock m_clock;
	sf::Texture* m_texture;
	sf::Sprite m_sprite;
};
