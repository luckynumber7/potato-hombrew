#include <memory>
#include <iostream>

#include "IntroState.hpp"
#include "PlayState.hpp"
#include "CreditsState.hpp"
#include "StateMachine.hpp"
#include "ScoreState.hpp"

#include "stdafx.hpp"
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML/Audio.hpp>

using namespace std;

ScoreState::ScoreState(StateMachine& machine, sf::RenderWindow& window, TextureManager* p_texureManager, bool replace, bool restart)
	: State{ machine, window, p_texureManager, replace, restart }
{

	if (!font.loadFromFile("../assets/Fonts/Caladea-Bold.ttf"))
	{
		std::cout << "Error: Could not load font" << std::endl;
	}
	if (!font2.loadFromFile("../assets/Fonts/Caladea-Regular.ttf"))
	{
		std::cout << "Error: Could not load font" << std::endl;
	}

	m_dialogTex = m_textureManager->LoadTexture("../assets/highscoreDialog.png");
	m_dialog.setTexture(*m_dialogTex, true);
	m_dialog.setOrigin(380, 300);
	m_dialog.setPosition(screenWidth/2, screenHeight/2);

	m_bgTex = m_textureManager->LoadTexture("../assets/Background.png");
	m_Cloud1Tex = m_textureManager->LoadTexture("../assets/Cloud1.png");
	m_fgTex = m_textureManager->LoadTexture("../assets/Foreground.png");
	m_CloudTex = m_textureManager->LoadTexture("../assets/Cloud.png");

	m_bg.setTexture(*m_bgTex, true);
	m_fg.setTexture(*m_fgTex, true);
	m_Cloud1.setTexture(*m_CloudTex, true);
	m_Cloud2.setTexture(*m_Cloud1Tex, true);

	m_Cloud1.setPosition(0.0f, -100.0f);
	m_Cloud2.setPosition(-500.0f, 200.0f);

	HghScr.setFont(font);
	HghScr.setString("Highscores");
	HghScr.setColor(sf::Color::White);
	HghScr.setCharacterSize(70);
	HghScr.setPosition(760, 290);

	NamesStr.setFont(font2);
	NamesStr.setColor(sf::Color::White);
	NamesStr.setCharacterSize(50);
	NamesStr.setPosition(700, 420);

	ScoresStr.setFont(font2);
	ScoresStr.setColor(sf::Color::White);
	ScoresStr.setCharacterSize(50);
	ScoresStr.setPosition(1025, 420);

	returnText.setFont(font);
	returnText.setColor(sf::Color::White);
	returnText.setCharacterSize(25);
	returnText.setString("Press ESC to return to menu");
	returnText.setPosition(610, 775);
}

void ScoreState::pause()
{

}

void ScoreState::resume()
{

}

void ScoreState::update()
{
	if (m_Cloud1.getPosition().x > 1980)
	{
		m_Cloud1.setPosition(0.0f, -100.0f);
	}
	if (m_Cloud2.getPosition().x > 1980)
	{
		m_Cloud2.setPosition(-500.0f, 200.0f);
	}

	m_Cloud1.move(0.3f, 0.033f);
	m_Cloud2.move(0.3f, 0);

	inname.open("../assets/Scoring/Names.txt");
	inscore.open("../assets/Scoring/Scores.txt");

	for (int i = 0; i <= 4; i++) // To get you all the lines.
	{
		getline(inname, Names[i]);// Saves the line in Names[0-4].

		actNString.append(Names[i]);
		actNString.append("\n");

		NamesStr.setString(actNString);
	}
	inname.close();

	for (int k = 0; k <= 4; k++)
	{
		getline(inscore, Scores[k]);// Saves the line in Scores[0-4].

		actSString.append(Scores[k]);
		actSString.append("\n");

		ScoresStr.setString(actSString);
	}
	inscore.close();

	actNString.erase(0, actNString.length());
	actSString.erase(0, actSString.length());


	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
	{

		ofstream outnames("../assets/Scoring/Names.txt");
		for (int j = 0; j <= 4; j++) // To get you all the lines.
		{
			outnames << Names[j] << endl;
		}
		outnames.close();

		ofstream outscore("../assets/Scoring/Scores.txt");
		for (int k = 0; k <= 4; k++) // To get you all the lines.
		{
			outscore << Scores[k] << endl;
		}
		outscore.close();
		m_next = StateMachine::build<IntroState>(m_machine, m_window, m_textureManager, true, false);
	}
}

void ScoreState::draw()
{

	m_window.clear();
	m_window.draw(m_bg);
	m_window.draw(m_Cloud1);
	m_window.draw(m_fg);
	m_window.draw(m_Cloud2);
	m_window.draw(m_dialog);
	m_window.draw(HghScr);
	m_window.draw(NamesStr);
	m_window.draw(ScoresStr);
	m_window.draw(returnText);
	m_window.display();
}