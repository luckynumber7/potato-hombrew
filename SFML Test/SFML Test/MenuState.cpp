#include "StateMachine.hpp"
#include "MenuState.hpp"
#include "IntroState.hpp"
#include "TextureManager.hpp"

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML\Main.hpp>
#include <SFML\Window.hpp>
#include <SFML\System.hpp>
#include <SFML\Audio.hpp>
#include <SFML\Graphics.hpp>

#include <memory>
#include <iostream>

MenuState::MenuState(StateMachine& machine, sf::RenderWindow& window, TextureManager* p_textureManager, bool replace, bool restart)
	: State{ machine, window, p_textureManager, replace, restart}
{

	if (!m_bgTex.loadFromFile("../assets/pauseBG.png"))
	{
		std::cout << "ERROR! Image could not be loaded." << std::endl;
	}
	m_bg.setTexture(m_bgTex, true);
	m_bg.setPosition(0, 0);
	
	if (!font.loadFromFile("../assets/STENCIL.ttf"))
	{
		std::cout << "ERROR! Font could not be loaded." << std::endl;
	}

	m_ContButtonTex.loadFromFile("../assets/ContButton.png");
	m_POptionsButtonTex.loadFromFile("../assets/POptionsButton.png");
	m_MenuButtonTex.loadFromFile("../assets/MenuButton.png");

	m_ContHoverTex.loadFromFile("../assets/ContHover.png");
	m_POptionsHoverTex.loadFromFile("../assets/OptionsHover.png");
	m_MenuHoverTex.loadFromFile("../assets/MenuHover.png");

	m_ContButton.setTexture(m_ContButtonTex, true);
	m_POptionsButton.setTexture(m_POptionsButtonTex, true);
	m_MenuButton.setTexture(m_MenuButtonTex, true);

	m_ContButton.setPosition(935.0f, 248.0f);
	m_MenuButton.setPosition(936.0f, 373.0f);

	text.setFont(font);
	text.setString("The game is paused");
	text.setColor(sf::Color::White);
	text.setCharacterSize(95);
	text.setPosition(960, 100);
	//std::cout << text.getGlobalBounds().width << " " << text.getGlobalBounds().height << std::endl;
	text.setOrigin(472, 33.5f);

/*	text2.setFont(font);
	text2.setString("Press 'Enter' to continue");
	text2.setColor(sf::Color::White);
	text2.setCharacterSize(32);
	text2.setPosition(1200, 850);

	text3.setFont(font);
	text3.setString("or 'Escape' to quit to Main menu");
	text3.setColor(sf::Color::White);
	text3.setCharacterSize(32);
	text3.setPosition(1200, 900);*/
	

	PauseView = sf::View(sf::Vector2f(960, 540), sf::Vector2f(1920, 1080));
	PauseView.setCenter(960, 540);
	m_window.setView(PauseView);

	std::cout << "MenuState Init" << std::endl;
}

void MenuState::pause()
{
	std::cout << "MenuState Pause" << std::endl;
}

void MenuState::resume()
{
	std::cout << "MenuState Resume" << std::endl;
}

void MenuState::update()
{

	sf::Event event;

	while (m_window.pollEvent(event))
	{

		switch (event.type)
		{
		case sf::Event::Closed:
			m_machine.quit();
			break;

		case sf::Event::KeyPressed:
		{
			switch (event.key.code)
			{
			case sf::Keyboard::S:
				if (hoverButton == 1)
				{
					hoverButton++;
					m_ContButton.setTexture(m_ContButtonTex);
					m_MenuButton.setTexture(m_MenuHoverTex);
				}
				if (hoverButton == 0)
				{
					hoverButton = 1;
					m_ContButton.setTexture(m_ContHoverTex);
				}
				break;

			case sf::Keyboard::W:
				if (hoverButton == 2 || hoverButton == 0)
				{

					hoverButton = 1;
					m_MenuButton.setTexture(m_MenuButtonTex);
					m_ContButton.setTexture(m_ContHoverTex);
				}
				break;

			case sf::Keyboard::Space:
				if (hoverButton == 1)
				{
					m_machine.lastState();
				}
				if (hoverButton == 2)
				{
					m_next = StateMachine::build<IntroState>(m_machine, m_window, m_textureManager, true, true);
				}
				break;

			default:
				break;

			}
		}

		break;
		}
	}
}

void MenuState::draw()
{
	// Clear the previous drawing
	m_window.clear();
	m_window.setView(PauseView);
	m_window.draw(m_bg);
	m_window.draw(text);
	m_window.draw(text2);
	m_window.draw(text3);
	m_window.draw(m_ContButton);
//	m_window.draw(m_POptionsButton);
	m_window.draw(m_MenuButton);
	m_window.display();
}
