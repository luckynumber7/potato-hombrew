#include <memory>
#include <iostream>

#include "IntroState.hpp"
#include "PlayState.hpp"
#include "ControlsState.hpp"
#include "StateMachine.hpp"

#include "stdafx.hpp"
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML/Audio.hpp>



ControlsState::ControlsState(StateMachine& machine, sf::RenderWindow& window, TextureManager* p_textureManager, bool replace, bool restart)
	: State{ machine, window, p_textureManager, replace, restart }
{
	if (!music.openFromFile("../assets/music/AcousticBlues.wav"))
	{

		std::cout << "Could not load music file" << std::endl;
	}

	music.setVolume(30);        //Reduce the volume
	music.setLoop(true);		//Set loop

	music.play();

	if (!font.loadFromFile("../assets/Fonts/Caladea-Bold.ttf"))
	{
		std::cout << "Error: Could not load font" << std::endl;
	}
	if (!font2.loadFromFile("../assets/Fonts/Caladea-Regular.ttf"))
	{
		std::cout << "Error: Could not load font" << std::endl;
	}

	m_bgTex = m_textureManager->LoadTexture("../assets/Background.png");
	m_Cloud1Tex = m_textureManager->LoadTexture("../assets/Cloud1.png");
	m_fgTex = m_textureManager->LoadTexture("../assets/Foreground.png");
	m_CloudTex = m_textureManager->LoadTexture("../assets/Cloud.png");

	m_bg.setTexture(*m_bgTex, true);
	m_fg.setTexture(*m_fgTex, true);
	m_Cloud1.setTexture(*m_CloudTex, true);
	m_Cloud2.setTexture(*m_Cloud1Tex, true);

	m_Cloud1.setPosition(0.0f, -100.0f);
	m_Cloud2.setPosition(-500.0f, 200.0f);

	text.setFont(font);
	text.setString("How To play:");
	text.setColor(sf::Color::Red);
	text.setCharacterSize(70);
	text.setPosition(100, 100);

	m_controlsTex.loadFromFile("../assets/controls.png");
	m_controls.setTexture(m_controlsTex, true);
	m_controls.setPosition(100, 220);

	text1.setFont(font2);
	text1.setString("Your goal is to deliver your cargo of potatoes to the destillery, on the way there you will also");
	text1.setColor(sf::Color::Red);
	text1.setCharacterSize(40);
	text1.setPosition(100, 620);

	text2.setFont(font2);
	text2.setString("have to you will also have to avoid or fight the law enforcement as they try to stop you. But do");
	text2.setColor(sf::Color::Red);
	text2.setCharacterSize(40);
	text2.setPosition(100, 660);

	text3.setFont(font2);
	text3.setString("keep in mind that your cargo is also your ammunition. Return to your farm to pick up more");
	text3.setColor(sf::Color::Red);
	text3.setCharacterSize(40);
	text3.setPosition(100, 700);

	text4.setFont(font2);
	text4.setString("potatoes. The compass in the lower left corner will show you where to go.");
	text4.setColor(sf::Color::Red);
	text4.setCharacterSize(40);
	text4.setPosition(100, 740);

	text5.setFont(font2);
	text5.setString("Deliver as many potatoes as you can before you get shot down!");
	text5.setColor(sf::Color::Red);
	text5.setCharacterSize(50);
	text5.setPosition(100, 800);

	text6.setFont(font);
	text6.setString("Lead Art:");
	text6.setColor(sf::Color::White);
	text6.setCharacterSize(40);
	text6.setPosition(960, 1680);

	text7.setFont(font2);
	text7.setString("Erik Levin");
	text7.setColor(sf::Color::White);
	text7.setCharacterSize(50);
	text7.setPosition(960, 1720);

	text8.setFont(font);
	text8.setString("Additional Art:");
	text8.setColor(sf::Color::White);
	text8.setCharacterSize(40);
	text8.setPosition(960, 1880);

	text9.setFont(font2);
	text9.setString("Gustav Larsson");
	text9.setColor(sf::Color::White);
	text9.setCharacterSize(50);
	text9.setPosition(960, 1920);

	text10.setFont(font2);
	text10.setString("Martin M�nsson");
	text10.setColor(sf::Color::White);
	text10.setCharacterSize(50);
	text10.setPosition(960, 1970);

	text11.setFont(font2);
	text11.setString("Sakarias St�hl");
	text11.setColor(sf::Color::White);
	text11.setCharacterSize(50);
	text11.setPosition(960, 2020);

}

void ControlsState::pause()
{

}

void ControlsState::resume()
{

}

void ControlsState::update()
{
	if (m_Cloud1.getPosition().x > 1980)
	{
		m_Cloud1.setPosition(0.0f, -100.0f);
	}
	if (m_Cloud2.getPosition().x > 1980)
	{
		m_Cloud2.setPosition(-500.0f, 200.0f);
	}

	m_Cloud1.move(0.3f, 0.033f);
	m_Cloud2.move(0.3f, 0);

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
	{
		music.stop();
		m_machine.lastState();
	}
}

void ControlsState::draw()
{

	m_window.clear();
	m_window.draw(m_bg);
	m_window.draw(m_Cloud1);
	m_window.draw(m_fg);
	m_window.draw(m_Cloud2);
	m_window.draw(text);
	m_window.draw(text1);
	m_window.draw(text2);
	m_window.draw(text3);
	m_window.draw(text4);
	m_window.draw(text5);
	m_window.draw(text6);
	m_window.draw(text7);
	m_window.draw(text8);
	m_window.draw(text9);
	m_window.draw(text10);
	m_window.draw(text11);
	m_window.draw(m_controls);
	m_window.display();
}
