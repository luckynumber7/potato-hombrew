#pragma once

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML\Main.hpp>
#include <SFML\Window.hpp>
#include <SFML\System.hpp>
#include <SFML\Audio.hpp>
#include <SFML\Graphics.hpp>

#include <ctime>
#include <random>
#include <vector>
#include <map>
#include <memory>
#include <stack>
#include <string>

const int mapWidth = 7903;
const int mapHeight = 3118;

const int screenWidth = 1920;
const int screenHeight = 1080;

const float PI = 3.14159265;

inline float FindAngle(float p_aX, float p_aY, float p_bX, float p_bY )
{
	float ans, ans2;
	float p_difX = abs(p_aX - p_bX);
	float p_difY = abs(p_aY - p_bY);
	ans2 = p_difY / p_difX;
	ans = atan(ans2) * 180 / PI;
	return ans;
	//anv�nda atan2f y axel, sen x

}

inline int clamp(int a, int b, int c)
{
	if (a < b)
		return b;
	if (a > c)
		return c;
	return a;
}

inline float clampFloat(float a, float b, float c)
{
	if (a < b)
		return b;
	if (a > c)
		return c;
	return a;
}



/*
*Sets texurerect of sprite to a function of sf::Vector2I * width/height of sprite
*sf::Vector2I should contain the position times widht/height of the starting frame, then
increase or change the value of sf::Vector2I x or y values to change frame
-int p_xMin = minimum value of sf::Vector2I.x * width (first frame)
-int p_xMax = maximum value of sf::Vector2I.x * width (last frame)
-int p_yMin = minimum value of sf::Vector2I.y * height (first column)
-int p_yMax = maximum value of sf::Vector2I.y * height (last column)
-int p_width = X-value of texture size of sprite
-int p_height = Y-value of texture size of sprite
*/
inline void Animation(sf::Sprite* p_sprite, sf::Vector2i p_frames, int p_xMin, int p_xMax, int p_yMin, int p_yMax, int p_width, int p_height)
{
	if (p_frames.x * p_width > p_sprite->getTexture()->getSize().x - p_width || p_frames.x * p_width > p_xMax)
	{
		p_sprite->setTextureRect(sf::IntRect(p_xMin, p_frames.y * p_height, p_width, p_height));
	}
	else if (p_frames.y * p_height > p_sprite->getTexture()->getSize().y - p_height || p_frames.y * p_height > p_yMax)
	{
		p_sprite->setTextureRect(sf::IntRect(p_frames.x * p_width, p_yMax, p_width, p_height));
	}
	else
		p_sprite->setTextureRect(sf::IntRect(clamp(p_frames.x * p_width, p_xMin, p_xMax), clamp(p_frames.y * p_height, p_yMin, p_yMax), p_width, p_height));

}

inline void Animation2(sf::Sprite p_sprite, sf::Vector2i p_frames, int p_xMin, int p_xMax, int p_yMin, int p_yMax, int p_width, int p_height)
{
	if (p_frames.x * p_width > p_sprite.getTexture()->getSize().x - p_width || p_frames.x * p_width > p_xMax)
	{
		p_sprite.setTextureRect(sf::IntRect(p_xMin, p_frames.y * p_height, p_width, p_height));
	}
	else if (p_frames.y * p_height > p_sprite.getTexture()->getSize().y - p_height || p_frames.y * p_height > p_yMax)
	{
		p_sprite.setTextureRect(sf::IntRect(p_frames.x * p_width, p_yMax, p_width, p_height));
	}
	else
		p_sprite.setTextureRect(sf::IntRect(clamp(p_frames.x * p_width, p_xMin, p_xMax), clamp(p_frames.y * p_height, p_yMin, p_yMax), p_width, p_height));

}
//WIP
//inline float SoundDistance(sf::Vector2f p_start, sf::Vector2f p_end, sf::Vector2f p_attenuation, float p_maxVolume)
//{
//	sf::Vector2f dist = p_end - p_start;
//	if (dist.x > p_attenuation.x || dist.y > p_attenuation.y)
//	{
//
//		float dist_x = dist.x - p_attenuation.x;
//		float dist_y = dist.y - p_attenuation.y;
//		if (dist_x < dist_y)
//		{
//			return clamp(p_maxVolume / dist_y, 0, p_maxVolume);
//		}
//		if (dist_x > dist_y)
//		{
//			return clamp(p_maxVolume / dist_x, 0, p_maxVolume);
//		}
//	}
//}