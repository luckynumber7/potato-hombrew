#include "State.hpp"

#include <SFML/Audio.hpp>
State::State(StateMachine& machine, sf::RenderWindow &window, TextureManager* p_textureManager, bool replace, bool restart)
	: m_machine{ machine }
	, m_window{ window }
	, m_textureManager{ p_textureManager }
	, m_replacing{ replace }
	, m_restarting{ restart }
{

}

std::unique_ptr<State> State::next()
{
	return std::move(m_next);
}

bool State::isReplacing()
{
	return m_replacing;
}

bool State::isRestarting()
{
	return m_restarting;
}