#include "IntroState.hpp"
#include "PlayState.hpp"
#include "CreditsState.hpp"
#include "ControlsState.hpp"
#include "StateMachine.hpp"
#include "ScoreState.hpp"

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML/Audio.hpp>


#include <iostream>
#include <memory>



IntroState::IntroState(StateMachine& machine, sf::RenderWindow& window, TextureManager* p_textureManager, bool replace, bool restart)
	: State{ machine, window, p_textureManager, replace, restart}
{
	//Sound
	if (!music.openFromFile("../assets/music/Hoedown.wav"))
	{

		//std::cout << "Could not load music file" << std::endl;
	}

	music.setVolume(30);        //Reduce the volume
	music.setLoop(true);		//Set Loop


	music.play();
	
	m_bgTex = m_textureManager->LoadTexture("../assets/Background.png");
	m_Cloud1Tex = m_textureManager->LoadTexture("../assets/Cloud1.png");
	m_fgTex = m_textureManager->LoadTexture("../assets/Foreground.png");
	m_CloudTex = m_textureManager->LoadTexture("../assets/Cloud.png");

	m_IdleButtsTex = m_textureManager->LoadTexture("../assets/MenuButtIdle.png");
	m_PlayHoverTex = m_textureManager->LoadTexture("../assets/StartHover.png");
	m_ScoreHoverTex = m_textureManager->LoadTexture("../assets/ScoreHover.png");
	m_ControlsHoverTex = m_textureManager->LoadTexture("../assets/ControlsHover.png");
	m_CreditsHoverTex = m_textureManager->LoadTexture("../assets/CreditsHover.png");
	m_QuitHoverTex = m_textureManager->LoadTexture("../assets/QuitHover.png");
	m_TitleTex = m_textureManager->LoadTexture("../assets/Title2.png");

	m_bg.setTexture(*m_bgTex, true);
	m_fg.setTexture(*m_fgTex, true);
	m_Cloud1.setTexture(*m_CloudTex, true);
	m_Cloud2.setTexture(*m_Cloud1Tex, true);


	m_IdleButts.setTexture(*m_IdleButtsTex, true);
	m_PlayButton.setTexture(*m_PlayHoverTex, true);
	m_ScoreButton.setTexture(*m_ScoreHoverTex, true);
	m_CreditsButton.setTexture(*m_CreditsHoverTex, true);
	m_ControlsButton.setTexture(*m_ControlsHoverTex, true);
	m_QuitButton.setTexture(*m_QuitHoverTex, true);
	m_Title.setTexture(*m_TitleTex, true);

	m_Cloud1.setPosition(0.0f, -100.0f);
	m_Cloud2.setPosition(-500.0f, 200.0f);

	m_IdleButts.setPosition(721.0f, 540.0f);
	m_PlayButton.setPosition(725.0f, 540.0f);
	m_ScoreButton.setPosition(721.0f, 631.0f);
	m_ControlsButton.setPosition(721.0f, 722.0f);
	m_CreditsButton.setPosition(721.0f, 813.0f);
	m_QuitButton.setPosition(721.0f, 904.0f);
	m_Title.setPosition(628.5f, 0);


	std::cout << "IntroState Init" << std::endl;

	MenuView = sf::View(sf::Vector2f(960, 540), sf::Vector2f(1920, 1080));
	MenuView.setCenter(960, 540);
	m_window.setView(MenuView);
}

IntroState::~IntroState()
{
	std::cout << "Testing if destructor is working" << std::endl;
	//It is
}

void IntroState::pause()
{
	std::cout << "IntroState Pause" << std::endl;
}

void IntroState::resume()
{
	std::cout << "IntroState Resume" << std::endl;
}

void IntroState::update()
{
	if (m_Cloud1.getPosition().x > 1980)
	{
		m_Cloud1.setPosition(0.0f, -100.0f);
	}
	if (m_Cloud2.getPosition().x > 1980)
	{
		m_Cloud2.setPosition(-500.0f, 200.0f);
	}

	m_Cloud1.move(0.3f, 0.033f);
	m_Cloud2.move(0.3f, 0);


	int mouseX = sf::Mouse::getPosition(m_window).x;
	int mouseY = sf::Mouse::getPosition(m_window).y;

	sf::Event event;

	while (m_window.pollEvent(event))
	{

		switch (event.type)
		{
			case sf::Event::Closed:
				m_machine.quit();
				break;

			case sf::Event::KeyPressed:
			{
				switch (event.key.code)
				{
					case sf::Keyboard::S:
						if (hoverButton == 3)
						{
							hoverButton++;
						}
						else if (hoverButton == 2)
						{
							hoverButton++;
						}
						else if (hoverButton == 0)
						{
							hoverButton = 1;
						}
						else if (hoverButton == 5)
						{
							hoverButton = 1;
						}
						else if (hoverButton == 1)
						{
							hoverButton++;
						}
						else if (hoverButton == 4)
						{
							hoverButton++;
						}
						break;

					case sf::Keyboard::W:
						if (hoverButton == 3)
						{
							hoverButton--;
						}
						else if (hoverButton == 4)
						{
							hoverButton--;
						}
						else if (hoverButton == 5)
						{
							hoverButton--;
						}
						else if (hoverButton == 1)
						{
							hoverButton = 5;
						}
						else if (hoverButton == 2 || hoverButton == 0)
						{
							hoverButton = 1;
						}
						break;
						break;


					case sf::Keyboard::Space:
						if (hoverButton == 1)
						{
							music.stop();
							m_next = StateMachine::build<PlayState>(m_machine, m_window, m_textureManager, true, false);
						}
						if (hoverButton == 2)
						{
							music.stop();
							m_next = StateMachine::build<ScoreState>(m_machine, m_window, m_textureManager, false, false);
						}
						if (hoverButton == 3)
						{
							music.stop();
							m_next = StateMachine::build<ControlsState>(m_machine, m_window, m_textureManager, false, false);
						}
						if (hoverButton == 4)
						{
							music.stop();
							m_next = StateMachine::build<CreditsState>(m_machine, m_window, m_textureManager, false, false);
						}
						if (hoverButton == 5)
						{
							m_machine.quit();
						}
						break;

					default:
						break;
				}
			}
			break;
		}
	}
}

void IntroState::draw()
{
	m_window.clear();
	m_window.setView(MenuView);
	m_window.draw(m_bg);
	m_window.draw(m_Cloud1);
	m_window.draw(m_fg);
	m_window.draw(m_Cloud2);


	m_window.draw(m_IdleButts);

	if(hoverButton == 1)
		m_window.draw(m_PlayButton);
	if(hoverButton == 2)
		m_window.draw(m_ScoreButton);
	if(hoverButton == 3)
		m_window.draw(m_ControlsButton);
	if(hoverButton == 4)
		m_window.draw(m_CreditsButton);
	if(hoverButton == 5)
		m_window.draw(m_QuitButton);


	m_window.draw(m_Title);
	m_window.display();
}
