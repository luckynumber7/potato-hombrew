#pragma once
#include "IEnemy.hpp"
#include "TextureManager.hpp"
#include "Powerup.hpp"
#include "Projectile.hpp"

class Player
{
public:
	Player(float p_posX, float p_posY, float p_potatoes, TextureManager* p_textureManager); //Powerup* p_powerup
	~Player();
	void Update();

	void SetPosition(float p_posX, float p_posY);
	void AddX(float p_x);
	void AddY(float p_y);
	float GetX();
	float GetY();
	float GetCenterX();
	float GetCenterY();
	float GetSpeed();
	void SetSpeed(float p_speed);


	float GetPotatoes();
	void SetPotatoes(float p_potatoes);

	sf::Time GetTimer();
	void SetTimer(sf::Time p_duration);


	void ShootProjectile();
	void ActivatePowerup(EPOWERUP p_type);
	bool ProjectileCollision(IEnemy* p_enemy);
	bool PowerupCollision(IEnemy* p_enemy);
	int GetHP();
	void SetHP(int p_amount);
	bool IsDead();
	void setShootDelay(float p_amount);

	float getDamageFrames();

	sf::Sprite* GetSprite();
	sf::RectangleShape GetHitbox();
	void setSpotted(bool p_spotted);
	bool IsSpotted();
	void setHidden(bool p_hidden);
	bool IsHidden();
	void Draw(sf::RenderWindow& p_window);
	void PauseSounds();
	bool PowerupCheck(EPOWERUP p_powerup);
private:
	TextureManager* m_textureManager;
	
	sf::Sprite* m_sprite;
	sf::Sprite* m_speedSprite;
	sf::Sprite* m_rotorSprite;

	Potato* m_potatoes[3];
	BakedPotato* m_bakedPotato;
	PotatoCloud* m_potatoCloud;
	sf::RectangleShape m_hitbox;

	sf::Texture* m_playerTex;
	sf::Texture* m_speedBooze;
	sf::Texture* m_damageTex;
	sf::Texture* m_speedTex;
	sf::Texture* m_rotorTex;
	sf::Texture* m_playerDeath;

	sf::Vector2i m_animation;
	sf::Vector2i m_speedAnimation;
	sf::Vector2i m_rotorAnimation;

	sf::Vector2f m_pos;
	sf::Vector2f m_center;

	sf::Clock m_shootStart;
	sf::Clock m_clock;
	sf::Clock m_boozeDelay;
	
	sf::Time m_shootDelay;
	sf::Time m_powerupDelay;
	sf::Time m_explosionDelay;
	sf::Time m_timer;
	sf::Time m_firingAnimationDelay;
	sf::Time m_speedAnimationDelay;
	sf::Time m_rotorAnimationDelay;

	sf::SoundBuffer m_fire;
	sf::SoundBuffer m_hit;
	sf::SoundBuffer m_damaged;
	sf::SoundBuffer m_explosion;
	sf::SoundBuffer m_potatoCloudClip;
	sf::SoundBuffer m_death;
	sf::SoundBuffer m_motor;
	sf::Sound m_potatoCloudSound;
	sf::Sound m_projectileSounds;
	sf::Sound m_damagedSounds;
	sf::Sound m_deathSound;
	sf::Sound m_sfxSounds;
	sf::Sound m_explosionSound;
	sf::Sound m_motorSound;

	int m_pitch;

	float m_speed;
	int m_HP;
	int m_potatoCounter;
	int m_fireFrames;
	int m_damageFrames;
	bool m_spotted;
	bool m_hidden;
	bool m_firing;
	bool m_boozeActive;
	bool m_boozeHeld;
	bool m_isDead;
	bool m_isPaused;
};