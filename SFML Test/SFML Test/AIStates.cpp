#include "stdafx.hpp"
#include "AIStates.hpp"
#include "Player.hpp"
#include "IEnemy.hpp"
#include "EnemyPlane.hpp"
#include <iostream>

//Patrol


Patrol::Patrol(IEnemy* p_enemy, Player* p_player)
{
	m_owner = p_enemy;
	m_target = p_player;
	m_randPos.x = rand() % mapWidth - 100;
	m_randPos.y = rand() % mapHeight - 200;
	if(m_owner->GetType() == ENEMY_PLANE)
		m_owner->SetSightRadius(400);
}

Patrol::~Patrol()
{
}


bool Patrol::HandleState()
{
	if (m_owner->IsAlerted() == false)
	{
		m_owner->MoveTowards(m_randPos);
		//	std::cout << "moving to X " << m_randPos.x << "  Y " << m_randPos.y << std::endl;
		if (abs(m_owner->GetX() - m_randPos.x) < 50 && abs(m_owner->GetY() - m_randPos.y) < 50)
		{
			int max_x = mapWidth - 50;
			int max_y = mapHeight - 200;
			//	std::cout << "Found it!" << std::endl;
			m_randPos.x = rand() % max_x;
			m_randPos.y = rand() % max_y;
		}
		else if (m_owner->GetX() < -50)
		{
			int max_x = mapWidth / 2;
			int max_y = mapHeight / 2;
			m_randPos.x = rand() % max_x;
			m_randPos.y = rand() % max_y;
			m_owner->GetSprite()->setRotation(90);
			m_owner->AddX(50);
		}
		else if (m_owner->GetY() < -50)
		{
			int max_x = mapWidth / 2;
			int max_y = mapHeight / 2;
			m_randPos.x = rand() % max_x;
			m_randPos.y = rand() % max_y;
			m_owner->GetSprite()->setRotation(180);
			m_owner->AddY(50);
		}
		else if (m_owner->GetX() > mapWidth)
		{
			int max_x = mapWidth / 2;
			int max_y = mapHeight / 2;
			m_randPos.x = rand() % max_x;
			m_randPos.y = rand() % max_y;
			m_owner->GetSprite()->setRotation(270);
			m_owner->AddX(-50);
		}
		else if (m_owner->GetY() > mapHeight)
		{
			int max_x = mapWidth / 2;
			int max_y = mapHeight / 2;
			m_randPos.x = rand() % max_x;
			m_randPos.y = rand() % max_y;
			m_owner->GetSprite()->setRotation(0);
			m_owner->AddY(-50);
		}

		//G�r position p� karta, r�r dig mot position.
		//N�r position �r n�dd, g�r ny position.
		//Kolla ifall sektion i karta �r tom innan
		//position anges.
		return true;
	}
	else
		return false;
}

ENUM_AISTATE Patrol::GetState()
{
	return PATROL;
}


//Chase
Chase::Chase(IEnemy* p_enemy, Player* p_player)
{
	m_owner = p_enemy;
	m_target = p_player;
	m_owner->SetSightRadius(1000);
}

Chase::~Chase()
{
}

bool Chase::HandleState()
{
	if (m_owner->IsAlerted() == false)
	{
		return false;
	}
	m_owner->MoveTowards(m_target->GetSprite()->getPosition());
	if (m_owner->GetX() < -50)
	{
		int max_x = mapWidth / 2;
		int max_y = mapHeight / 2;
		m_owner->GetSprite()->setRotation(90);
		m_owner->AddX(50);
	}
	else if (m_owner->GetY() < -50)
	{
		int max_x = mapWidth / 2;
		int max_y = mapHeight / 2;
		m_owner->GetSprite()->setRotation(180);
		m_owner->AddY(50);
	}
	else if (m_owner->GetX() > mapWidth)
	{
		int max_x = mapWidth / 2;
		int max_y = mapHeight / 2;
		m_owner->GetSprite()->setRotation(270);
		m_owner->AddX(-50);
	}
	else if (m_owner->GetY() > mapHeight)
	{
		int max_x = mapWidth / 2;
		int max_y = mapHeight / 2;
		m_owner->GetSprite()->setRotation(0);
		m_owner->AddY(-50);
	}
	return true;
}

ENUM_AISTATE Chase::GetState()
{
	return CHASE;
}


//Search
Search::Search(IEnemy* p_enemy, Player* p_player, sf::Vector2f p_waypoint)
{
	m_owner = p_enemy;
	m_target = p_player;
	m_waypoint = p_waypoint;
	m_timer = m_owner->GetTimer();
	m_owner->SetSightRadius(1000);
}

Search::~Search()
{
}

bool Search::HandleState()
{
	if (m_owner->IsAlerted() == true || m_timer < sf::Time::Zero)
		return false;
	else
	{
		m_owner->MoveTowards(m_waypoint);
		m_timer -= m_clock.restart();
	}
}

ENUM_AISTATE Search::GetState()
{
	return SEARCH;
}
