#ifndef MENUSTATE_HPP
#define MENUSTATE_HPP

#include "State.hpp"
#include "TextureManager.hpp"

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML\Main.hpp>
#include <SFML\Window.hpp>
#include <SFML\System.hpp>
#include <SFML\Audio.hpp>
#include <SFML\Graphics.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>

class StateMachine;

namespace sf
{
	class RenderWindow;
}

class MenuState : public State
{
public:
	MenuState(StateMachine& machine, sf::RenderWindow& window, TextureManager* p_textureManager, bool replace = false, bool restart = false);

	void pause();
	void resume();

	void update();
	void draw();

private:

	sf::Texture m_ContButtonTex;
	sf::Texture m_POptionsButtonTex;
	sf::Texture m_MenuButtonTex;

	sf::Texture m_ContHoverTex;
	sf::Texture m_POptionsHoverTex;
	sf::Texture m_MenuHoverTex;

	sf::Sprite m_ContButton;
	sf::Sprite m_POptionsButton;
	sf::Sprite m_MenuButton;

	sf::Font font;
	sf::Text text;
	sf::Text text2;
	sf::Text text3;

	sf::Texture m_bgTex;
	sf::Sprite m_bg;

	sf::View PauseView;

	float hoverButton;
};

#endif // MENUSTATE_HPP
