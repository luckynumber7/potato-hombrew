#include "stdafx.hpp"
#include "Player.hpp"

#include <iostream>

Player::Player(float p_posX, float p_posY, float p_potatoes, TextureManager* p_textureManager) //, Powerup* p_powerup
{
	m_textureManager = p_textureManager;

	m_HP = 100;
	m_potatoCounter = p_potatoes;
	m_speed = 0.70;
	m_fireFrames = 0;
	
	m_firing = false;
	m_spotted = false;
	m_hidden = false;
	m_boozeActive = false;
	m_boozeHeld = false;
	m_isDead = false;
	m_isPaused = false;

	m_timer = sf::seconds(5);
	m_shootDelay = sf::seconds(-1);
	m_firingAnimationDelay = sf::seconds(0);
	m_speedAnimationDelay = sf::seconds(0);
	m_rotorAnimationDelay = sf::seconds(0);
	
	m_pos.x = p_posX;
	m_pos.y = p_posY;
	m_animation = sf::Vector2i(0, 1);
	m_speedAnimation = sf::Vector2i(0, 0);
	m_rotorAnimation = sf::Vector2i(0, 0);
	
	//sf::Sprite* sprite = spritemanager->CreateSprite("Player Plane Sprite Sheet 2.png", 0, 0, 50, 64)
	//m_potatoTex = m_textureManager->LoadTexture("../assets/PotatoBullet.png");
	/*m_bakedPotatoTex = new sf::Texture;
	m_bakedPotatoTex->loadFromFile("../assets/Baked Potato sprite.png");
	m_potatoExlosionTex = new sf::Texture;
	m_potatoExlosionTex->loadFromFile("../assets/Baked potato explosion sprite sheet.png");*/
	//m_bakedPotatoTex = m_textureManager->LoadTexture("../assets/Baked Potato sprite.png");
	//m_potatoExlosionTex = m_textureManager->LoadTexture("../assets/Baked potato explosion sprite sheet.png");

	m_playerTex = m_textureManager->LoadTexture("../assets/PlayerPlane Spritesheet.png");
	m_playerDeath = m_textureManager->LoadTexture("../assets/PlayerPlane Death Animation Spritesheet.png");
	m_damageTex = m_textureManager->LoadTexture("../assets/Bullet Impact Spritesheet.png");
	m_speedTex = m_textureManager->LoadTexture("../assets/Speed booze sprite sheet.png");
	m_rotorTex = m_textureManager->LoadTexture("../assets/Propeller animation sprite sheet.png");

	m_sprite = new sf::Sprite;
	m_sprite->setTexture(*m_playerTex);
	m_sprite->setTextureRect(sf::IntRect(0, 0, 133, 150));

	m_speedSprite = new sf::Sprite;
	m_speedSprite->setTexture(*m_speedTex);
	m_speedSprite->setTextureRect(sf::IntRect(0, 0, 131, 280));

	m_rotorSprite = new sf::Sprite;
	m_rotorSprite->setTexture(*m_rotorTex);
	m_rotorSprite->setTextureRect(sf::IntRect(0, 0, 66, 8));

	m_center.x = 133 / 2;
	m_center.y = 150 / 2;
	m_sprite->setOrigin(m_center);
	m_speedSprite->setOrigin(m_center);
	m_rotorSprite->setOrigin(m_center.x / 2, m_center.y);
	m_sprite->setRotation(180);

	m_hitbox.setSize(sf::Vector2f(60, 130));
	m_hitbox.setOrigin(sf::Vector2f(m_center.x - (m_sprite->getTextureRect().width - m_hitbox.getSize().x) / 2, m_center.y));
	m_hitbox.setFillColor(sf::Color::Blue);
	
	for (int i = 0; i < 3; i++)
	{
		m_potatoes[i] = nullptr;
	}
	m_bakedPotato = nullptr;
	m_potatoCloud = nullptr;

	if (!m_motor.loadFromFile("../assets/sounds/Flygplan Motor.wav"))
		std::cout << "ERROR! Could not load sound!" << std::endl;
	if (!m_fire.loadFromFile("../assets/sounds/potato.wav"))
		std::cout << "ERROR! Could not load sound!" << std::endl;
	if(!m_hit.loadFromFile("../assets/sounds/Feedback Potato.wav"))
		std::cout << "ERROR! Could not load sound!" << std::endl;
	if (!m_damaged.loadFromFile("../assets/sounds/Feedback Machinegun L�ng.wav"))
		std::cout << "ERROR! Could not load sound!" << std::endl;
	if (!m_death.loadFromFile("../assets/sounds/Plane Goes Down.wav"))
		std::cout << "ERROR! Could not load sound!" << std::endl;
	if (!m_explosion.loadFromFile("../assets/sounds/Plane Crashing.wav"))
		std::cout << "ERROR! Could not load sound!" << std::endl;
	if (!m_potatoCloudClip.loadFromFile("../assets/sounds/Potato Cloud.wav"))
		std::cout << "ERROR! Could not load sound!" << std::endl;

	m_deathSound.setBuffer(m_death);
	m_explosionSound.setBuffer(m_explosion);
	m_explosionSound.setVolume(50);
	m_damagedSounds.setBuffer(m_damaged);
	m_damagedSounds.setVolume(10);
	m_potatoCloudSound.setBuffer(m_potatoCloudClip);
	m_motorSound.setBuffer(m_motor);
	m_motorSound.setLoop(true);
	m_motorSound.play();
	m_projectileSounds.setVolume(120);
}

Player::~Player()
{

	for (int i = 0; i < 3; i++)
	{
		if (m_potatoes[i] != nullptr)
		{
			delete m_potatoes[i];
			m_potatoes[i] = nullptr;
		}
	}
	if (m_bakedPotato != nullptr)
	{
		delete m_bakedPotato;
	}
	if (m_potatoCloud != nullptr)
	{
		delete m_potatoCloud;
	}
	delete m_speedSprite;
	delete m_rotorSprite;
	delete m_sprite;
}

void Player::Update()
{
	//gunPitch = rand() % 5 + -1;
	//sound.setPitch(gunPitch);
	if (m_isPaused)
	{
		m_clock.restart();
		m_isPaused = false;
	}
	if (m_HP > 0)
	{
		if (m_motorSound.getStatus() == m_motorSound.Paused || m_motorSound.Stopped)
		{
			m_motorSound.play();
		}
		else if (m_speed <= 0.7)
		{
			m_motorSound.setVolume(75);
			if (m_HP > 50)
			{
				m_motorSound.setPitch(1);
			}
			else if (m_HP > 20)
			{
				m_motorSound.setPitch(0.8);
			}
			else
			{
				m_motorSound.setPitch(0.6);
			}
		}

		if (m_speedAnimationDelay < sf::Time::Zero && m_boozeHeld == true && m_potatoCounter > 0)
		{
			m_speedAnimation.x++;
			if (m_speedAnimation.x > 5)
			{
				m_speedAnimation.x = 0;
			}
			Animation(m_speedSprite, m_speedAnimation, 0, 788, 0, 280, 131, 280);
			m_speedAnimationDelay = sf::milliseconds(2000);
		}
		else
		{
			m_speedAnimationDelay -= sf::milliseconds(100);
		}
		if (m_boozeDelay.getElapsedTime().asSeconds() > 0.3 && m_boozeHeld == true && m_potatoCounter > 0)
		{
			m_potatoCounter--;
			m_boozeDelay.restart();
		}
		else if (m_boozeActive == false)
		{
			m_speed = 0.7;
		}

		if (m_rotorAnimationDelay < sf::Time::Zero)
		{
			m_rotorAnimation.x++;
			if (m_rotorAnimation.x > 7)
			{
				m_rotorAnimation.x = 0;
			}
			Animation(m_rotorSprite, m_rotorAnimation, 0, 531, 0, 8, 66, 8);
			m_rotorAnimationDelay = sf::milliseconds(2000);
		}
		else
		{
			m_rotorAnimationDelay -= sf::milliseconds(50);
		}


		m_pos.x += m_speed * sin(m_sprite->getRotation()*3.14159265 / 180.0) * 1.0;
		m_pos.y += -m_speed * cos(m_sprite->getRotation()*3.14159265 / 180.0) * 1.0;
		m_sprite->setPosition(m_pos);
		
		m_hitbox.setPosition(m_pos);
		m_hitbox.setRotation(m_sprite->getRotation());

		m_speedSprite->setPosition(m_pos);
		m_speedSprite->setRotation(m_sprite->getRotation());
		m_rotorSprite->setPosition(m_pos);
		m_rotorSprite->setRotation(m_sprite->getRotation());

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
		{
			m_sprite->rotate(0.2f);
			if (m_firing == false)
			{
				m_animation.x = 5;
			}
			Animation(m_sprite, m_animation, 0, 1995, 0, 0, 133, 150);
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
		{
			m_sprite->rotate(-0.2f);
			if (m_firing == false)
			{
				m_animation.x = 10;
			}
			Animation(m_sprite, m_animation, 0, 1995, 0, 0, 133, 150);
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad1) || sf::Keyboard::isKeyPressed(sf::Keyboard::Num1))
		{
			ActivatePowerup(BAKED_POTATO);
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad2) || sf::Keyboard::isKeyPressed(sf::Keyboard::Num2))
		{
			m_boozeHeld = true;
			ActivatePowerup(SPEED_BOOZE);
		}
		else
		{
			m_boozeActive = false;
			m_boozeHeld = false;
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad3) || sf::Keyboard::isKeyPressed(sf::Keyboard::Num3))
		{
			ActivatePowerup(POTATO_CLOUD);
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
		{
			ShootProjectile();
		}
		//DEBUG HP KEY
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::H))
		{
			m_HP = 10000;
		}
		if (m_firing == true && m_firingAnimationDelay < sf::Time::Zero)
		{
			m_fireFrames++;
			m_animation.x++;
			if (m_fireFrames > 4)
			{
				m_firing = false;
				m_fireFrames = 0;
				clamp(m_animation.x -= 5, 0, 15);
			}
			Animation(m_sprite, m_animation, 0, 1995, 0, 0, 133, 150);
			m_firingAnimationDelay = sf::milliseconds(30);
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::A) == false && sf::Keyboard::isKeyPressed(sf::Keyboard::D) == false && m_firing == false)
		{
			m_animation.x = 0;
			Animation(m_sprite, m_animation, 0, 1995, 0, 0, 133, 150);
		}
		m_firingAnimationDelay -= sf::microseconds(1000);

		if (m_sprite->getPosition().y < -100)
		{
			m_sprite->setRotation(180);
			m_pos.y += 10;
		}
		else if (m_sprite->getPosition().x < -100)
		{
			m_sprite->setRotation(90);
			m_pos.x += 10;
		}
		else if (m_sprite->getPosition().y > mapHeight + 50)
		{
			m_sprite->setRotation(0);
			m_pos.y -= 10;
		}
		else if (m_sprite->getPosition().x > mapWidth + 50)
		{
			m_sprite->setRotation(270);
			m_pos.x -= 10;
		}

		if (m_spotted == true && m_timer > sf::Time::Zero)
		{
			m_timer -= sf::microseconds(400);
		}
		else
		{
			m_spotted = false;
			m_timer = sf::seconds(5);
		}
		if (m_damageFrames <= 0)
		{
			m_sprite->setColor(sf::Color::White);
		}
		m_damageFrames--;
		{
			bool temp = true;
			if (m_bakedPotato != nullptr)
			{
				temp = m_bakedPotato->Update();
				if (temp == false)
				{
					delete m_bakedPotato;
					m_bakedPotato = nullptr;
					m_explosionSound.stop();
				}
				else if (m_bakedPotato->IsExploding() && m_explosionSound.getStatus() != m_explosionSound.Playing)
				{
					m_explosionSound.play();
				}
			}
		}
		{
			bool temp = true;
			if (m_potatoCloud != nullptr)
			{
				temp = m_potatoCloud->Update();
				if (temp == false)
				{
					delete m_potatoCloud;
					m_potatoCloud = nullptr;
				}
			}
		}
		for (int i = 0; i < 3; i++)
		{
			if (m_potatoes[i] != nullptr)
			{
				m_potatoes[i]->Trajectory();
				if (m_potatoes[i]->GetLifetime() < sf::Time::Zero)
				{
					delete m_potatoes[i];
					m_potatoes[i] = nullptr;
				}
			}
		}
	}
	else if (m_animation.x > 0 && m_deathSound.getPitch() == 1)
	{
		m_sprite->setColor(sf::Color::White);
		m_animation.x = 0;
		m_deathSound.setPitch(0.7);
		Animation(m_sprite, m_animation, 0, 2394, 0, 0, 133, 150);
	}
	else
	{
		if (m_animation.x < 18)
		{
			m_sprite->setTexture(*m_playerDeath);
			if (m_shootDelay < sf::Time::Zero)
			{
				if (m_animation.x == 0)
				{
					m_motorSound.stop();
					m_deathSound.play();
				}
				Animation(m_sprite, m_animation, 0, 2394, 0, 0, 133, 150);
				clamp(m_animation.x++, 1, 18);
				m_shootDelay = sf::milliseconds(75);
			}
			m_sprite->rotate(0.3);
			m_pos.x += (m_speed / 2)  * sin(m_sprite->getRotation()*3.14159265 / 180.0) * 1.0;
			m_pos.y += -(m_speed / 2)* cos(m_sprite->getRotation()*3.14159265 / 180.0) * 1.0;
			m_sprite->setPosition(m_pos);
		}
		else if(m_explosionSound.getPitch() == 1)
		{
			m_explosionSound.setPitch(1.2);
			m_explosionSound.play();
		}
		else if (m_animation.x == 18)
		{
			m_isDead = true;
		}
		m_spotted = false;
		m_hidden = false;
	}
	//m_powerupDelay -= m_clock.restart();
	m_shootDelay -= m_clock.restart();
}

void Player::SetPosition(float p_posX, float p_posY)
{
	m_pos.x = p_posX;
	m_pos.y = p_posX;
}

void Player::AddX(float p_x)
{
	m_pos.x += p_x;
}

void Player::AddY(float p_y)
{
	m_pos.y += p_y;
}

float Player::GetX()
{
	return m_pos.x;
}

float Player::GetY()
{
	return m_pos.y;
}

float Player::GetCenterX()
{
	return m_center.x;
}

float Player::GetCenterY()
{
	return m_center.y;
}

float Player::GetSpeed()
{
	return m_speed;
}

void Player::SetSpeed(float p_speed)
{
	m_speed = p_speed;
}


float Player::GetPotatoes()
{
	return m_potatoCounter;
}

void Player::SetPotatoes(float p_potatoes)
{
	m_potatoCounter = p_potatoes;

}

sf::Time Player::GetTimer()
{
	return m_timer;
}

void Player::SetTimer(sf::Time p_duration)
{
	m_timer = p_duration;
}

void Player::ShootProjectile()
{
	for (int i = 0; i < 3; i++)
	{
		if (m_shootDelay < sf::Time::Zero && m_potatoes[i] == nullptr  && m_potatoCounter > 0)
		{
			m_potatoCounter--;
			m_firing = true;
			int speed_difference = clamp(rand() % 3, 2, 3);
			//int direction_difference = clamp(rand() % 2, 1, 2);
			sf::Vector2f direction = sf::Vector2f
				(
					sin(m_sprite->getRotation() * PI / 180.0) * 1.0,
					cos(m_sprite->getRotation() * PI / 180.0) * 1.0
					);
			m_potatoes[i] = new Potato(m_textureManager, speed_difference, direction, m_pos);
			m_shootDelay = sf::milliseconds(1200);

			/*	int Rand = clamp(rand() % 5, 1, 5);
			float pitch = static_cast<float>(Rand);
			m_sound.setPitch(pitch / 2);*/
			//m_sound.setVolume(120);
			m_projectileSounds.setBuffer(m_fire);
			m_projectileSounds.play();
		}
	}
	
}

void Player::ActivatePowerup(EPOWERUP p_type)
{
	sf::Vector2f direction = sf::Vector2f
		(
			sin(m_sprite->getRotation() * PI / 180.0) * 1.0,
			cos(m_sprite->getRotation() * PI / 180.0) * 1.0
			);
	switch (p_type)
	{
	case BAKED_POTATO:
		if (m_bakedPotato == nullptr && m_potatoCounter > 5)
		{
			m_potatoCounter -= 5;
			m_bakedPotato = new BakedPotato(m_textureManager, direction, m_pos);
		}
		break;
	case SPEED_BOOZE:
		if (m_potatoCounter > 0)
		{
			m_boozeActive = true;
			m_speed = 1.2;
			m_motorSound.setPitch(1.5);
		}
		else
		{
			m_boozeActive = false;
		}
		break;
	case POTATO_CLOUD:
		if (m_potatoCloud == nullptr && m_potatoCounter > 10)
		{
			m_potatoCounter -= 10;

			m_potatoCloudSound.setVolume(100);
			m_potatoCloudSound.setPitch(0.8);
			m_potatoCloudSound.play();

			m_potatoCloud = new PotatoCloud(m_textureManager, direction, m_pos, m_sprite->getRotation());
		}
		break;
	default:
		break;
	}
}

bool Player::ProjectileCollision(IEnemy* p_enemy)
{
	for (int i = 0; i < 3; i++)
	{
		if (m_potatoes[i] != nullptr)
		{
			if (p_enemy->GetHitBox().getGlobalBounds().intersects(m_potatoes[i]->GetSprite().getGlobalBounds())
				&& p_enemy->GetHP() > 0 && p_enemy->GetType() != ENEMY_TURRET)
			{
				/*if (p_enemy->IsAlerted())
				{
					p_enemy->SetHP(-2);
				}*/
				p_enemy->SetHP(-1);
				p_enemy->SetAlerted(true);
				delete m_potatoes[i];
				m_potatoes[i] = nullptr;

		/*		delete m_sfx[0];
				m_sfx[0] = new sf::Sprite;
				m_sfx[0]->setTexture(*m_damageTex, false);
				m_sfx[0]->setTextureRect(sf::IntRect(0, 0, 25, 24));
				int randX = p_enemy->GetX() + rand() % p_enemy->GetSprite()->getTexture()->getSize().x;
				int randY = p_enemy->GetY() + rand() % p_enemy->GetSprite()->getTexture()->getSize().y;*/

				m_projectileSounds.setBuffer(m_hit);
				m_projectileSounds.play();
				return true;
			}
		}
	}
	return false;
}

bool Player::PowerupCollision(IEnemy* p_enemy)
{
	if (m_bakedPotato != nullptr && m_bakedPotato->IsExploding() == true && 
	m_bakedPotato->GetHitbox().getGlobalBounds().intersects(p_enemy->GetHitBox().getGlobalBounds())) //And invincibility frames != true
	{
		p_enemy->SetHP(-2);
		return true;
	}
	if (m_potatoCloud != nullptr && m_potatoCloud->IsActive() == true &&
		m_potatoCloud->GetHitbox().getGlobalBounds().intersects(p_enemy->GetHitBox().getGlobalBounds())
		&& p_enemy->GetType() != ENEMY_TURRET)
	{
		if (m_potatoCloudSound.getStatus() != sf::Sound::Status::Playing)
		{
			m_potatoCloudSound.setVolume(50);
			m_potatoCloudSound.setPitch(1.5);
			m_potatoCloudSound.play();
		}
		p_enemy->SetHP(-1);
		return true;
	}
	return false;
}

int Player::GetHP()
{
	return m_HP;
}

void Player::SetHP(int p_amount)
{
	clamp(m_HP += p_amount, 0, 100);
	if (p_amount + m_HP < m_HP)
	{
		int randPitch = clamp(rand() % 5, 1, 5);
		float Pitch = 0.5 + static_cast<float>(randPitch) / 1.50;
		m_sprite->setColor(sf::Color::Red);
		m_damagedSounds.setPitch(Pitch);
		m_damagedSounds.play();
		m_damageFrames = 50;
	}
}

bool Player::IsDead()
{
	return m_isDead;
}

void Player::setShootDelay(float p_amount)
{
	m_shootDelay = sf::seconds(p_amount);
	m_isPaused = true;
}

float Player::getDamageFrames()
{
	return m_damageFrames;
}

sf::Sprite* Player::GetSprite()
{
	return m_sprite;
}

sf::RectangleShape Player::GetHitbox()
{
	return m_hitbox;
}

void Player::setSpotted(bool p_spotted)
{
	m_spotted = p_spotted;
}

bool Player::IsSpotted()
{
	return m_spotted;
}

void Player::setHidden(bool p_hidden)
{
	m_hidden = p_hidden;
}

bool Player::IsHidden()
{
	return m_hidden;
}

void Player::Draw(sf::RenderWindow& p_window)
{
	if (m_HP > 0)
	{
		//p_window.draw(m_hitbox);
		for (int i = 0; i < 3; i++)
		{
			if (m_potatoes[i] != nullptr)
			{
				p_window.draw(m_potatoes[i]->GetSprite());
			}
		}
		if (m_bakedPotato != nullptr)
		{
			m_bakedPotato->draw(p_window);
		}
		if (m_potatoCloud != nullptr)
		{
			m_potatoCloud->draw(p_window);
		}
	}
	if (m_animation.x <= 18)
	{
		p_window.draw(*m_sprite);
	}
	if (m_HP > 0)
	{
		p_window.draw(*m_rotorSprite);
	}
	if (m_speed > 0.7)
	{
		p_window.draw(*m_speedSprite);
	}
}

void Player::PauseSounds()
{
	if (m_motorSound.getStatus() == m_motorSound.Playing)
	{
		m_motorSound.pause();
	}
}

bool Player::PowerupCheck(EPOWERUP p_powerup)
{
	if (p_powerup == SPEED_BOOZE && m_speed > 0.7)
	{
		return true;
	}
	else if (m_bakedPotato != nullptr)
	{
		if (m_bakedPotato->GetType() == p_powerup)
		{
			return true;
		}
		else if (m_potatoCloud != nullptr)
		{
			return true;
		}
	}
	else if (m_potatoCloud != nullptr)
	{
		if (m_potatoCloud->GetType() == p_powerup)
		{
			return true;
		}
		else if (m_bakedPotato != nullptr)
		{
			return true;
		}

	}
	return false;
}
