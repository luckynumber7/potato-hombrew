#include "stdafx.hpp"
#include "EnemyTurret.hpp"
#include "IEnemy.hpp"
#include "Player.hpp"
#include "AIStates.hpp"
#include "Projectile.hpp"
#include <iostream>

EnemyTurret::EnemyTurret(Player* p_player, TextureManager* p_textureManager, float p_posX, float p_posY)
{
	m_textureManager = p_textureManager;

	m_player = p_player;

	m_pos.x = p_posX;
	m_pos.y = p_posY;
	m_animation = sf::Vector2i(0, 1);
	m_alarmVolume = 90;

	m_sightRadius.setRadius(700);
	m_sightRadius.setOrigin(m_sightRadius.getRadius(), m_sightRadius.getRadius());
	m_sightRadius.setFillColor(sf::Color(100, 250, 50, 20));

	m_texture = m_textureManager->LoadTexture("../assets/Anti Aircraft Artillery Sprite Sheet.png");
	m_sprite = new sf::Sprite;
	m_sprite->setTexture(*m_texture);
	m_sprite->setTextureRect(sf::IntRect(0, 0, 40, 80));

	m_searchLightTex = m_textureManager->LoadTexture("../assets/Search Light Sprite sheet.png");
	m_searchLight.setTexture(*m_searchLightTex);
	m_searchLight.setTextureRect(sf::IntRect(144, 0, 144, 746));
	m_searchLight.setColor(sf::Color(255, 255, 255, 50));

	m_center.x = 20;
	m_center.y = 60;
	m_sprite->setOrigin(m_center);
	m_searchLight.setOrigin(m_center.x + 35, m_searchLight.getTextureRect().height);
	m_searchLight.setPosition(m_pos);

	m_hitbox.setSize(sf::Vector2f(40, 40));
	m_hitbox.setOrigin(m_center.x, m_center.y / 3);
	m_hitbox.setFillColor(sf::Color::Blue);

	m_HP = 20;
	bool m_alerted = false;
	bool m_hasFired = false;
	m_speed = 0.00;
	m_timer = sf::seconds(6);
	m_shootDelay = sf::seconds(1);
	for (int i = 0; i < 10; i++)
	{
		m_bullets[i] = nullptr;
	}

	if (!m_fireClip.loadFromFile("../assets/sounds/AAgun.wav"))
		std::cout << "ERROR! Could not load sound!" << std::endl;
	if (!m_alarmClip.loadFromFile("../assets/sounds/Anti Aircraft Artillery Alarm Sound.wav"))
		std::cout << "ERROR! Could not load sound!" << std::endl;

	m_fireSound.setBuffer(m_fireClip);
	m_fireSound.setVolume(30);
	m_alarmSound.setBuffer(m_alarmClip);
	m_alarmSound.setVolume(m_alarmVolume);
	m_alarmSound.setLoop(true);
}

EnemyTurret::~EnemyTurret()
{
	for (int i = 0; i < 10; i++)
	{
		if (m_bullets[i] != nullptr)
		{
			delete m_bullets[i];
			m_bullets[i] = nullptr;
		}
	}

	delete m_sprite;
}

void EnemyTurret::Update()
{
	if (m_HP > 0)
	{

		if (m_player->IsHidden())
		{
			m_alerted = false;
		}
		if (m_alarmSound.getStatus() == m_alarmSound.Paused)
		{
			m_alarmSound.play();
		}

		m_sprite->setPosition(m_pos);
		m_hitbox.setPosition(m_pos);
		m_searchLight.setPosition(m_pos);
		m_sightRadius.setPosition(m_pos.x, m_pos.y);
		m_searchLight.setRotation(m_sprite->getRotation());
		SightCheck(m_player->GetSprite());
		if (m_alerted)
		{
			float ans, ans2, ans3;
			ans = atan2(m_player->GetY() - m_pos.y, m_player->GetX() - m_pos.x);
			float pai = 180 / PI;
			ans *= pai;

			if (ans < 0)
			{
				ans = 360 - (-ans);
			}
			ans2 = static_cast<int>(ans);
			m_sprite->setRotation(90 + ans2);

			m_searchLight.setColor(sf::Color(255, 0, 0, 50));
			m_animation.x = 1;
			Animation(m_sprite, m_animation, 0, 120, 0, 0, 40, 80);
			m_shootDelay -= sf::microseconds(1000);

			ShootProjectile();
			if (m_alarmSound.getStatus() != m_alarmSound.Playing)
			{
				m_alarmSound.play();
			}

		}
		else
		{
			m_searchLight.setColor(sf::Color(255, 255, 255, 50));
			m_animation.x = 0;
			Animation(m_sprite, m_animation, 0, 120, 0, 0, 40, 80);
			
			m_sprite->rotate(0.1);
			m_shootDelay = sf::seconds(1);
			if (m_alarmSound.getStatus() == m_alarmSound.Playing)
			{
				m_alarmSound.stop();
			}
		}
		for (int i = 0; i < 10; i++)
		{
			if (m_bullets[i] != nullptr)
			{
				m_bullets[i]->Trajectory();
				if (m_bullets[i]->GetLifetime() < sf::Time::Zero)
				{
					delete m_bullets[i];
					m_bullets[i] = nullptr;
				}
			}
		}
	}
	else
	{
		if (m_alarmSound.getStatus() == m_alarmSound.Playing)
		{
			m_alarmSound.stop();
		}
	}
}

sf::Sprite* EnemyTurret::GetSprite()
{
	return m_sprite;
}

sf::RectangleShape EnemyTurret::GetHitBox()
{
	return m_hitbox;
}

EENEMY EnemyTurret::GetType()
{
	return ENEMY_TURRET;
}

bool EnemyTurret::IsAlerted()
{
	return m_alerted;
}

void EnemyTurret::SetAlerted(bool p_alerted)
{
	m_alerted = p_alerted;
}

sf::CircleShape EnemyTurret::GetSightRadius()
{
	return sf::CircleShape();
}

void EnemyTurret::SetSightRadius(float p_radius)
{
	m_sightRadius.setRadius(p_radius);
	m_sightRadius.setOrigin(p_radius, p_radius);
}

int EnemyTurret::GetHP()
{
	return m_HP;
}

void EnemyTurret::SetHP(int p_amount)
{
	m_HP += p_amount;
}

sf::Time EnemyTurret::GetTimer()
{
	return m_timer;
}

void EnemyTurret::SetPosition(float p_posX, float p_posY)
{
	m_pos.x = p_posX;
	m_pos.y = p_posY;
}

void EnemyTurret::AddX(float p_x)
{
	m_pos.x += p_x;
}

void EnemyTurret::AddY(float p_y)
{
	m_pos.y += p_y;
}

float EnemyTurret::GetX()
{
	return m_pos.x;
}

float EnemyTurret::GetY()
{
	return m_pos.y;
}

float EnemyTurret::GetCenterX()
{
	return m_center.x;
}

float EnemyTurret::GetCenterY()
{
	return m_center.y;
}

void EnemyTurret::MoveTowards(sf::Vector2f p_pos)
{
}

void EnemyTurret::SightCheck(sf::Sprite * p_sprite)
{
	if (m_sightRadius.getGlobalBounds().intersects(p_sprite->getGlobalBounds()))
	{
		float XDif = abs(m_pos.x) - abs(p_sprite->getPosition().x);
		float YDif = abs(m_pos.y) - abs(p_sprite->getPosition().y);
		float hyp = pow(abs(YDif), 2) + pow(abs(XDif), 2);
		if (sqrt(hyp) < m_sightRadius.getRadius() && !m_player->IsHidden())
		{
			m_alerted = true;
		}
		else
		{
			m_alerted = false;
		}
	}
	
}

void EnemyTurret::ShootProjectile()
{
	int count = 0;
	if (m_shootDelay < sf::Time::Zero)
	{
		for (int i = 0; i < 10; i++)
		{
			if (m_bullets[i] == nullptr)
			{
				int speed_difference = clamp(rand() % 5, 5, 5);
				//int direction_difference = clamp(rand() % 2, 1, 2);
				sf::Vector2f direction = sf::Vector2f
					(
						sin(m_sprite->getRotation() * PI / 180.0) * 1.0,
						cos(m_sprite->getRotation()* PI / 180.0) * 1.0
						);
				m_bullets[i] = new Bullet(m_textureManager, speed_difference, direction, m_pos, m_sprite);
				m_shootDelay = sf::milliseconds(800);

				m_fireSound.play();
			}
			count++;
		}
		m_animation.x = 2;
		Animation(m_sprite, m_animation, 0, 120, 0, 0, 40, 80);
	}

	else if(count >= 10)
	{
		count = 0;
		m_shootDelay = sf::seconds(2);
		for (int i = 0; i < 10; i++)
		{
			if (m_bullets[i] != nullptr)
			{
				delete m_bullets[i];
				m_bullets[i] = nullptr;
			}
		}
	}

	
}

bool EnemyTurret::ProjectileCollision()
{
	for (int i = 0; i < 10; i++)
	{
		if (m_bullets[i] != nullptr && m_player->GetHitbox().getGlobalBounds().intersects(m_bullets[i]->GetSprite().getGlobalBounds()) && m_player->GetHP() > 0)
		{
			m_player->SetHP(-1);
			delete m_bullets[i];
			m_bullets[i] = nullptr;
			return true;
		}
	}
	return false;
}

void EnemyTurret::Draw(sf::RenderWindow & p_window)
{
	//DEBUG F�R SIGHTRADIUS
	//p_window.draw(m_sightRadius);
	if (m_HP > 0)
	{
		//p_window.draw(m_hitbox);
		p_window.draw(m_searchLight);
		for (int i = 0; i < 10; i++)
		{
			if (m_bullets[i] != nullptr)
			{
				p_window.draw(m_bullets[i]->GetSprite());
			}
		}
	}
	if (m_HP <= 0)
	{
		m_sprite->setColor(sf::Color::Black);
	}
	p_window.draw(*m_sprite);
}

void EnemyTurret::PauseSounds()
{
	if (m_alarmSound.getStatus() == m_alarmSound.Playing)
	{
		m_alarmSound.pause();
	}
}

void EnemyTurret::SetState(AIStates * p_state)
{
}
