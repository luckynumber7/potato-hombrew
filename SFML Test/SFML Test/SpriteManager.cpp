#include "SpriteManager.hpp"
#include "stdafx.hpp"

SpriteManager::SpriteManager()
{

}

SpriteManager::~SpriteManager()
{
	auto it = m_sprites.begin();
	while (it != m_sprites.end())
	{
		delete (*it);
		it++;
	}
	m_sprites.clear();
	m_textureMap.clear();

}

sf::Sprite* SpriteManager::CreateSprite(const std::string& p_filename, int p_x, int p_y, int p_w, int p_h)
{
	auto it = m_textureMap.find(p_filename);
	if (it == m_textureMap.end())
	{
		sf::Texture texture;
		if (!texture.loadFromFile(p_filename))
		{
			printf("ERROR LOADING IMAGE");
		}
		texture.setSmooth(true);
		m_textureMap.insert(std::pair<std::string, sf::Texture>(p_filename, texture));
		it = m_textureMap.find(p_filename);
	}
	sf::Sprite* sprite = new sf::Sprite;
	sprite->setTexture(it->second);
	sprite->setTextureRect(sf::IntRect(p_x, p_y, p_w, p_h));
	m_sprites.push_back(sprite);
	return sprite;

}

void SpriteManager::DestroySprite(sf::Sprite* p_sprite)
{
	auto it = m_sprites.begin();
	if (it != m_sprites.end())
	{
		if ((*it) == p_sprite)
		{
			delete (*it);
			m_sprites.erase(it);
			return;
		}
		it++;
	}
}
