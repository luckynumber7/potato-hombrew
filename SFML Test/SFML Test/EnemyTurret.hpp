#pragma once
#include "IEnemy.hpp"
#include "TextureManager.hpp"

class AIStates;
class Player;
class Projectile;
class Bullet;

class EnemyTurret : public IEnemy
{
public:
	EnemyTurret(Player* p_player, TextureManager* p_textureManager, float p_posX, float p_posY);
	~EnemyTurret();
	void Update();
	sf::Sprite* GetSprite();
	sf::RectangleShape GetHitBox();
	EENEMY GetType();

	bool IsAlerted();
	void SetAlerted(bool p_alerted);

	sf::CircleShape GetSightRadius();
	void SetSightRadius(float p_radius);
	int GetHP();
	void SetHP(int p_amount);

	sf::Time GetTimer();

	void SetPosition(float p_posX, float p_posY);
	void AddX(float p_x);
	void AddY(float p_y);
	float GetX();
	float GetY();
	float GetCenterX();
	float GetCenterY();
	void MoveTowards(sf::Vector2f p_pos);
	void SightCheck(sf::Sprite* p_sprite);
	void ShootProjectile();

	bool ProjectileCollision();

	void Draw(sf::RenderWindow& p_window);
	void PauseSounds();
	void SetState(AIStates* p_state);
private:
	TextureManager* m_textureManager;

	bool m_alerted;
	float m_speed;
	int m_HP;
	int m_alarmVolume;
	

	AIStates* m_state;
	Player* m_player;
	sf::Sprite* m_sprite;
	Bullet* m_bullets[10];
	sf::Sprite m_searchLight;

	sf::Texture* m_texture;
	sf::Texture* m_searchLightTex;

	sf::CircleShape m_sightRadius;
	sf::RectangleShape m_hitbox;

	sf::Vector2f m_pos;
	sf::Vector2f m_center;
	sf::Vector2f m_waypoint;
	sf::Vector2i m_animation;

	sf::Clock m_clock;
	sf::Time m_shootDelay;
	sf::Time m_timer;

	sf::SoundBuffer m_fireClip;
	sf::SoundBuffer m_alarmClip;
	sf::Sound m_fireSound;
	sf::Sound m_alarmSound;
};