#include "Application.hpp"


//sf::Clock deltaClock;


Application::Application()
{
	//m_window.setFramerateLimit(60);
	m_textureManager = new TextureManager;
}

Application::~Application()
{
	delete m_textureManager;
}

void Application::run()
{
	// Create render window
	m_window.create(sf::VideoMode{ 1920, 1080 }, "Potato Pirates - Team 7 - Beta 1.3", sf::Style::Titlebar | sf::Style::Close); // | sf::Style::Fullscreen 
	m_textureManager->PreLoadTextures();
	// Initialize the engine and IntroState(Menu)
	m_machine.run(StateMachine::build<IntroState>(m_machine, m_window, m_textureManager, true, false));
	// Main loop, drawing and updating
	const float targettime = 1.0f / 600.0f;
	float accumulator = 0.0f;
	float frametime = 0.0f;

	sf::Clock clock;



	while (m_machine.running())
	{
		sf::Time deltatime = clock.restart();
		frametime = deltatime.asSeconds();
		if (frametime > 0.1f)
			frametime = 0.1f;

		accumulator += frametime;
		while (accumulator > targettime)
		{

			accumulator -= targettime;

			m_machine.nextState();
			m_machine.update();
			m_machine.draw();


		}

		
	}
}
