#include "State.hpp"
#include "TextureManager.hpp"

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/System.hpp>
#include <SFML/Graphics/Color.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>

#include <SFML\Audio.hpp>
#include <SFML/Audio/Music.hpp>

class StateMachine;

namespace sf
{
	class RenderWindow;
}

class IntroState : public State
{
public:
	IntroState(StateMachine& game, sf::RenderWindow& window, TextureManager* p_textureManager, bool replace = true, bool restart = true);
	~IntroState();

	void pause();
	void resume();
	void update();
	void draw();

private:
	sf::Texture* m_bgTex;
	sf::Texture* m_Cloud1Tex;
	sf::Texture* m_fgTex;
	sf::Texture* m_CloudTex;

	sf::Texture* m_IdleButtsTex;
	sf::Texture* m_PlayHoverTex;
	sf::Texture* m_ControlsHoverTex;
	sf::Texture* m_ScoreHoverTex;
	sf::Texture* m_CreditsHoverTex;
	sf::Texture* m_QuitHoverTex;
	sf::Texture* m_TitleTex;

	sf::Sprite m_bg;
	sf::Sprite m_Cloud1;
	sf::Sprite m_fg;
	sf::Sprite m_Cloud2;

	sf::Sprite m_IdleButts;
	sf::Sprite m_PlayButton;
	sf::Sprite m_ScoreButton;
	sf::Sprite m_CreditsButton;
	sf::Sprite m_ControlsButton;
	sf::Sprite m_QuitButton;

	sf::Sprite m_Title;

	sf::Music music;

	sf::View MenuView;

	int hoverButton = 0;

};

