#pragma once
#include "stdafx.hpp"

//TextureManager pre-loads all textures using its own CreateTexture function
//The textures are then loaded by using the LoadTexture function
class TextureManager
{
public:
	TextureManager();
	~TextureManager();
	void PreLoadTextures();
	void CreateTexture(std::string p_filepath);
	void DestroyTexture(std::string p_filepath);
	sf::Texture* LoadTexture(std::string p_filepath);
private:
	std::map<std::string, sf::Texture*> m_textures;

};