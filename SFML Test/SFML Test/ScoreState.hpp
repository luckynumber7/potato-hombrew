#pragma once
#include "State.hpp"

#include "TextureManager.hpp"

#include <SFML/System.hpp>
#include <SFML/Graphics/Color.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>

#include <SFML\Audio.hpp>
#include <SFML/Audio/Music.hpp>

class StateMachine;

namespace sf
{
	class RenderWindow;
}

class ScoreState : public State
{
public:
	ScoreState(StateMachine& game, sf::RenderWindow& window, TextureManager* p_textureManager, bool replace = false, bool restart = false);

	void pause();
	void resume();
	void update();
	void draw();
private:
	sf::Texture* m_bgTex;
	sf::Texture* m_Cloud1Tex;
	sf::Texture* m_fgTex;
	sf::Texture* m_CloudTex;

	sf::Sprite m_bg;
	sf::Sprite m_Cloud1;
	sf::Sprite m_fg;
	sf::Sprite m_Cloud2;

	sf::Font font;
	sf::Font font2;
	sf::Text HghScr;
	sf::Text NamesStr;
	sf::Text ScoresStr;

	sf::Text returnText;

	sf::Texture* m_dialogTex;
	sf::Sprite m_dialog;

	std::ifstream inname;
	std::ifstream inscore;
	std::ifstream incash;

	std::ofstream outnames;
	std::ofstream outscore;

	sf::Text enterText;
	sf::Text playerText;

	std::string Names[5];
	std::string Scores[5];

	std::string actNString;
	std::string actSString;

	sf::Music music;
};
