#include "stdafx.hpp"
#include "Powerup.hpp"

//Baked potato related code
BakedPotato::BakedPotato(TextureManager* p_TextureManager, sf::Vector2f p_direction, sf::Vector2f p_start) //sf::Texture p_texturePotato, sf::Texture p_textureExplosion
{
	m_timer = sf::milliseconds(1000);
	m_bakedPotato = p_TextureManager->LoadTexture("../assets/Baked Potato sprite.png");
	m_explosion = p_TextureManager->LoadTexture("../assets/Baked potato explosion sprite sheet.png");
	m_center.x = 15;
	m_center.y = 15;

	m_sprite = new sf::Sprite;
	m_sprite->setTexture(*m_bakedPotato);
	m_sprite->setPosition(p_start);
	m_sprite->setOrigin(m_center.x, m_center.y);

	m_scale.x = 1;
	m_scale.y = 1;
	m_color = 0;

	m_hitbox.setSize(sf::Vector2f(15, 15));
	m_hitbox.setFillColor(sf::Color::Blue);

	m_direction = p_direction;
	m_animation = sf::Vector2i(0, 1);
	m_isExploding = false;
}

BakedPotato::~BakedPotato()
{
	delete m_sprite;
}

bool BakedPotato::Update()
{

	m_timer -= m_clock.restart();
	
	if (m_animation.x >= 11)
	{
		return false;
	}
	if(m_timer < sf::Time::Zero && m_isExploding == true)
	{
		m_animation.x++;
		Animation(m_sprite, m_animation, 0, 3069, 0, 0, 279, 298);
		m_timer = sf::milliseconds(100);
	}
	else if (m_timer < sf::Time::Zero && m_isExploding == false)
	{
		m_center.x = 279 / 2;
		m_center.y = 298 / 2;
		m_sprite->setRotation(0);
		m_sprite->setOrigin(m_center.x, m_center.y);
		m_sprite->setTexture(*m_explosion);
		m_sprite->setTextureRect(sf::IntRect(0, 0, 279, 298));
		m_hitbox.setSize(sf::Vector2f(180, 200));
		m_hitbox.setOrigin(sf::Vector2f(m_center.x - (m_sprite->getTextureRect().width - m_hitbox.getSize().x) / 2, m_center.y - (m_sprite->getTextureRect().height - m_hitbox.getSize().y) / 2));
		m_hitbox.setPosition(m_sprite->getPosition());
		//m_sprite.setOrigin()
		m_isExploding = true;
	}
	else if(m_isExploding == false)
	{
		m_sprite->move(1.1 * m_direction.x, -1.1 * m_direction.y);
		m_scale.x = m_scale.x + 0.0003;
		m_scale.y = m_scale.y + 0.0003;
		m_sprite->rotate(1);
		/*int tempcolor_a, tempcolor_b;
		tempcolor_a = clamp(m_color - 153, 20, 153);
		tempcolor_b = clamp(m_color - 153, 20, 153);
		m_sprite->setColor(sf::Color(m_color, tempcolor_a, tempcolor_b, 255));
		m_color++;*/
		m_sprite->setScale(m_scale);
	}
	return true;
}

bool BakedPotato::IsExploding()
{
	return m_isExploding;
}

sf::Sprite* BakedPotato::GetSprite()
{
	return m_sprite;
}

EPOWERUP BakedPotato::GetType()
{
	return BAKED_POTATO;
}

sf::RectangleShape BakedPotato::GetHitbox()
{
	return m_hitbox;
}

void BakedPotato::draw(sf::RenderWindow & p_window)
{
//	p_window.draw(m_hitbox);
	p_window.draw(*m_sprite);
}

//Potato cloud related code
PotatoCloud::PotatoCloud(TextureManager* p_textureManager, sf::Vector2f p_direction, sf::Vector2f p_start, float p_rotation)
{
	m_timer = sf::milliseconds(100);
	m_lifetime = sf::seconds(2);

	m_potatoCloud = p_textureManager->LoadTexture("../assets/Potato cloud sprite sheet.png");
	
	m_sprite = new sf::Sprite;
	m_sprite->setTexture(*m_potatoCloud);
	m_sprite->setTextureRect(sf::IntRect(0, 0, 351, 212));

	m_center.x = 351 / 2;
	m_center.y = 212 / 2;
	m_scale.x = 1;
	m_scale.y = 1;
	m_fade = 255;

	m_direction = p_direction;
	m_animation = sf::Vector2i(0, 1);


	m_sprite->setPosition(p_start);
	m_sprite->setOrigin(m_center.x, m_center.y);
	m_sprite->setRotation(p_rotation);

	m_hitbox.setSize(sf::Vector2f(300, 100));
	m_hitbox.setOrigin(sf::Vector2f(m_center.x - (m_sprite->getTextureRect().width - m_hitbox.getSize().x) / 2, m_center.y - (m_sprite->getTextureRect().height - m_hitbox.getSize().y) / 2));
	m_hitbox.setFillColor(sf::Color::Blue);
	m_hitbox.setRotation(p_rotation);

	m_active = false;
}

PotatoCloud::~PotatoCloud()
{
	delete m_sprite;
}

bool PotatoCloud::Update()
{
	m_timer -= m_clock.restart();
	if (m_lifetime < sf::Time::Zero && m_timer < sf::Time::Zero)
	{
		m_animation.x++;
		Animation(m_sprite, m_animation, 0, 5970, 0, 0, 351, 212);
		m_timer = sf::milliseconds(100);
		if(m_animation.x >= 17)
			return false;
	}
	else if(m_timer < sf::Time::Zero && m_active == true)
	{
		m_animation.x++;
		if (m_animation.x >= 9)
		{
			m_animation.x = 6;
		}
		Animation(m_sprite, m_animation, 0, 5970, 0, 0, 351, 212);
		m_timer = sf::milliseconds(100);
		m_lifetime -= sf::milliseconds(600);
	}
	else if (m_timer < sf::Time::Zero && m_active == false)
	{
		if (m_animation.x == 5)
		{
			m_active = true;
		}
		m_animation.x++;
		Animation(m_sprite, m_animation, 0, 5970, 0, 0, 351, 212);
		m_timer = sf::milliseconds(100);
	}
	                          
	m_sprite->move(-0.3 * m_direction.x, 0.3 * m_direction.y);
	m_hitbox.setPosition(m_sprite->getPosition());
	return true;
}

bool PotatoCloud::IsActive()
{
	return m_active;
}

sf::Sprite* PotatoCloud::GetSprite()
{
	return m_sprite;
}

EPOWERUP PotatoCloud::GetType()
{
	return POTATO_CLOUD;
}

sf::RectangleShape PotatoCloud::GetHitbox()
{
	return m_hitbox;
}

void PotatoCloud::draw(sf::RenderWindow & p_window)
{
//	p_window.draw(m_hitbox);
	p_window.draw(*m_sprite);
}
