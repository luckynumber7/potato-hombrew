#include "stdafx.hpp"
#include "Projectile.hpp"

//Potato-related code

Potato::Potato(TextureManager* p_textureManager, float p_speed, sf::Vector2f p_direction, sf::Vector2f p_start)
{
	m_potato = p_textureManager->LoadTexture("../assets/PotatoBullet.png");
	m_sprite.setTexture(*m_potato);
	m_sprite.setPosition(p_start);

	m_lifetime = sf::seconds(2);
	m_color = 255;
	m_scale.x = 1.75;
	m_scale.y = 1.75;
	m_speed = p_speed;
	m_direction = p_direction;

	
}

Potato::~Potato()
{

}

void Potato::Trajectory()
{
	m_sprite.move(m_speed * m_direction.x, -m_speed * m_direction.y);
	m_sprite.rotate(1);
	m_scale.x = m_scale.x - 0.00005;
	m_scale.y = m_scale.y - 0.00005;
	m_sprite.setScale(m_scale);

	//m_color = clamp(m_color - 0.000000001, 0, 255);
	//m_sprite.setColor(sf::Color(m_color, m_color, m_color, m_color));
	m_lifetime -= m_clock.restart();
}

sf::Time Potato::GetLifetime()
{
	return m_lifetime;
}

EPROJECTILE Potato::GetType()
{
	return PLAYER_POTATO;
}

sf::Sprite Potato::GetSprite()
{
	return m_sprite;
}

//Bullet-related code

Bullet::Bullet(TextureManager* p_textureManager, float p_speed, sf::Vector2f p_direction, sf::Vector2f p_start, sf::Sprite* p_sprite)
{
	m_texture = p_textureManager->LoadTexture("../assets/Middle Bullet.png");
	m_sprite.setTexture(*m_texture);
	m_lifetime = sf::seconds(1);
	m_color = 255;
	m_speed = p_speed;
	m_direction = p_direction;
	m_sprite.setPosition(p_start);
	m_sprite.setOrigin(3.5, 11.5);
	m_sprite.setRotation(p_sprite->getRotation());
	m_scale.x = 1.10;
	m_scale.y = 1.10;
}
Bullet::~Bullet()
{
	
}
void Bullet::Trajectory()
{
	m_sprite.move(m_speed * m_direction.x, -m_speed * m_direction.y);
	m_scale.x = m_scale.x - 0.0005;
	m_scale.y = m_scale.y - 0.0005;
	m_sprite.setScale(m_scale);

	//m_color = clamp(m_color - 0.000000001, 0, 255);
	//m_sprite.setColor(sf::Color(m_color, m_color, m_color, m_color));
	m_lifetime -= m_clock.restart();
}
sf::Time Bullet::GetLifetime()
{
	return m_lifetime;
}
EPROJECTILE Bullet::GetType()
{
	return ENEMY_BULLET;
}
sf::Sprite Bullet::GetSprite()
{
	return m_sprite;
}

